package fr.univ_lille.cristal.emeraude.n2s3

import akka.remote.testkit.MultiNodeConfig
import com.typesafe.config.ConfigFactory

object MultiNodeSampleConfig extends MultiNodeConfig {
  val master = role("master")
  val slave1 = role("slave1")
  val slave2 = role("slave2")

  nodeConfig(master)(ConfigFactory.load("master.conf"))
  nodeConfig(slave1)(ConfigFactory.load("slave.conf"))
  nodeConfig(slave2)(ConfigFactory.load("slave.conf"))
}