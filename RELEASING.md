# N2S3's release process so far is the following

During development,
  * development of version [X] is done in branch dev-[X], e.g. dev-0.2
  * version numbers in build.sbt is [X]-SNAPSHOT, e.g., 0.2-SNAPSHOT
  * regularly, SNAPSHOT versions are published in https://oss.sonatype.org/content/repositories/snapshots/fr/univ-lille/cristal/n2s3_2.11/
At release time
  1) branch dev-[X] is merged into master
  2) version numbers in build.sbt are set to [X]
  3) a fixed version is staged/published in https://oss.sonatype.org/
  4) a tag v[X] is created
  5) a new development branch dev-[X+1] is started
     * and a new development cycle starts
