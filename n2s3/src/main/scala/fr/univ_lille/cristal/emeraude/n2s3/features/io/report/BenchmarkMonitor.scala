/************************************************************************************************************
 * Contributors:
 * 		- created by Pierre Falez
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.io.report

import java.io.{File, PrintWriter}

import akka.actor.{Actor, ActorRef}
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.Stop
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done, Initialize}
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

import scala.collection.mutable
import scala.concurrent.Await

case class GetResult() extends Message


class InputCrossInfo(val label : String , val startTime : Timestamp, val endTime : Timestamp) {
  val spikeList = mutable.ArrayBuffer[(String, Timestamp)]()
}
class BenchmarkMonitor(n2s3 : N2S3, outputNeuron : Seq[NetworkEntityPath], timeOffset : Time = Time(0)) extends NetworkActor {

  val inputList = scala.collection.mutable.ListBuffer[InputCrossInfo]()

  val inAdvanceSpike = scala.collection.mutable.Queue[(NetworkEntityPath, Timestamp)]()
  val outputNeuronAssoc : Map[NetworkEntityPath, String] = outputNeuron.map(p => p -> p.toString).toMap

  override def initialize(): Unit = {
    askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))

    outputNeuron.map {path  =>
      askFuture(path, SubscribeSynchronized(NeuronFireEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))
    }.foreach(f => Await.result(f, Config.longTimeout.duration))

  }

  override def destroy(): Unit = {
    askTo(n2s3.inputLayerRef.get.getContainer, Unsubscribe(LabelChangeEvent, ExternalSender.getReference(self)))

    outputNeuron.map { path =>
      askFuture(path, Unsubscribe(NeuronFireEvent, ExternalSender.getReference(self)))
    }.foreach(f => Await.result(f, Config.longTimeout.duration))

  }

  def classifySpike(origin : NetworkEntityPath, timestamp : Timestamp) : Unit = {
    var currentIndex = inputList.size - 1

    while (currentIndex >= 0 && inputList(currentIndex).startTime+timeOffset.timestamp > timestamp)
      currentIndex -= 1

    if (currentIndex >= 0)
      inputList(currentIndex).spikeList += ((outputNeuronAssoc(origin), timestamp))
  }

  override def process(message: Message, sender : ActorRef): Unit = message match {
    case SynchronizedEvent(s, _, m, _) =>
      m match {
        case LabelChangeResponse(start, end, label) =>
          inputList += new InputCrossInfo(label, start, end)

        case NeuronFireResponse(timestamp, path) =>
          if (inputList.isEmpty || (inputList.last.endTime != Long.MaxValue && timestamp > inputList.last.endTime + timeOffset.timestamp)) {
            inAdvanceSpike += ((path, timestamp))
          }
          else {
            classifySpike(path, timestamp)
          }
      }
      ExternalSender.sendTo(s, Done)
    case GetResult() =>
      inAdvanceSpike.foreach(spike => classifySpike(spike._1, spike._2))
      inAdvanceSpike.clear()

      val file = new File("results/raw_spikes")
      if (file.exists()) file.delete()
      file.getParentFile.mkdirs()
      val writer = new PrintWriter(file)
      inputList.zipWithIndex.foreach{ case(input, index) =>
        writer.println("#" + (index+1) + " : " + input.label+" ["+input.startTime+";"+input.endTime+"]")
        input.spikeList.foreach(spike => writer.println(spike._1+" "+spike._2))
      }
      writer.close()
      sender ! new BenchmarkResult(inputList.map(input => input.label).distinct , outputNeuronAssoc.values.toSeq, inputList)

    case Done => sender ! Done
    case Initialize => sender ! Done
    case Stop =>
      sender ! Done
      context.stop(self)

  }

}