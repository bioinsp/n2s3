package fr.univ_lille.cristal.emeraude.n2s3.support.io

import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{InputPacket, InputTemporalPacket}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

/**
  * Created by guille on 5/20/16.
  */
abstract class InputStream[T <: InputPacket] {

  final def nextData() : T = {
    transformData(next())
  }

  def resetStream() : Unit = {
    reset()
  }

  def next(): T
  def next(n: Int): Seq[T] = for (i<- 0 until n) yield this.next()
  def reset() : Unit
  def atEnd(): Boolean

  def transformData(data : T) : T = data
}

abstract class TemporalInputStream[T <: InputTemporalPacket] extends InputStream[T] {

  var currentPrefixTimestamp : Timestamp = 0
  var maxTimestamp : Timestamp = 0

  override def transformData(data : T) : T = {
    maxTimestamp = math.max(maxTimestamp, data.getStartTimestamp+data.getDuration)
    data.modifyTimestamp(currentPrefixTimestamp)
    data
  }

  override def resetStream() : Unit = {
    currentPrefixTimestamp = maxTimestamp
    reset()
  }
}

abstract class InputStreamDecorator[T <: InputPacket, R <: InputPacket](decoree: InputStream[R]) extends InputStream [T] {
  def reset() = decoree.reset()
  override def toString = decoree.toString
}

