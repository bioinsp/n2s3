package fr.univ_lille.cristal.emeraude.n2s3.features.builder

import fr.univ_lille.cristal.emeraude.n2s3.core._

/**
  * Created by guille on 7/29/16.
  */
class NeuronRef(val group: NeuronGroupRef, index: Seq[Int]) extends N2S3ActorRef {
  def getIdentifier: Any = this.group.getIdentifierOf(this)
  def newNeuron(): NetworkEntity = group.defaultNeuronConstructor()
  def isConnectedTo(aNeuron: NeuronRef) = group.isConnected(this, aNeuron)
  def getIndex : Seq[Int] = index
}
