package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time

import scala.util.Random

/**
  * Created by falezp on 27/10/16.
  */


object JitterNoise {
  def apply[T <: InputTemporalPacket](standartDeviation : Time) = new JitterNoise[T](standartDeviation)
}

class JitterNoise[T <: InputTemporalPacket](standartDeviation : Time) extends StreamConverter[T, T] {

  private var minTimestamp : Timestamp = 0L
  private var nextMinTimestamp : Timestamp = 0L

  override def dataConverter(in: T) : T = {
    nextMinTimestamp = minTimestamp
    val out = in.mapUnitData{case(_, d) => applyJitter(d)}
    minTimestamp = nextMinTimestamp
    out
  }

  private def applyJitter(el : InputTemporalData) : InputTemporalData = {
    val t = math.max((Random.nextGaussian()*standartDeviation.timestamp).toLong, minTimestamp-el.getStartTimestamp)
    el.modifyTimestamp(t)
    nextMinTimestamp = math.max(el.getStartTimestamp+el.getDuration, nextMinTimestamp)
    el
  }

  def resetConverter() : Unit = {
    minTimestamp = 0L
    nextMinTimestamp = 0L
  }
}
