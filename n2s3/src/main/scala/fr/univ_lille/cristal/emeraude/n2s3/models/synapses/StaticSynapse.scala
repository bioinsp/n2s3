package fr.univ_lille.cristal.emeraude.n2s3.models.synapses

import fr.univ_lille.cristal.emeraude.n2s3.core.{NeuronConnection, SynapseBuilder}
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{BackwardSpike, ElectricSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse.FloatSynapse
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message
/**
  * Created by pfalez on 24/05/17.
  */


object StaticSynapse{
  def apply(w : Float = 1f, d : Time = Time(0)) = new StaticSynapseBuilder(w, d)
}

class StaticSynapseBuilder(w : Float, d : Time = Time(0)) extends SynapseBuilder {
  override def createSynapse: NeuronConnection = new StaticSynapse(w, d)
}

class StaticSynapse(w : Float = 1f, d : Time = Time(0)) extends FloatSynapse(w, d) {
  override def processConnectionMessage(timestamp: Timestamp, message: Message): Unit = message match {
    case ElectricSpike(charge) =>
      getEnds.sendToOutput(timestamp+getDelay.timestamp, ElectricSpike(charge*getWeight))
    case BackwardSpike =>
  }
}