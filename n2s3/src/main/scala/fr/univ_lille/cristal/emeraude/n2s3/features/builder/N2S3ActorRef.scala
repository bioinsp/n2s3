package fr.univ_lille.cristal.emeraude.n2s3.features.builder
import fr.univ_lille.cristal.emeraude.n2s3.core.{ExternalSender, NetworkEntityPath}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * Created by guille on 8/4/16.
  */
trait N2S3ActorRef {

  var actorPath: Option[NetworkEntityPath] = None
  var deployed = false
  def setActor(path: NetworkEntityPath) = this.actorPath = Some(path)
  def getNetworkAddress = this.actorPath.get


  def isDeployed = deployed
  def setDeplyed(state : Boolean = true) = {
    deployed = state
  }

  def send(message: Message): Unit =  {
    ExternalSender.sendTo(this.actorPath.get, message )
  }

  def ask(message: Message): Unit =  {
    ExternalSender.askTo(this.actorPath.get, message )
  }
}