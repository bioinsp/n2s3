package fr.univ_lille.cristal.emeraude.n2s3.core.models.properties

import fr.univ_lille.cristal.emeraude.n2s3.core.Property
import fr.univ_lille.cristal.emeraude.n2s3.support.Time

/**
  * Created by falezp on 25/10/16.
  */

/**
  * Property used to represent membrane time constant for the membrane voltage leakage
  * @tparam T is the leak value type
  */
class MembraneLeak[T] extends Property[T]

//
//  Type specializations
//
object MembraneLeakTime extends MembraneLeak[Time]
