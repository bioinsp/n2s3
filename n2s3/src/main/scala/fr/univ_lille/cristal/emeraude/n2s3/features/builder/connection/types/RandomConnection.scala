package fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types

import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.{Connection, ConnectionPolicy, ConnectionTypeBuilder}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{NeuronGroupRef, NeuronRef}

import scala.util.Random

/**
  * Created by falezp on 07/07/16.
  */

object RandomConnection extends ConnectionTypeBuilder{
  val defaultConnectionRate = 0.5f
  def apply() = new RandomConnectionBuilder(defaultConnectionRate)
  def apply(connectionRate: Float) = new RandomConnectionBuilder(connectionRate)
  override def createConnection(neuronConnectionConstructor: () => NeuronConnection): RandomConnection = this().createConnection(neuronConnectionConstructor)
}

class RandomConnectionBuilder(connectionRate : Float) extends ConnectionTypeBuilder{
  override def createConnection(neuronConnectionConstructor: () => NeuronConnection) = new RandomConnection(connectionRate, neuronConnectionConstructor)
}


class RandomConnection(connectionRate : Float, connectionConstructor : () => NeuronConnection) extends ConnectionPolicy {
  override def generate(from: NeuronGroupRef, to: NeuronGroupRef): Traversable[Connection] = {
    for {
      in <- from.neuronPaths
      out <- to.neuronPaths
      rand = Random.nextFloat()
      if in != out && rand <= connectionRate
    } yield Connection(in, out, Some(connectionConstructor()))
  }

  /** ******************************************************************************************************************
    * Testing
    * *****************************************************************************************************************/
  override def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = true
}