package fr.univ_lille.cristal.emeraude.n2s3.models.neurons

import fr.univ_lille.cristal.emeraude.n2s3.core
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{ConnectionID, Inhibition, NeuronEnds}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{BackwardSpike, ElectricSpike, ResetState}
import fr.univ_lille.cristal.emeraude.n2s3.core.event.{NeuronFireEvent, NeuronFireResponse, NeuronPotentialResponse, NeuronPotentialUpdateEvent}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse.FloatSynapse
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias._
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message
import squants.electro.CapacitanceConversions.CapacitanceConversions
import squants.electro.ElectricPotential
import squants.electro.ElectricPotentialConversions.ElectricPotentialConversions

import scala.math._

/**
  * Created by falezp on 13/07/16.
  * Implementation of the Leaky-Integrate-And-Fire neuron model
  *
  * More informations can be found at the following address:
  * 	- http://www.cns.nyu.edu/~eorhan/notes/lif-neuron.pdf
  */



object LIF extends NeuronModel {
  override def createNeuron(): LIF = new LIF()
}

class LIF extends core.Neuron {
  /********************************************************************************************************************
    * Model Parameters
    ******************************************************************************************************************/

  private var membraneThresholdType = MembraneThresholdTypeEnum.Dynamic
  private var membranePotential = 0 millivolts
  private var restingPotential = 0 millivolts
  private var membranePotentialThreshold = 1 millivolts
  private var membraneCapacitance = 1 farads
  private var tRefract = 1 MilliSecond
  private var tInhibit = 10 MilliSecond
  private var tLeak = 100 MilliSecond


  private var theta = 0 millivolts
  private var thetaLeak = 1e7 MilliSecond
  private var theta_step = 0.05  millivolts
  private var inhibitionRatio = 0f


  /********************************************************************************************************************
    * Remembered properties
    ******************************************************************************************************************/
  private var tLastTheta : Timestamp = Int.MinValue
  private var tLastInhib : Timestamp = Int.MinValue
  private var tLastSpike : Timestamp = Int.MinValue
  private var tLastFire : Timestamp = Int.MinValue
  /********************************************************************************************************************
    * Properties and Events
    ******************************************************************************************************************/
  addProperty[ElectricPotential](MembranePotentialThreshold, () => membranePotentialThreshold, membranePotentialThreshold = _)
  addProperty[ElectricPotential](MembraneRestingPotential, () => restingPotential, restingPotential = _)
  addProperty[Time](MembraneLeakTime, () => tLeak, tLeak = _)
  addProperty[Time](MembraneRefractoryDuration, () => tRefract, tRefract = _)
  addProperty[Time](MembraneInhibitionDuration, () => tInhibit, tInhibit = _)
  addProperty[MembraneThresholdTypeEnum.Value](MembraneThresholdType, () => membraneThresholdType, membraneThresholdType = _)
  addProperty[ElectricPotential](AdaptiveThreshold, () => theta, theta = _)
  addProperty[ElectricPotential](AdaptiveThresholdAdd, () => theta_step, theta_step = _)
  addProperty[Time](AdaptiveThresholdLeak, () => thetaLeak, thetaLeak = _)
  InhibitionRatio


  addEvent(NeuronPotentialUpdateEvent)

  /********************************************************************************************************************
    * Neuron Behavior
    ******************************************************************************************************************/


  def processSomaMessage(timestamp : Timestamp, message : Message, fromConnection : Option[(ConnectionID, NeuronConnection)], ends : NeuronEnds) : Unit = message match {
    case ElectricSpike(spikeCharge) =>

      //println(membranePotentialThreshold+" + "+theta)


      if (timestamp - tLastFire < tRefract.timestamp || timestamp - tLastInhib < tInhibit.timestamp)
        return

      //println((spikeCharge/membraneCapacitance).toMillivolts)
        membranePotential = membranePotential * exp(-(timestamp - tLastSpike).toDouble / tLeak.timestamp.toDouble) + spikeCharge / membraneCapacitance


        val v = membranePotential + restingPotential
        // println(getNetworkAddress+" "+restingPotential+"+"+membranePotential+" <> "+membranePotentialThreshold+"+"+theta)

        if (membraneThresholdType == MembraneThresholdTypeEnum.Dynamic && !hasFixedParameter) {
          this.theta = this.theta * exp(-(timestamp - this.tLastSpike).toDouble / thetaLeak.timestamp.toDouble).toFloat
          this.tLastTheta = timestamp
        }

        if (v >= membranePotentialThreshold + theta) {

          membranePotential = 0 millivolts

          if (membraneThresholdType == MembraneThresholdTypeEnum.Dynamic && !hasFixedParameter) {
            theta += theta_step
          }

          triggerEventWith(NeuronFireEvent, NeuronFireResponse(timestamp, getNetworkAddress))
          triggerEventWith(NeuronPotentialUpdateEvent, NeuronPotentialResponse(timestamp, getNetworkAddress, membranePotential.toMillivolts.toFloat))
          ends.sendToAllInput(timestamp, BackwardSpike)
          ends.sendToAllOutput(timestamp, ElectricSpike())
          tLastFire = timestamp
        }
        else {
          triggerEventWith(NeuronPotentialUpdateEvent, NeuronPotentialResponse(timestamp, getNetworkAddress, membranePotential.toMillivolts.toFloat))
        }

        tLastSpike = timestamp

    case Inhibition(ratio) =>
      tLastInhib = timestamp
      membranePotential *= ratio

      triggerEventWith(NeuronPotentialUpdateEvent, NeuronPotentialResponse(timestamp, getNetworkAddress, membranePotential.toMillivolts.toFloat))
    case ResetState =>
      tLastInhib = Int.MinValue
      tLastFire = Int.MinValue
      membranePotential = 0 millivolts
  }
}
