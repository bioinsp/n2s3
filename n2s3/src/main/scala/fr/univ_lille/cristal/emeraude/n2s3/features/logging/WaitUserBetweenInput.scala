package fr.univ_lille.cristal.emeraude.n2s3.features.logging

import akka.actor.{ActorRef, Props}
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Done
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.core.{ExternalSender, NetworkActor}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupObserverRef}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{LocalActorDeploymentStrategy, Message}

/**
  * Created by pfalez on 10/05/17.
  */
class WaitUserBetweenInputRef(n2s3 : N2S3) extends NeuronGroupObserverRef {
  var actors : Option[ActorRef] = None
  def getActors : Seq[ActorRef] = actors.toSeq


  override def deploy(n2s3: N2S3): Unit = {
    this.actors = Some(n2s3.system.actorOf(Props(new WaitUserBetweenInput(n2s3)), LocalActorDeploymentStrategy))
  }
}
class WaitUserBetweenInput(n2s3 : N2S3) extends NetworkActor {


  override def initialize(): Unit = {
    askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))
  }

  override def destroy(): Unit = {
    askTo(n2s3.inputLayerRef.get.getContainer, Unsubscribe(LabelChangeEvent, ExternalSender.getReference(self)))
  }
  override def process(message: Message, sender : ActorRef): Unit = message match {
    case SynchronizedEvent(s, _, m, _) =>
      m match {
        case LabelChangeResponse(start, end, label) =>

          println("Press a key...")
          Console.in.read
      }
      ExternalSender.sendTo(s, Done)
  }

}