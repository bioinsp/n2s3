package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import fr.univ_lille.cristal.emeraude.n2s3.core.actors.InputLayer

/**
  * Created by guille on 6/10/16.
  */

abstract class InputStageStrategy() {
  var inputLayer: Option[InputLayer] = None
  protected def doEndOfStage() = this.inputLayer match{
    case Some(layer) =>
    case None =>
  }

  def streamFinished() = { this.doEndOfStage() }
  def itemProcessed()
}

class EndStageOnItemProcessed() extends InputStageStrategy {
  override def itemProcessed(): Unit = { this.doEndOfStage() }
}

class EndStageOnStreamEnd() extends InputStageStrategy {
  override def itemProcessed(): Unit = { /*Do Nothing*/ }
}
