package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import fr.univ_lille.cristal.emeraude.n2s3.core.event.{Event, EventResponse}

/**
  * Created by pfalez on 10/04/17.
  */

object OnOffFunctions {

  private def sumKernel(kernelWidth : Int, kernelHeight : Int, function : (Float, Float) => Float): Float = {
    (for {
      x <- 0 until kernelWidth
      y <- 0 until kernelHeight
    } yield {
      val curr_x = x.toFloat-kernelWidth/2
      val curr_y = y.toFloat-kernelHeight/2
      function(curr_x, curr_y)
    }).sum
  }

  def Gaussian(sigma : Float, normalize : Boolean = true) : (Float, Float) => Float = {
    (x : Float, y : Float) => {
      val factor = if(normalize) 1.0 / (sigma*math.sqrt(2f*math.Pi)) else 1f
      (factor*math.exp(-(x * x + y * y) / (2.0 * sigma * sigma))).toFloat
    }
  }

  def DifferenceOfGaussian(sigmaCenter : Float, sigmaSurround : Float, kernelWidth : Int, kernelHeight : Int, normalize : Boolean = true) : (Float, Float) => Float = {

    val factor = if(normalize) {
      sumKernel(kernelWidth, kernelHeight, Gaussian(sigmaCenter, normalize = false))/sumKernel(kernelWidth, kernelHeight, Gaussian(sigmaSurround, normalize = false))
    } else {
      1f
    }

    (x : Float, y : Float) => {
      Gaussian(sigmaCenter, normalize = false).apply(x , y)-factor*Gaussian(sigmaSurround, normalize = false).apply(x , y)
    }
  }

  def LaplacianOfGaussian(sigma : Float) : (Float, Float) => Float = {
    (x : Float, y : Float) => {
      ((-1.0 / (math.Pi * math.pow(sigma, 4.0))) * (1.0 - (x * x + y * y) / (2.0 * sigma * sigma)) * math.exp(-(x * x + y * y) / (2.0 * sigma * sigma))).toFloat
    }
/*
    if(normalize) {
      val f_norm = f(0, 0)
      (x: Float, y: Float) => {
        f(x, y) / f_norm
      }
    }
    else
      f*/
  }
}

object OnOffCell {
  def apply(filterWidth : Int, filterHeight : Int, filterFunction : (Float, Float) => Float) = new OnOffCell(filterWidth, filterHeight, filterFunction)
}

object OnOffCellEvent extends Event[OnOffCellEventResponse]
case class OnOffCellEventResponse(sample : InputSample3D[Float]) extends EventResponse

class OnOffCell(filterWidth : Int, filterHeight : Int, filterFunction : (Float, Float) => Float) extends StreamConverter[InputSample2D[Float], InputSample3D[Float]] {

  override def initialize(): Unit = {
    super.initialize()
    addEvent(OnOffCellEvent)
  }

  override def dataConverter(in: InputSample2D[Float]): InputSample3D[Float] = {

    val max_values = Array.ofDim[Float](2)
    val data = for(x <- 0 until in.getShape.dimensions(0)-filterWidth+1) yield {
      for(y <- 0 until in.getShape.dimensions(1)-filterHeight+1) yield {
        for(t <- Seq(true, false)) yield {
          val v = (for{
            dx <- 0 until filterWidth
            dy <- 0 until filterHeight
          } yield {
            val curr_x = dx.toFloat-filterWidth/2
            val curr_y = dy.toFloat-filterHeight/2
            val pixel = if(t) in.getDataOnChannel(x+dx, y+dy) else 1f-in.getDataOnChannel(x+dx, y+dy)
            pixel*filterFunction(curr_x, curr_y)
          }).sum


          val index = if(t) 0 else 1

          max_values(index) = math.max(max_values(index), v)
          math.max(0f, v)
        }
      }
    }

    val sample = new InputSample3D[Float](
      data.map{
        _.map {
          _.zip(max_values.map(m => if(m <= 0) 1f else m)).map{case(v, max) => v/max}
        }
      }
    ).setMetaData(in.getMetaData:_*)

    triggerEventWith(OnOffCellEvent, OnOffCellEventResponse(sample))
/*
    println(sample)
    System.exit(0)
*/
    sample
  }

  override def shapeConverter(shape : Shape): Shape = {
    assert(shape.dimensionNumber == 2)
    Shape(shape.dimensions(0)-filterWidth+1, shape.dimensions(1)-filterHeight+1, 2)
  }

  override def resetConverter(): Unit = {}
}
