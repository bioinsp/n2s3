package fr.univ_lille.cristal.emeraude.n2s3.core.event

/**
  * Created by guille on 10/20/16.
  */
abstract class TimedEvent[Response <: TimedEventResponse] extends Event[Response]
