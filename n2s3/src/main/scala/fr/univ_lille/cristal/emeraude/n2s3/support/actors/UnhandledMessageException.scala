package fr.univ_lille.cristal.emeraude.n2s3.support.actors

/**
  * Created by guille on 10/14/16.
  */
class UnhandledMessageException(c : Class[_], message : Message)
  extends RuntimeException("Unhandled message in " + c + " :\n"
      +"Message : " + message +"\n") {
}
