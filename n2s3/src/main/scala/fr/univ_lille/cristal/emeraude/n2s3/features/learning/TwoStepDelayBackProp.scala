package fr.univ_lille.cristal.emeraude.n2s3.features.learning

import java.io.{File, PrintWriter}

import akka.actor.{Actor, Props}
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done}
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapseWeightAndDelay
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy

import scala.collection.mutable
import scala.concurrent.Await
/**
  * Created by falezp on 13/12/16.
  */
class TwoStepDelayBackProp(n2s3 : N2S3, outputs : Map[String, NetworkEntityPath], learningRate : Double, correctTargetDelay : Time, incorrectTargetDelay : Time) {

  object Close

  implicit val timeout = Config.longTimeout

  class TwoStepDelayBackPropActor(n2s3: N2S3, outputs: Map[String, NetworkEntityPath], learningRate: Double, correctTargetDelay: Time, incorrectTargetDelay: Time) extends Actor {

    val writer = new PrintWriter(new File("error"))

    val initialMembraneTimeConstant : Time = 5 MilliSecond
    val membraneTimeConstant = 5 MilliSecond
    val binaryThreshold = 0.0

    val connections = mutable.ArrayBuffer[(NetworkEntityPath, NetworkEntityPath, Timestamp, Double)]()
    val neurons = mutable.HashMap[NetworkEntityPath, (Double, Timestamp)]()

    val forwardResults = mutable.HashMap[NetworkEntityPath, (Timestamp, Double)]()

    val inputPattern = mutable.HashMap[NetworkEntityPath, Timestamp]()
    var currentInputStart : Option[Timestamp] = None
    var currentInputLabel : Option[String] = None

    var currentTimestamp : Timestamp = 0
    val currentTimestampFire = mutable.ArrayBuffer[NetworkEntityPath]()

    ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))

    n2s3.inputLayerRef.get.neuronPaths.foreach { path =>
      ExternalSender.askTo(path, SubscribeSynchronized(NeuronFireEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))
    }

    load()

    override def receive = {
      case SynchronizedEvent(s, _, m, _) =>
        m match {
          case LabelChangeResponse(start, end, label) =>
            if(start > currentTimestamp) {

              currentTimestampFire.foreach { neuron =>
                if(!inputPattern.isDefinedAt(neuron))
                  inputPattern += neuron -> currentTimestamp
              }

              currentTimestampFire.clear()
              currentTimestamp = start
            }

            if(currentInputStart.isDefined && currentInputLabel.isDefined)
              execute()

            inputPattern.clear()
            currentInputStart = Some(start)
            currentInputLabel = Some(label)

          case NeuronFireResponse(timestamp, source) =>
            if(timestamp > currentTimestamp) {

              currentTimestampFire.foreach { neuron =>
                if(!inputPattern.isDefinedAt(neuron))
                  inputPattern += neuron -> currentTimestamp
              }

              currentTimestampFire.clear()
              currentTimestamp = timestamp
            }

            currentTimestampFire += source


          case _ => throw new RuntimeException("Unrecognized message : "+m)
        }
        ExternalSender.sendTo(s, Done)
      case Close =>
        currentTimestampFire.foreach { neuron =>
          if(!inputPattern.isDefinedAt(neuron))
            inputPattern += neuron -> currentTimestamp
        }
        execute()
        save()


        ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, Unsubscribe(LabelChangeEvent, ExternalSender.getReference(self)))

        n2s3.inputLayerRef.get.neuronPaths.foreach { path =>
          ExternalSender.askTo(path, Unsubscribe(NeuronFireEvent, ExternalSender.getReference(self)))
        }
        sender ! Done
        context stop self
      case Done => sender ! Done
      case m => throw new RuntimeException("Unrecognized message : "+m)
    }

    def load() : Unit = {
      n2s3.layers.flatMap(_.neuronPaths).foreach { output_neuron =>

        neurons += output_neuron -> (1.0, initialMembraneTimeConstant.timestamp)

        (ExternalSender.askTo(output_neuron, GetAllConnectionProperty(SynapseWeightAndDelay)) match {
          case PropertyValue(values: Seq[(NetworkEntityPath, (Float, Time))]@unchecked) => values
          case _ => Seq[(NetworkEntityPath, (Float, Time))]()
        }).foreach { case (input_neuron, (weight, delay)) =>
          connections += ((input_neuron, output_neuron, delay.timestamp, weight))
        }
      }
/*
      n2s3.layers.flatMap(_.neuronPaths).foreach { output_neuron =>

      }
*/
    }

    def gaussian_kernel(x : Double, mean : Double, std : Double) : Double = {
      val a = 1.0/*1.0/(std*math.sqrt(2.0*math.Pi))*/
      a*math.exp(-(x-mean)*(x-mean))/(2.0*std*std)
    }

    def avg_score_kernel(output_neuron : NetworkEntityPath, spikes : Seq[(Timestamp, Double, Double)]) : (Timestamp, Double) = {
      val spike_kernels = spikes.zipWithIndex.map{ case((t, weight, score), index) =>
        (index, spikes.foldLeft(0.0){case(acc, (t_x, weight, score)) => acc+gaussian_kernel(t_x.toDouble, t.toDouble, neurons(output_neuron)._2.toDouble)*weight*score })
      }
      val max_kernel = spike_kernels.maxBy(_._2)
      (spikes(max_kernel._1)._1, max_kernel._2)
    }

    def execute() : Unit = {
      forward()
      backward()
      forwardResults.clear()
    }

    // only feed-forward topology
    def forward() : Unit = {
      println("#### Label = "+currentInputLabel.get+" Inputs=["+inputPattern.mkString(",")+"]")


      val next_buffer = mutable.ArrayBuffer[(NetworkEntityPath, Timestamp, Double)]()
      next_buffer ++= inputPattern.map{case(neuron, t) => (neuron, t-currentInputStart.get, 1.0)}

      while(next_buffer.nonEmpty) {
        forwardResults ++= next_buffer.map{case(neuron, t, score) => (neuron, (t, score))}
        println(next_buffer.mkString(", "))
        val process_buffer = mutable.ArrayBuffer[(NetworkEntityPath, Timestamp, Double)]()
        process_buffer ++= next_buffer
        next_buffer.clear()

        val output_timestamps = mutable.HashMap[NetworkEntityPath, mutable.ArrayBuffer[(Timestamp, Double, Double)]]()

        // forward connections
        process_buffer.foreach { case (input_neuron, t, score) =>
          connections.filter(_._1 == input_neuron).foreach { case (_, output_neuron, delay, weight) =>
            output_timestamps.getOrElseUpdate(output_neuron, mutable.ArrayBuffer[(Timestamp, Double, Double)]()) += ((t + delay, weight, score))
          }
        }

        if(output_timestamps.nonEmpty) {

          println(">"+output_timestamps.mkString(", "))

          val scores = output_timestamps.map { case (key, list) =>
            val (avg, score) = avg_score_kernel(key, list)
            (key, avg.toLong, score)
          }

          val max_score = scores.map(_._3).max

          next_buffer ++= scores.map { case (key, t, score) =>
            val normalized_score = score / max_score

            (key, t, if(normalized_score > binaryThreshold) normalized_score else 0.0)
          }
        }
      }
    }

    def backward() : Unit = {

      val neuronError = mutable.HashMap[NetworkEntityPath, Double]()
      val queue = mutable.Queue[NetworkEntityPath]()

      // TMP
      val change = mutable.HashMap[(NetworkEntityPath, NetworkEntityPath), (Double, Double, Double)]()

      var total_error = 0.0

      outputs.foreach{ case (label, neuron) =>

        if(forwardResults.isDefinedAt(neuron)) {
          val expected_delay = if (label == currentInputLabel.get) correctTargetDelay.timestamp else incorrectTargetDelay.timestamp
          val result_delay = forwardResults(neuron)._1
          val error = -(expected_delay - result_delay)
          println("E=" + expected_delay + ", R=" + result_delay + ", e=" + error)
          total_error += Time(math.abs(error)).asMilliSecond
          neuronError += neuron -> error
          queue.enqueue(neuron)
        }
      }

      writer.println(total_error)
      writer.flush()

      while(queue.nonEmpty) {
        val neuron = queue.dequeue()

        // error computation
        val neuron_error = neuronError.getOrElseUpdate(neuron, connections.filter(_._1 == neuron).foldLeft(0.0) { case (acc, (input_neurons, output_neurons, delay, weight)) =>
          acc + neuronError.getOrElse(output_neurons, 0.0)*forwardResults(input_neurons)._2
        })

        connections.filter(_._2 == neuron).foreach{ case (input_neuron, _, _, _) =>
          if(!queue.contains(input_neuron)) {
            queue.enqueue(input_neuron)
          }
        }

        // error correction
        connections.zipWithIndex.filter(_._1._2 == neuron).foreach { case ((input_neuron, output_neurons, delay, weight), index) =>
          val connection_error = -(forwardResults(output_neurons)._1-forwardResults(input_neuron)._1-delay)*forwardResults(input_neuron)._2

          change += (input_neuron, output_neurons) -> (neuron_error, connection_error, -learningRate*(neuron_error+connection_error)*forwardResults(output_neurons)._2)

          connections.update(index, (input_neuron, output_neurons, math.max(0L, delay-(learningRate*(neuron_error+connection_error)).toLong), weight))
        }
      }

      println("error : "+neuronError.mkString(", "))
      println(connections.mkString(", "))
      println(change.mkString(", "))
    }


    def save() : Unit = {
      println("Save training")

      n2s3.layers.flatMap(_.neuronPaths).foreach { output_neuron =>
        val properties = ExternalSender.askTo(output_neuron, GetAllConnectionProperty(SynapseWeightAndDelay))
          .asInstanceOf[ConnectionPropertyValues[(Float, Time)]].values.map { case (connectionId, inputNeuron, (weight, delay)) =>
          val find_connection = connections.filter(c => c._1 == inputNeuron && c._2 == output_neuron)
          val connection = if(find_connection.size == 1)
            find_connection.head
          else
            (connectionId, output_neuron, delay.timestamp, weight.toDouble)

          (connectionId, (connection._4.toFloat, Time(connection._3)))
        }
        ExternalSender.askTo(output_neuron, SetAllConnectionProperty(SynapseWeightAndDelay, properties))

        /*
        (ExternalSender.askTo(output_neuron, SetAllConnectionProperty(SynapseWeightAndDelay)) match {
          case PropertyValue(values: Seq[(NetworkEntityPath, (Float, Time))]@unchecked) => values
          case _ => Seq[(NetworkEntityPath, (Float, Time))]()
        }).foreach { case (input_neuron, (weight, delay)) =>
          connections += ((input_neuron, output_neuron, delay.timestamp, weight))
        }*/
      }
    }
  }

  val actor = n2s3.system.actorOf(Props(new TwoStepDelayBackPropActor(n2s3, outputs, learningRate, correctTargetDelay, incorrectTargetDelay)), LocalActorDeploymentStrategy)
  Await.result(actor ? Done, timeout.duration)

  def destroy() : Unit = {
    Await.result(actor ? Close, timeout.duration)
  }

}
