package fr.univ_lille.cristal.emeraude.n2s3.core.models.properties

import fr.univ_lille.cristal.emeraude.n2s3.core.Property
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import squants.electro.ElectricPotential

/**
  * Created by pfalez on 30/05/17.
  */
object AdaptiveThreshold extends Property[ElectricPotential]
object AdaptiveThresholdAdd extends Property[ElectricPotential]
object AdaptiveThresholdLeak extends Property[Time]