package fr.univ_lille.cristal.emeraude.n2s3.core.event

/***************************************************************************************************
 *  Event triggered when update arise in neuron
 **************************************************************************************************/
abstract class NeuronUpdateEvent extends Event[EventTriggered.type]