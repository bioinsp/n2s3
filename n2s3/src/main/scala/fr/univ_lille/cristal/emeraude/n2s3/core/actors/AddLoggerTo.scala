package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import akka.actor.ActorRef
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * Created by guille on 10/14/16.
  */
case class AddLoggerTo(actor : ActorRef) extends Message
