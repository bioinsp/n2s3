package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/*************************************************************************************************
*>>>>>>> a2debf0a5ca3792a035f19095d06369a403808c7
 * Base class for all the synchronization mechanism.
 ************************************************************************************************/
class SyncMessage extends Message
