package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/***********************************************************************************************
 * 	Basic class for all spike message type
 **********************************************************************************************/
abstract class Spike extends Message

/**
  * Spike send to all incoming connection when a neuron fire
  */
object BackwardSpike extends Spike
