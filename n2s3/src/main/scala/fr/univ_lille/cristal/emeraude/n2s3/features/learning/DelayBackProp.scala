package fr.univ_lille.cristal.emeraude.n2s3.features.learning

import akka.actor.{Actor, Props}
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done}
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.{MembraneLeakTime, MembraneThresholdFloat, SynapseWeightAndDelay}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.ExportNetworkTopologyChange
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy

import scala.collection.mutable
import scala.concurrent.Await

/**
  * Created by falezp on 07/12/16.
  */
class DelayBackProp(n2s3 : N2S3, outputs : Map[String, NetworkEntityPath], learningRate : Double, correctTargetDelay : Time, incorrectTargetDelay : Time) {

  object Close

  implicit val timeout = Config.longTimeout

  class DelayAdaptationActor(n2s3 : N2S3, outputs : Map[String, NetworkEntityPath], learningRate : Double, correctTargetDelay : Time, incorrectTargetDelay : Time) extends Actor {

    val fireHistory = mutable.HashMap[NetworkEntityPath, mutable.ArrayBuffer[Timestamp]]()
    var currentInputStart : Option[Timestamp] = None
    var currentInputLabel : Option[String] = None

    var currentTimestamp : Timestamp = 0
    val currentTimestampFire = mutable.ArrayBuffer[NetworkEntityPath]()

    val outputEvolution = new ExportNetworkTopologyChange(n2s3, "delay_backprop_evolution.tex")

    ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))

    n2s3.layers.flatMap(_.neuronPaths).foreach { path =>
      ExternalSender.askTo(path, SubscribeSynchronized(NeuronFireEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))
    }

    override def receive = {
      case SynchronizedEvent(s, _, m, _) =>
        m match {
          case LabelChangeResponse(start, end, label) =>
            if(start > currentTimestamp) {

              currentTimestampFire.foreach { neuron =>
                fireHistory.getOrElseUpdate(neuron, mutable.ArrayBuffer[Timestamp]()) += currentTimestamp
              }

              currentTimestampFire.clear()
              currentTimestamp = start
            }

            execute()
            fireHistory.clear()
            currentInputStart = Some(start)
            currentInputLabel = Some(label)

          case NeuronFireResponse(timestamp, source) =>
            if(timestamp > currentTimestamp) {

              currentTimestampFire.foreach { neuron =>
                fireHistory.getOrElseUpdate(neuron, mutable.ArrayBuffer[Timestamp]()) += currentTimestamp
              }

              currentTimestampFire.clear()
              currentTimestamp = timestamp
            }

            currentTimestampFire += source


          case _ => throw new RuntimeException("Unrecognized message : "+m)
        }
        ExternalSender.sendTo(s, Done)
      case Close =>
        currentTimestampFire.foreach { neuron =>
          fireHistory.getOrElseUpdate(neuron, mutable.ArrayBuffer[Timestamp]()) += currentTimestamp
        }
        execute()
        ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, Unsubscribe(LabelChangeEvent, ExternalSender.getReference(self)))

        n2s3.layers.flatMap(_.neuronPaths).foreach { path =>
          ExternalSender.askTo(path, Unsubscribe(NeuronFireEvent, ExternalSender.getReference(self)))
        }
        sender ! Done
        context stop self
      case Done => sender ! Done
      case m => throw new RuntimeException("Unrecognized message : "+m)
    }

    def execute() : Unit = {

      if(currentInputStart.isEmpty || currentInputLabel.isEmpty)
        return
      // max margin
      /*
      val min = outputFire match {
        case l if l.isEmpty => None
        case l => Some(l.minBy(_._2))
      }

      outputs.foreach { case(label, neuron) =>
        if(outputFire.isDefinedAt(neuron))
          println("["+label+"] : "+outputFire(neuron)+(if(min.isDefined && min.get._1 == neuron) " Winner" else ""))
        else
          println("["+label+"] : None")
      }
    }
    */

      val change = mutable.ArrayBuffer[(NetworkEntityPath, NetworkEntityPath, Double)]()

      val queue = mutable.Queue[(NetworkEntityPath, Timestamp)]()
      val forwardConnection = mutable.HashMap[NetworkEntityPath, mutable.ArrayBuffer[(NetworkEntityPath, Double)]]()
      val neuronError = mutable.HashMap[NetworkEntityPath, (Double, Boolean)]()

      def add_connection(from : NetworkEntityPath, to : NetworkEntityPath, atFire : Timestamp, delay : Time) : Option[(Timestamp, Double)] = {
        val list = forwardConnection.getOrElseUpdate(from, mutable.ArrayBuffer[(NetworkEntityPath, Double)]())

        if(!list.exists(_._1 == to) && fireHistory.isDefinedAt(from)) {
          val pre_spike_list = fireHistory(from).filter(_ + delay.timestamp <= atFire)
          if(pre_spike_list.nonEmpty) {
            val tau_leak = ExternalSender.askTo(to, GetProperty(MembraneLeakTime)) match {
              case PropertyValue(v: Time) => v
              case _ => Time(1)
            }

            val t_pre = pre_spike_list.max + delay.timestamp
            val impact_factor = math.exp(-(atFire - t_pre).toDouble / tau_leak.timestamp.toDouble)
            list += to -> impact_factor

            Some(pre_spike_list.max, impact_factor)
          }
          else None
        }
        else None
      }

      println("#### "+fireHistory.mkString(", "))
      println("Label = "+currentInputLabel.get+" Inputs=["+n2s3.inputLayerRef.get.neuronPaths.map(n => fireHistory.getOrElse(n, Seq()).map(_-currentInputStart.get).mkString("/")).mkString(", ")+"]")
      println("Hidden>"+n2s3.layers(1).neuronPaths.filter(n => fireHistory.isDefinedAt(n)).map(n => (n, fireHistory(n).min-currentInputStart.get)).mkString(", "))

      // min t_fire - t_pre
      outputs.foreach { case (label, neuron) =>
        //optimize_neuron_fire(neuron)

        val tau_leak = ExternalSender.askTo(neuron, GetProperty(MembraneLeakTime)) match {
          case PropertyValue(v: Time) => v
          case _ => Time(1)
        }

        if (!fireHistory.isDefinedAt(neuron)) {
          println("No spike activity...")

          //decrease threshold of each silent neuron
          n2s3.layers.flatMap(_.neuronPaths).filter(n => !fireHistory.isDefinedAt(n)).foreach { n =>
            ExternalSender.askTo(n, GetProperty(MembraneThresholdFloat)) match {
              case PropertyValue(_) => change_threshold(n, 0.9f)
              case _ =>
            }

          }
        }
        else {
          val expected_delay = if (label == currentInputLabel.get) correctTargetDelay.timestamp else incorrectTargetDelay.timestamp
          val result_timestamp = fireHistory(neuron).min
          val result_delay = result_timestamp-currentInputStart.get

          val error = -(expected_delay-result_delay)
          println("E="+expected_delay+", R="+result_delay+", e="+error)

          val apply_local_change =  label == currentInputLabel.get
          neuronError += neuron -> (error.toDouble, apply_local_change)

          val result = ExternalSender.askTo(neuron, GetAllConnectionProperty(SynapseWeightAndDelay))
            .asInstanceOf[ConnectionPropertyValues[(Float, Time)]].values.map { case (connectionId, inputNeuron, (weight, delay)) =>
            if(!queue.exists(_._1 == inputNeuron) && fireHistory.isDefinedAt(inputNeuron))
              queue.enqueue((inputNeuron, fireHistory(inputNeuron).min))

            val influence =  add_connection(inputNeuron, neuron, result_timestamp, delay)

            if(influence.isDefined) {
              val t_pre = fireHistory(inputNeuron).filter(_ + delay.timestamp <= result_timestamp).max + delay.timestamp

              val error_local_delay = if(apply_local_change) -(result_timestamp-t_pre)*0.5 else 0.0
              val n_delay = math.max(0L, delay.timestamp - (learningRate * (error + error_local_delay)).toLong)

              if(n_delay != delay.timestamp)
                change += ((inputNeuron, neuron, Time(n_delay-delay.timestamp).asMilliSecond))

              println(inputNeuron+" -> "+neuron+" : current_error="+error+", influence="+influence.get._2+", error_global_delay="+error+", error_local_delay="+error_local_delay+" | "+
                delay.asMilliSecond+" -> "+Time(n_delay).asMilliSecond+" ("+Time(n_delay-delay.timestamp).asMilliSecond+") [O]"
              )

              (connectionId, (weight, Time(n_delay)))
            }
            else (connectionId, (weight, delay))
          }

          ExternalSender.askTo(neuron, SetAllConnectionProperty(SynapseWeightAndDelay, result))
        }
      }


      while(queue.nonEmpty) {
        val (current_neuron, current_fire_t) = queue.dequeue()

        /*
          Check output computed
         */
          //val current_error = neuronError(current_neuron)

          //println(current_neuron + " " + current_fire_t)
          val current_error = forwardConnection.getOrElse(current_neuron, Seq()).foldLeft(0.0) { case (acc, (output_neurons, factor)) =>
            acc + neuronError.getOrElse(output_neurons, (0.0, false))._1*factor
          }

          val apply_local_change = forwardConnection.getOrElse(current_neuron, Seq()).exists{ case (output_neurons, _) =>
            neuronError.getOrElse(output_neurons, (0.0, false))._2
          }

          neuronError += current_neuron -> (current_error, apply_local_change)

          //println(">" + current_neuron + " = "+current_error)

          val input_synapses = ExternalSender.askTo(current_neuron, GetAllConnectionProperty(SynapseWeightAndDelay))
            .asInstanceOf[ConnectionPropertyValues[(Float, Time)]].values

          val tau_leak = ExternalSender.askTo(current_neuron, GetProperty(MembraneLeakTime)) match {
            case PropertyValue(v: Time) => v
            case _ => Time(1)
          }

          // TMP
         /* val s = (ExternalSender.askTo(current_neuron, GetAllConnectionProperty(SynapseWeightAndDelay)) match {
            case PropertyValue(values: Seq[(NetworkEntityPath, (Float, Time))]@unchecked) => values
            case _ => Seq[(NetworkEntityPath, (Float, Time))]()
          }).map{ case (input_neuron, (w, d)) =>
            val (v, t) = if (fireHistory.isDefinedAt(input_neuron) && fireHistory(input_neuron).exists(_ + d.timestamp <= current_fire_t)) {
              val t_pre = fireHistory(input_neuron).filter(_ + d.timestamp <= current_fire_t).max + d.timestamp
              (math.exp(-(current_fire_t - t_pre).toDouble / tau_leak.timestamp.toDouble), current_fire_t - t_pre)
            }
            else (0.0, -1L)
            (input_neuron, v, t)
          }
          val th = ExternalSender.askTo(current_neuron, GetProperty(MembraneThresholdFloat)) match {
            case PropertyValue(v: Float) => v
            case _ => 0
          }*/
          //println(current_neuron+" : ["+s.mkString(", ")+"]="+s.foldLeft(0.0){case(acc, (_, v, _)) => acc+v}+" "+th)
          // END TMP

          val result = input_synapses.map { case (connectionId, inputNeuron, (weight, delay)) =>

            add_connection(inputNeuron, current_neuron, current_fire_t, delay)

            if (fireHistory.isDefinedAt(inputNeuron) && fireHistory(inputNeuron).exists(_ + delay.timestamp <= current_fire_t)) {
              val t_pre = fireHistory(inputNeuron).filter(_ + delay.timestamp <= current_fire_t).max + delay.timestamp

              val influence = math.exp(-(current_fire_t - t_pre).toDouble / tau_leak.timestamp.toDouble)

              val error_local_delay = if(apply_local_change) -(current_fire_t-t_pre)*0.5 else 0.0

              val n_delay = math.max(0L, delay.timestamp - (learningRate * (0/*current_error*/ + error_local_delay)).toLong)

              if(n_delay != delay.timestamp)
                change += ((inputNeuron, current_neuron, Time(n_delay-delay.timestamp).asMilliSecond))

                println(inputNeuron+" -> "+current_neuron+" : current_error="+current_error+", influence="+influence+", error_global_delay="+current_error+", error_local_delay="+error_local_delay+" ("+(current_fire_t-t_pre)+") | "+
                  delay.asMilliSecond+" -> "+Time(n_delay).asMilliSecond+" ("+Time(n_delay-delay.timestamp).asMilliSecond+")  [H]"
                )

              if (!queue.exists(_._1 == inputNeuron) && fireHistory.isDefinedAt(inputNeuron))
                queue.enqueue((inputNeuron, t_pre - delay.timestamp))

              (connectionId, (weight, Time(n_delay)))

            }
            else (connectionId, (weight, delay))
          }


          ExternalSender.askTo(current_neuron, SetAllConnectionProperty(SynapseWeightAndDelay, result))
          //println(neuron+" : "+Time(margin).asMilliSecond)
        //}

      }

      println(change.map{case(from, to, delta) => from+"->"+to+" : "+delta}.mkString("\n"))

      //outputEvolution.next()

    }


    def optimize_neuron_fire(neuron : NetworkEntityPath) : Unit = {
      println("optimize_neuron_fire : "+neuron)

      if(!fireHistory.isDefinedAt(neuron)) {
        println("No spike activity...")

        change_threshold(neuron, -0.1f)

        // decrease threshold

        return
      }

      val input_synapses = ExternalSender.askTo(neuron, GetAllConnectionProperty(SynapseWeightAndDelay))
        .asInstanceOf[ConnectionPropertyValues[(Float, Time)]].values
      //first spike timestamp
      val t_fire = fireHistory(neuron).min

      println("Delay fire : "+Time(t_fire-currentInputStart.get).asMilliSecond)

      val tau_leak = ExternalSender.askTo(neuron, GetProperty(MembraneLeakTime)) match {
        case PropertyValue(v : Time) => v
      }

      val result = input_synapses.map { case (connectionId, inputNeuron, (weight, delay)) =>
        if(fireHistory.isDefinedAt(inputNeuron)) {
          val t_pre = fireHistory(inputNeuron).minBy(t => math.abs(t_fire - t))+delay.timestamp

          println(inputNeuron+" -> "+neuron+" : "+delay.asMilliSecond+", t_pre="+t_pre+", t_fire="+t_fire+" delta_t="+Time(t_fire - t_pre).asMilliSecond)


          if(t_pre > t_fire) {
            val involved_factor = math.exp(-(t_pre - t_fire).toDouble/tau_leak.timestamp.toDouble)
            change_threshold(neuron, (involved_factor*learningRate).toFloat)
            (connectionId, (weight, delay))
          }
          else {
            val involved_factor = math.exp(-(t_fire - t_pre).toDouble/tau_leak.timestamp.toDouble)
            val delta_d = math.min(t_fire - t_pre, (involved_factor*learningRate*(t_fire - t_pre)).toLong)

            println(delay.asMilliSecond+" -> "+Time(delay.timestamp+delta_d).asMilliSecond+" ("+Time(delta_d).asMilliSecond+")")
            (connectionId, (weight, Time(delay.timestamp+delta_d)))
          }

        }
        else {
          (connectionId, (weight, delay))
        }
      }

      ExternalSender.askTo(neuron, SetAllConnectionProperty(SynapseWeightAndDelay, result))
    }
  }

  def change_threshold(neuron : NetworkEntityPath, factor : Float) = {
    val th = ExternalSender.askTo(neuron, GetProperty(MembraneThresholdFloat)) match {
      case PropertyValue(v : Float) => v
    }
    ExternalSender.askTo(neuron, SetProperty(MembraneThresholdFloat, th*factor))

    println("Change th of "+neuron+" : "+th+" -> "+(th*factor)+" ("+(th-th*factor)+")")
  }

  val actor = n2s3.system.actorOf(Props(new DelayAdaptationActor(n2s3, outputs, learningRate, correctTargetDelay, incorrectTargetDelay)), LocalActorDeploymentStrategy)
  Await.result(actor ? Done, timeout.duration)

  def destroy() : Unit = {
    Await.result(actor ? Close, timeout.duration)
  }
}