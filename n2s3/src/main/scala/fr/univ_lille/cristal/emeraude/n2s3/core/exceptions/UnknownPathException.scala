package fr.univ_lille.cristal.emeraude.n2s3.core.exceptions

import fr.univ_lille.cristal.emeraude.n2s3.core.NetworkEntityPath

/**
  * Created by falezp on 02/06/16.
  */

/**
  * Exception throw when a NetworkEntityPath target can't be found
  *
  * @param path which is unknown
  */
class UnknownPathException(val path: NetworkEntityPath) extends RuntimeException("Unknown path \""+
  path.local.foldLeft(if(path.actor != null) path.actor.path.name+":" else "")((acc, curr) => acc+"/"+curr.toString)+"\"") {
  def this(local : Traversable[Any]) = this(NetworkEntityPath(null, local))
}

