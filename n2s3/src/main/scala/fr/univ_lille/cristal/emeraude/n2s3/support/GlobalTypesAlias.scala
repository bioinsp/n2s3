package fr.univ_lille.cristal.emeraude.n2s3.support

/**********************************************************************************************************
 * Global types alias
 **********************************************************************************************************/
object GlobalTypesAlias {
  type Timestamp = Long
  type ConnectionId = Int
}
