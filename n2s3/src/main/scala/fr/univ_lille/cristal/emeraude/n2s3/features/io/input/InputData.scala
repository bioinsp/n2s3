package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

/**
  * Created by falezp on 06/02/17.
  */


trait InputMetaData[T] {
  var metaDataContainer : InputMetaDataContainer[T]

  protected def copyMetaData(from : InputMetaDataContainer[T]) : this.type = setMetaData(from.getMetaData:_*)

  def getMetaData : Seq[T] = metaDataContainer.getMetaData
  def setMetaData(data : T*) : this.type = {
    metaDataContainer = metaDataContainer.setMetaData(data)
    this
  }
  def mapMetaData(f : T => T) : this.type = {
    metaDataContainer = metaDataContainer.mapMetaData(f)
    this
  }
}

class InputMetaDataContainer[T](data : Seq[T] = Seq[T]()) {
  def getMetaData : Seq[T] = data
  def setMetaData(metaData : Seq[T]) : InputMetaDataContainer[T] = new InputMetaDataContainer(metaData)
  def mapMetaData(f : T => T) : InputMetaDataContainer[T] = new InputMetaDataContainer(data.map(m => f(m)))
}

abstract class InputPacket {

  type InputDataType
  type InputMetaDataType

  protected var shape : Shape = Shape()

  def getShape : Shape = shape
  def withShape(s : Shape) : this.type = {
    shape = s
    this
  }


  def getDataOnChannel(index : Int*) : InputDataType
  def getAllData : Map[Seq[Int], InputDataType]

  def getMetaData : Seq[InputMetaDataType]

  def mapData(f : (Seq[Int], InputDataType) => InputDataType) : this.type
  def mapDataWithShape(shape : Shape, f : (Seq[Int], InputDataType) => (Seq[Int], InputDataType)) : this.type

  def flatMapData(f : (Seq[Int], InputDataType) => Seq[InputDataType]) : this.type
  def flatMapDataWithShape(shape : Shape, f : (Seq[Int], InputDataType) => Seq[(Seq[Int], InputDataType)]) : this.type

  def setAllData(data : Seq[(Seq[Int], InputDataType)]) : this.type
  def setAllDataWithShape(shape : Shape, data : Seq[(Seq[Int], InputDataType)]) : this.type

  def mapMetaData(f : InputMetaDataType => InputMetaDataType) : this.type

  override def equals(o: scala.Any): Boolean = o match {
    case t : InputPacket => t.getShape == getShape  && t.getAllData == getAllData && t.getMetaData == getMetaData
    case _ => false
  }

  override def toString: String = "InputPacket("+getAllData.map(e => "["+e._1.mkString(";")+"]:{"+e._2.toString+"}").mkString(", ")+")"
}

trait InputSeqDataPacket extends InputPacket {
  type InputUnitDataType
  override type InputDataType = Seq[InputUnitDataType]

  def mapUnitData(f : (Seq[Int], InputUnitDataType) => InputUnitDataType) : this.type
  def mapUnitDataWithShape(nShape : Shape, f : (Seq[Int], InputUnitDataType) => (Seq[Int], InputUnitDataType)) : this.type
}