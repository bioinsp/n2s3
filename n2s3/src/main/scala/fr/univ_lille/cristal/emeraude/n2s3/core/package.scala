/************************************************************************************************************
 * Contributors: 
 * 		-  wgouzer & qbailleul
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3

/********************************************************************************************************
 * The main elements of this simulator, we've defined abstract classes for the network,
 * neurons, synapses and spikes ; the user will then extends them with its models of neurons, synapses 
 * and network.
 *******************************************************************************************************/
package object core
