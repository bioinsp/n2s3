package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import java.awt.{Color, Dimension, FlowLayout, Graphics}

import javax.swing.{JFrame, JPanel, WindowConstants}
import akka.actor.{ActorRef, Props}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapticWeightFloat
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupObserverRef}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{LocalActorDeploymentStrategy, Message}

/**
  * Created by falezp on 22/03/17.
  */

class SynapticWeightSelectGraphRef(list : Seq[Seq[Seq[ConnectionPath]]], colorFunction : Float => Color, synapseSize : Int = 4, refreshRate : Int = 1000/24, name : String = "") extends NeuronGroupObserverRef {
  var actor: Option[ActorRef] = None
  def getActors : Seq[ActorRef] = this.actor.toSeq


  override def deploy(n2s3: N2S3): Unit = {
    this.actor = Some(n2s3.system.actorOf(Props(new SynapticWeightSelectGraph(list, colorFunction, synapseSize, refreshRate, name)), LocalActorDeploymentStrategy))
  }
}

object SynapticWeightSelectGraph {

  def generateDegradeColor(value : Float) : Color = {
    if(value < 0f)
      Color.BLACK
    else if(value > 1f)
      Color.WHITE
    else {
      val r = math.max(0f, -2f*value*value+4f*value-1f)
      val b = math.max(0f, -2f*value*value+1)
      val g = math.max(0f, 4f*value*(-value+1f))
      new Color(r, b, g)
    }
  }

  def hsl(h : Float, s : Float, l : Float) : Color = {
    val c = ((1f-math.abs(2f*l-1))*s)
    val x = (c*(1-math.abs((h/60)%2f-1)))
    val m = l-c/2f
    if(h < 60)
      new Color(c, x, 0)
    else if(h < 120)
      new Color(x, c, 0)
    else if(h < 180)
      new Color(0, c, x)
    else if(h < 240)
      new Color(0, x, c)
    else if(h < 300)
      new Color(c, 0, x)
    else
      new Color(x, 0, c)
  }

  def heatMap(value : Float) : Color = {
    if(value == 0f)
      new Color(0, 0, 255)
    else if(value < 0f)
      new Color(0, 0, 0)
    else if(value > 1f)
      new Color(255, 255, 255)
    else
      hsl((1f-value)*240f, 1f, 0.5f)
  }

  def grayScale(value : Float) : Color = {
    if(value < 0f)
      Color.BLACK
    else if(value > 1f)
      Color.WHITE
    else {
      new Color(value, value, value)
    }
  }

  def pos_neg(value : Float) : Color = {
    if(value < -1f)
      new Color(0, 0, 0)
    else if(value <= 0f)
      new Color(255+(value*255f).toInt, 255+(value*255f).toInt, 255)
    else if(value > 1f)
      new Color(0, 0, 0)
    else
      new Color(255, 255-(value*255f).toInt, 255-(value*255f).toInt)
  }


  def paint(list : Iterable[(Int, Int, Float)], g : Graphics, factor : Int = 1, color : Float => Color = heatMap): Unit = {
    list.foreach{ case(x, y, value) =>
      g.setColor(color(value))
      g.fillRect(x * factor, y * factor, factor, factor)
    }
  }
}

class SynapticWeightSelectGraph(list : Seq[Seq[Seq[ConnectionPath]]], colorFunction : Float => Color, synapseSize : Int = 4, refreshRate : Int = 1000/24, name : String) extends AutoRefreshNetworkActor(refreshRate, list.size) {

  class WeightPanel(list : Seq[Seq[ConnectionPath]]) extends JPanel {

    val synapseList = list.zipWithIndex.flatMap{case(l, x) => l.zipWithIndex.map{case(p, y) => (x, y, p)}}.groupBy(_._3.outputNeuron)
    val xMax = synapseList.flatMap(_._2.map(_._1)).max
    val yMax = synapseList.flatMap(_._2.map(_._2)).max

    setPreferredSize(new Dimension((xMax+1)*synapseSize, (yMax+1)*synapseSize))
    setMinimumSize(new Dimension((xMax+1)*synapseSize, (yMax+1)*synapseSize))

    var values = synapseList.flatMap(_._2.map(e => (e._1, e._2, 0f)))

    def update(): Unit = {
      values = synapseList.flatMap{ case(neuron, connectionList) =>
        if(connectionList.size == 1) {
          Some((connectionList.head._1, connectionList.head._2,
            ExternalConnectionSender.askTo(connectionList.head._3, GetConnectionProperty(SynapticWeightFloat))
              .asInstanceOf[PropertyValue[Float]].value))
        }
        else {
          val values = ExternalSender.askTo(neuron, GetAllConnectionProperty(SynapticWeightFloat))
            .asInstanceOf[ConnectionPropertyValues[Float]].values.groupBy(_._1)
          connectionList.map{case(x, y, p) => (x, y, values(p.connectionID).head._3)}
        }
      }
    }

    override def paintComponent(g: Graphics): Unit = {
      SynapticWeightSelectGraph.paint(values, g, synapseSize, colorFunction)
    }

  }

  val frame = new JFrame()
  frame.setLayout(new FlowLayout())

  val panels : Seq[WeightPanel] = list.map{ l =>
    val panel = new WeightPanel(l)
    frame.add(panel)
    panel
  }

  frame.setTitle(name)

  frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
  frame.validate()
  //frame.setSize(600, 400)
  frame.pack()
  frame.setVisible(true)

  override def initialize(): Unit = {

  }

  override def destroy(): Unit = {

  }

  override def process(message: Message, sender : ActorRef): Unit = {

  }

  override def update(n: Int): Unit = {
    panels(n).update()
    panels(n).repaint()
    panels(n).revalidate()
  }
}
