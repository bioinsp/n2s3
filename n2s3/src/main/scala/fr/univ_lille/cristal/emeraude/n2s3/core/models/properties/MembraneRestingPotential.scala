package fr.univ_lille.cristal.emeraude.n2s3.core.models.properties

import fr.univ_lille.cristal.emeraude.n2s3.core.Property
import squants.electro.ElectricPotential

/**
  * Created by falezp on 18/11/16.
  */

/**
  * Property used to represent the resting potential of the neuron membrane
  * @tparam T is the resting potential value type
  */
class MembraneRestingPotential[T] extends Property[T]

//
//  Type specializations
//
object MembraneRestingPotential extends MembraneRestingPotential[ElectricPotential]
