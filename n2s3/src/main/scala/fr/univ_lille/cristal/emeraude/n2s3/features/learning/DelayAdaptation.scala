package fr.univ_lille.cristal.emeraude.n2s3.features.learning

import akka.actor.{Actor, Props}
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors._
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapseWeightAndDelay
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias._
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy

import scala.collection.mutable
import scala.concurrent.Await
/**
  * Created by falezp on 26/10/16.
  */

/*
 * From "A supervised learning approach based on STDP and polychronization in spiking neuron network"
 * http://publications.idiap.ch/downloads/papers/2007/paugam-esann-2007.pdf
 */

class DelayAdaptation(n2s3 : N2S3, outputs : Map[String, NetworkEntityPath], learningRate : Double) {

  object Close

  implicit val timeout = Config.longTimeout

  class DelayAdaptationActor(n2s3 : N2S3, outputs : Map[String, NetworkEntityPath], learningRate : Double) extends Actor {

    val inputQueue = mutable.Queue[(String, Timestamp)]()
    val outputFire = mutable.HashMap[NetworkEntityPath, Timestamp]()
    val hiddenFire = mutable.HashMap[NetworkEntityPath, mutable.ArrayBuffer[Timestamp]]()

    ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))

    n2s3.layers.flatMap(_.neuronPaths).foreach { path =>
      ExternalSender.askTo(path, SubscribeSynchronized(NeuronFireEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))
    }

    override def receive = {
      case SynchronizedEvent(s, _, m, _) =>
        m match {
          case LabelChangeResponse(start, end, label) =>
            execute()

            inputQueue.clear()
            outputFire.clear()
            hiddenFire.clear()

            inputQueue.enqueue(label -> start)
          case NeuronFireResponse(timestamp, source) =>
            if(inputQueue.nonEmpty && timestamp >= inputQueue.front._2) {
              if (outputs.exists(_._2 == source)) {
                if (!outputFire.isDefinedAt(source))
                  outputFire += source -> timestamp
              } else {
                if (!hiddenFire.isDefinedAt(source))
                  hiddenFire += source -> mutable.ArrayBuffer[Timestamp]()

                hiddenFire(source) += timestamp
              }
            }
          case _ => throw new RuntimeException("Unrecognized message : "+m)
        }
        ExternalSender.sendTo(s, Done)
      case Close =>
        execute()
        ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, Unsubscribe(LabelChangeEvent, ExternalSender.getReference(self)))

          n2s3.layers.flatMap(_.neuronPaths).foreach { path =>
        ExternalSender.askTo(path, Unsubscribe(NeuronFireEvent, ExternalSender.getReference(self)))
      }
      sender ! Done
      context stop self
      case Done => sender ! Done
      case m => throw new RuntimeException("Unrecognized message : "+m)
    }

    def execute() : Unit = {
      val min_margin = 10 MilliSecond

      if(inputQueue.isEmpty || outputFire.size < 2 || !outputFire.isDefinedAt(outputs(inputQueue.front._1)))
        return

      val current_label = inputQueue.dequeue()
      val margin = outputFire.filterKeys(_ != outputs(current_label._1)).minBy(_._2)._2-outputFire(outputs(current_label._1))

      //println(current_label._1+" : "+outputFire.map(o => o._1+" => "+(o._2-current_label._2)).mkString(", "))


      if(margin < min_margin.timestamp) {

        outputs.filter(f => outputFire.isDefinedAt(f._2)).foreach { case (key, neuron_path) =>
          val current_timing = outputFire(neuron_path)

          val current_synapses = ExternalSender.askTo(neuron_path, GetAllConnectionProperty(SynapseWeightAndDelay))
            .asInstanceOf[ConnectionPropertyValues[(Float, Time)]].values

          val timings = current_synapses.flatMap { case (connectionId, inputNeuron, (w, d)) =>
            hiddenFire.getOrElse(inputNeuron, Seq()).map { timestamp =>
              (connectionId, timestamp + d.timestamp)
            }
          }.filter(_._2 <= current_timing)

          if (timings.nonEmpty) {

            val max_timing = timings.maxBy(_._2)

            ExternalSender.askTo(neuron_path, SetAllConnectionProperty(SynapseWeightAndDelay, current_synapses.map { case (connectionId, inputNeuron, (w, d)) =>
              val n_d = if (hiddenFire.getOrElse(inputNeuron, Seq()).exists(_ + d.timestamp == max_timing._2)) {
                Time(math.max(0, d.timestamp + (if (outputs(current_label._1) == neuron_path) -(learningRate MilliSecond).timestamp else (learningRate MilliSecond).timestamp)))
              }
              else
                d
              (connectionId, (w, n_d))
            }))
          }
        }
      }
    }
  }

  val actor = n2s3.system.actorOf(Props(new DelayAdaptationActor(n2s3, outputs, learningRate)), LocalActorDeploymentStrategy)
  Await.result(actor ? Done, timeout.duration)

  def destroy() : Unit = {
    Await.result(actor ? Close, timeout.duration)
  }
}