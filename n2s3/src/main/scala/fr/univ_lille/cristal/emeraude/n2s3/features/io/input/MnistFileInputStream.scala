package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import java.io.{DataInputStream, FileInputStream, IOException}

/**
 * Class Reader for mnist file
 */

object InputMnist extends InputFormat[InputSample2D[Float]](Shape(28, 28)) {
  def DataFrom(imageFile: String, labelFile: String, sizeOfChunk: Int  = 64) = new MnistFileInputStream(imageFile, labelFile, sizeOfChunk)
}

object MnistFileInputStream{
  def apply(imageFile: String, labelFile: String, sizeOfChunk: Int  = 64) = new MnistFileInputStream(imageFile, labelFile, sizeOfChunk)
}

class MnistFileInputStream(val imageFile: String, val labelFile: String, val sizeOfChunk: Int  = 64) extends InputGenerator[InputSample2D[Float]] {


  //Magic number
  val MAGIC_IMAGE = 2051
  val MAGIC_LABEL = 2049

  //stream of file
  var labelsInputStream = new DataInputStream(new FileInputStream(labelFile))
  var imagesInputStream = new DataInputStream(new FileInputStream(imageFile))

  //Check the magic number
  if (labelsInputStream.readInt() != MAGIC_LABEL)
    throw new IOException("Error on magic number for label file: [" + labelFile + "]")

  if (imagesInputStream.readInt != MAGIC_IMAGE)
    throw new IOException("Error on magic number for image file: [" + imageFile + "]")

  //check the number of elements
  val numberImage = imagesInputStream.readInt()
  val numberLabel = labelsInputStream.readInt()
  private var numberOfReadImages: Int = 0

  var cursor = 0

  if (numberImage != numberLabel)
    throw new IOException("The number of images contained in: [" + imageFile + "] does not match the number of label [" + labelFile + "]")

  val rows = imagesInputStream.readInt()
  val columns = imagesInputStream.readInt()

  val imagesBuffer = Array.ofDim[Double](sizeOfChunk, rows, columns)
  val label = Array.ofDim[Double](sizeOfChunk)

  def next(): InputSample2D[Float] = {

    if (this.numberOfReadImages >= this.numberLabel) {
      throw new IOException("Attempt to read after the end of the file")
    }


    val label = labelsInputStream.readUnsignedByte().toString

    val pixels = for(y <- 0 until columns) yield {
      for(x <- 0 until rows) yield imagesInputStream.readUnsignedByte().toFloat
    }

    val image = for(x <- 0 until rows) yield {
      for(y <- 0 until columns) yield pixels(y)(x) / 255f
    }
    this.numberOfReadImages += 1

    new InputSample2D[Float](image).setMetaData(InputLabel(label))
  }

  def atEnd(): Boolean = this.numberOfReadImages >= this.numberImage

  def reset() = {
    labelsInputStream = new DataInputStream(new FileInputStream(labelFile))
    labelsInputStream.readInt()
    labelsInputStream.readInt()
    imagesInputStream = new DataInputStream(new FileInputStream(imageFile))
    imagesInputStream.readInt()
    imagesInputStream.readInt()
    imagesInputStream.readInt()
    imagesInputStream.readInt()
    cursor = 0
    numberOfReadImages = 0
  }

  def shape: Shape = Shape(columns, rows)

  override def toString = "[Mnist] "+imageFile+" : "+ numberOfReadImages + " / " + numberImage
}
