package fr.univ_lille.cristal.emeraude.n2s3.core.event

import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

/**
  * Created by guille on 10/20/16.
  */
abstract class TimedEventResponse extends EventResponse {
  val timestamp : Timestamp
}
