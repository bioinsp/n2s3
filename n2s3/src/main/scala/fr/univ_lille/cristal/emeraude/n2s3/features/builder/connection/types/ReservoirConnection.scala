package fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types

import fr.univ_lille.cristal.emeraude.n2s3.core.{NetworkEntityPath, NeuronConnection}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.{Connection, ConnectionPolicy}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{NeuronGroupRef, NeuronRef}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.Shape

import scala.util.Random

/**
  * Created by falezp on 27/10/16.
  */
class ReservoirConnection(rate : Float, connectionConstructor : Float => NeuronConnection) extends ConnectionPolicy {
  override def generate(from: NeuronGroupRef, to: NeuronGroupRef): Traversable[Connection] = {
    if(from != to)
      throw new RuntimeException("Reservoir need to be connected to the same layer")
    createConnection(from.neuronPaths, from.shape)
  }

  def createConnection(group : Seq[NetworkEntityPath], shape : Shape) : Traversable[Connection] = {

    def computeDistance(index1 : Seq[Int], index2 : Seq[Int]) : Double = {
      math.sqrt(index1.zip(index2).foldLeft(0.0){case (acc, (i1, i2)) => acc+(i1-i2)*(i1-i2)})
    }

    for {
      i <- shape.allIndex
      o <- shape.allIndex
      if i != o && Random.nextFloat() <= rate*math.exp(-computeDistance(i, o)/5.0)
    } yield {
      Connection(group(shape.toIndex(i:_*)), group(shape.toIndex(o:_*)), Some(connectionConstructor(computeDistance(i, o).toFloat)))
    }
  }

  /** ******************************************************************************************************************
    * Testing
    * *****************************************************************************************************************/
  override def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = true
}

class ReservoirConnection2(synapsePerNeuron : Int, connectionConstructor : Float => NeuronConnection) extends ConnectionPolicy {
  override def generate(from: NeuronGroupRef, to: NeuronGroupRef): Traversable[Connection] = {
    if(from != to)
      throw new RuntimeException("Reservoir need to be connected to the same layer")
    createConnection(from.neuronPaths, from.shape)
  }

  def createConnection(group : Seq[NetworkEntityPath], shape : Shape) : Traversable[Connection] = {

    def computeDistance(index1 : Seq[Int], index2 : Seq[Int]) : Double = {
      math.sqrt(index1.zip(index2).foldLeft(0.0){case (acc, (i1, i2)) => acc+(i1-i2)*(i1-i2)})
    }

    for {
      (input_shape, input_index) <- shape.allIndex.zipWithIndex
      output_index <- 0 until synapsePerNeuron
    } yield {
      val output_index_r = Random.nextInt(group.size-2)
      val output_shape = if(output_index_r >= input_index) output_index_r+1 else output_index_r
      Connection(group(shape.toIndex(input_shape:_*)), group(output_shape), Some(connectionConstructor(computeDistance(input_shape, shape.allIndex(output_shape)).toFloat)))
    }
  }

  /** ******************************************************************************************************************
    * Testing
    * *****************************************************************************************************************/
  override def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = true
}

class ReservoirConnectionExcInh(rate : Float, isExc : (Shape, Seq[Int], Seq[Int]) => Boolean, excConnectionConstructor : () => NeuronConnection, inhConnectionConstructor : () => NeuronConnection) extends ConnectionPolicy {
  override def generate(from: NeuronGroupRef, to: NeuronGroupRef): Traversable[Connection] = {
    if(from != to)
      throw new RuntimeException("Reservoir need to be connected to the same layer")
    createConnection(from.neuronPaths, from.shape)
  }

  def createConnection(group : Seq[NetworkEntityPath], shape : Shape) : Traversable[Connection] = {

    for {
      i <- shape.allIndex
      o <- shape.allIndex
      if i != o && Random.nextFloat() <= rate
    } yield {
      Connection(group(shape.toIndex(i:_*)), group(shape.toIndex(o:_*)), Some(
        if(isExc(shape, i, o))
          excConnectionConstructor()
        else
          inhConnectionConstructor()
      ))
    }
  }

  /** ******************************************************************************************************************
    * Testing
    * *****************************************************************************************************************/
  override def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = true
}
