package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message
/**
  * Created by pfalez on 24/05/17.
  */
case class ForecastFire(id : Int) extends Message