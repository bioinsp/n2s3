/************************************************************************************************************
 * Contributors: 
 * 		- created by Pierre Falez
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.core

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{Connection, NeuronEnds, NeuronMessage}
import fr.univ_lille.cristal.emeraude.n2s3.core.event.{Event, EventResponse}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.{ConnectionId, Timestamp}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * NeuronConnection Companion object
  */
object NeuronConnection {

  /**
    * Class used to manage communication with input and output neurons of a connection
    *
    * @param connection is the current connection object
    * @param neuron is the output neuron, which contains the connection
    * @param connectionId is the id of the current connection
    */
  class ConnectionEnds(connection : Connection, val neuron : Neuron, val connectionId: ConnectionId) {

    var directMessageSent : Int = 0

    /**
      * Send a message to the input neuron
      *
      * @param timestamp is the timestamp at which the message will be processed
      * @param message is the content which will be sent
      */
    def sendToInput(timestamp : Timestamp, message : Message) : Unit = {
      if(timestamp >= getCurrentTimestamp) {
        connection.preSyncRef.send(NeuronMessage(timestamp, message, connection.preSyncRef, connection.postSyncRef, None))
      }
      else
        throw new RuntimeException("can't send message with past timestamp")
    }

    /**
      * Send a message to the input neuron. In case of no delay, the output neuron will immediately process the message
      *
      * @param timestamp is the timestamp at which the message will be processed
      * @param message is the content which will be sent
      */
    def sendToOutput(timestamp : Timestamp, message : Message) : Unit = {
      if(timestamp == getCurrentTimestamp) {
        val ends = new NeuronEnds(neuron, timestamp)
        neuron.processSomaMessage(timestamp, message, Some(connectionId -> neuron.getInputConnection(connectionId).connection), ends)
      }
      else if(timestamp >= getCurrentTimestamp) {
        neuron.thisToSyncRef.send(NeuronMessage(timestamp, message, neuron.thisToSyncRef, neuron.syncToThisRef, Some(connectionId)))
      }
      else
        throw new RuntimeException("can't send message with past timestamp")
    }

    def getCurrentTimestamp : Timestamp = neuron.getCurrentTimestamp
  }
}

abstract class SynapseBuilder {
  def createSynapse: NeuronConnection
}

/**
  * Basic trait for all neuron connection
  *
  */
trait NeuronConnection extends Serializable{
  import NeuronConnection._

  var ends : Option[ConnectionEnds] = None
  private var fixedParameter = false

  def initialize() : Unit = {
    addProperty(ConnectionList)
    addProperty(ConnectionClassName)
  }

  def reset() : Unit = {}

  /**
    * Method used to describe the behavior of the connection model
    *
    * @param timestamp is the current time
    * @param message is the content
    */
  def processConnectionMessage(timestamp : Timestamp, message : Message) : Unit

  def getProperty[T](property : ConnectionProperty[T]) : Option[T] = property match {
    case ConnectionClassName =>
      Some(getClass.getCanonicalName.asInstanceOf[T])
    case ConnectionList =>
      Some('\u0000'.asInstanceOf[T])
    case _ =>
      None
  }
  def setProperty[T](property : ConnectionProperty[T], value : T) : Unit = {

  }

  def getEnds : ConnectionEnds = {
    if(ends.isEmpty)
      throw new RuntimeException("Synapse not connected")

    ends.get
  }

  def triggerEvent[Response <: EventResponse](event : Event[Response], response : Response) : Unit = {
    getEnds.neuron.triggerEventWith(event, response)
  }

  def addProperty[T](connectionProperty: ConnectionProperty[T]) : Unit = {
    getEnds.neuron.addConnectionProperty(connectionProperty)

  }

  def addEvent(event : Event[_ <: EventResponse]) : Unit = {
    getEnds.neuron.addEvent(event)
  }

  def hasFixedParameter : Boolean = this.fixedParameter
  def setFixedParameter(state : Boolean) : Unit = this.fixedParameter = state
}

