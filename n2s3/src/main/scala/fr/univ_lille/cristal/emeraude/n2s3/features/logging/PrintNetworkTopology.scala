package fr.univ_lille.cristal.emeraude.n2s3.features.logging

import java.io.PrintStream

import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.{MembraneLeakTime, MembraneThresholdFloat, SynapseWeightAndDelay}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.support.Time

import scala.collection.mutable

/**
  * Created by falezp on 12/12/16.
  */
object PrintNetworkTopology {

  def to(outStream : PrintStream, n2S3: N2S3) : Unit = {

    var neuron_count = 0
    var synapse_count = 0

    val width = 80

    n2S3.layers.foreach{ layer =>

      val data = layer.neuronPaths.map { neuron =>
        val properties = ExternalSender.askTo(neuron, GetAllProperties) match {
          case value : PropertiesList => value.content
        }

        val connections = ExternalSender.askTo(neuron, GetAllConnectionProperty(SynapseWeightAndDelay)) match {
          case PropertyValue(values: Seq[(NetworkEntityPath, (Float, Time))]@unchecked) => values
          case _ => Seq[(NetworkEntityPath, (Float, Time))]()
        }

        (neuron, properties, connections)
      }

      val neuron_size = data.size
      val synapse_size = data.map(_._3.size).sum


      neuron_count += neuron_size
      synapse_count += synapse_size

      val label_str = layer.identifier+" ("+neuron_size+"N, "+synapse_size+"S)"

      outStream.println("/"+(0 until (width-2)).map(_ => "=").mkString+"\\")
      outStream.println("|"+
        (0 until (width-2-label_str.length)/2).map(_ => " ").mkString+
        label_str+
        (0 until width-2-label_str.length-(width-2-label_str.length)/2).map(_ => " ").mkString+
        "|")
      outStream.println("\\"+(0 until (width-2)).map(_ => "=").mkString+"/")

      data.foreach{ case(neuron, properties, connections) =>

        val properties_str = mutable.ArrayBuffer[(String, String)]()

        if(properties.isDefinedAt(MembraneThresholdFloat.asInstanceOf[Property[Any]]))
          properties_str += "th" -> properties(MembraneThresholdFloat.asInstanceOf[Property[Any]]).toString

        if(properties.isDefinedAt(MembraneLeakTime.asInstanceOf[Property[Any]]))
          properties_str += "leak" -> properties(MembraneLeakTime.asInstanceOf[Property[Any]]).toString

        outStream.println("# "+neuron+" : "+properties_str.map{case(k,v) => k+"="+v}.mkString(", "))

        connections.foreach{ case(input_neuron, (w, d)) =>
          outStream.println("\tfrom "+input_neuron+" [weight="+w+", delay="+d.asMilliSecond+"ms]")
        }
      }
    }

    outStream.println((0 until width).map(_ => "=").mkString)
    outStream.println("Total : "+neuron_count+"N, "+synapse_count+"S")
    outStream.println((0 until width).map(_ => "=").mkString)

    outStream.flush()
  }

}

object NetworkTopologyCount {

  def from(n2S3: N2S3) : (Int, Int) = {

    var neuron_count = 0
    var synapse_count = 0

    n2S3.layers.foreach{ layer =>

      val data = layer.neuronPaths.map { neuron =>
        val properties = ExternalSender.askTo(neuron, GetAllProperties) match {
          case value : PropertiesList => value.content
        }

        val connections = ExternalSender.askTo(neuron, GetAllConnectionProperty(SynapseWeightAndDelay)) match {
          case PropertyValue(values: Seq[(NetworkEntityPath, (Float, Time))]@unchecked) => values
          case _ => Seq[(NetworkEntityPath, (Float, Time))]()
        }

        (neuron, properties, connections)
      }

      val neuron_size = data.size
      val synapse_size = data.map(_._3.size).sum


      neuron_count += neuron_size
      synapse_count += synapse_size
    }

    (neuron_count, synapse_count)

  }

}
