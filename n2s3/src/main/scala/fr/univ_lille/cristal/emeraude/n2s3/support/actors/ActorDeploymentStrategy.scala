package fr.univ_lille.cristal.emeraude.n2s3.support.actors

import akka.actor.Deploy

abstract class ActorDeploymentStrategy {
  def getDeployForActor() : Deploy
}

object LocalActorDeploymentStrategy extends ActorDeploymentStrategy {
  override def getDeployForActor(): Deploy = Deploy.local
}