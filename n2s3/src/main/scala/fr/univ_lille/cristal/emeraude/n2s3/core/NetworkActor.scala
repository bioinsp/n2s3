package fr.univ_lille.cristal.emeraude.n2s3.core

import java.awt.event.{ActionEvent, ActionListener}
import javax.swing.Timer

import akka.actor.SupervisorStrategy.Stop
import akka.actor.{Actor, ActorRef}
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Done, Initialize}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

import scala.concurrent.Future

/**
  * Created by falezp on 13/03/17.
  */
abstract class NetworkActor extends Actor {

  def initialize() : Unit
  def destroy() : Unit
  def process(message : Message, sender : ActorRef) : Unit

  def internInitialize() : Unit = {}
  def internDestroy() : Unit = {}

  def askTo(path : NetworkEntityPath, message: Message) : Any = {
    ExternalSender.askTo(path, message)
  }

  def askFuture(path : NetworkEntityPath, message: Message) : Future[Any] = {
    ExternalSender.askFuture(path, message)
  }

  def sendTo(path : NetworkEntityPath, message: Message) : Unit = {
    ExternalSender.sendTo(path, message)
  }

  def getReferenceOf(path : NetworkEntityPath) : NetworkEntityReference = {
      new ExternalNetworkEntityReference(path)
  }

  def getReferenceOf(path : NetworkEntityPath, sender : NetworkEntityPath) : NetworkEntityReference = {
    new RemoteNetworkEntityReference(path, sender)
  }

  def receive : Receive = {
    case Initialize =>
      initialize()
      internInitialize()
      sender ! Done
    case Stop =>
      internDestroy()
      destroy()
      sender ! Done
      context.stop(self)
    case m : Message =>
      process(m, sender)
    case m => println("[Logger] Unknown message "+m)
  }

}

/**
  *
  * @param refreshRate : refresh delay in milliseconds
  * @param n : number of element to update
  */

abstract class AutoRefreshNetworkActor(refreshRate : Int, n : Int = 1) extends NetworkActor {
  private var i = 0

  private val taskPerformer = new ActionListener() {
    override def actionPerformed(actionEvent: ActionEvent): Unit = {
      update(i)
      i = (i+1)%n
    }
  }

  private val timer = new Timer(refreshRate, taskPerformer)

  def update(n : Int) : Unit

  override def internInitialize() : Unit = {
    super.internInitialize()
    timer.start()
  }

  override def internDestroy() : Unit = {
    super.internDestroy()
    timer.stop()
  }
}
