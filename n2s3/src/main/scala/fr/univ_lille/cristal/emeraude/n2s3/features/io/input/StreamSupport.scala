package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import java.io.EOFException

import fr.univ_lille.cristal.emeraude.n2s3.core.actors.InputLayer
import fr.univ_lille.cristal.emeraude.n2s3.support.FormatTimestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

import scala.collection.mutable.ArrayBuffer

/**
  * Created by falezp on 30/06/16.
  */

trait StreamTimestampsManager {

  private var currentPrefix : Timestamp = 0
  protected var maxTimestamp : Timestamp = 0

  def prefix : Timestamp  = currentPrefix

  def setPrefix(timestamp : Timestamp) : Unit = {
    this.currentPrefix = timestamp
  }

  def maxPrefix(add : Timestamp = 0) : Unit = {
    this.currentPrefix = maxTimestamp+add
    //println("Set prefix at "+FormatTimestamp.format(this.currentPrefix))
  }

  def resetTimestamps() : Unit = {
    currentPrefix = 0L
    maxTimestamp = 0L
  }

  def retainMaxPrefix(timestamp : Timestamp) : Unit = {
    maxTimestamp = math.max(timestamp, maxTimestamp)
  }

}

object GlobalStream extends StreamTimestampsManager {
  def process[T <: N2S3InputPacket](data : T) : T = {
    data.modifyTimestamp(prefix)
    retainMaxPrefix(data.getStartTimestamp+data.getDuration)
    data
  }

  def reset() : Unit = {
    maxPrefix()
  }
}

class StreamSupport[I <: InputPacket,  O <: InputPacket](val shape : Shape) {

  var input : Option[StreamEntry[I]] = None
  var output : Option[StreamSupport[I, InputPacket]] = None
  var outputConverter : Option[StreamConverter[O, InputPacket]] = None
  var container : Option[InputLayer] = None

  def pipe[T <: InputPacket](converter : StreamConverter[O, T]): StreamSupport[I, T] = {
    converter.setShape(converter.shapeConverter(shape))
    val outputStream = new StreamSupport[I, T](converter.shapeConverter(shape))
    outputStream.input = Some(getEntry)
    output = Some(outputStream.asInstanceOf[StreamSupport[I, InputPacket]])
    outputConverter = Some(converter.asInstanceOf[StreamConverter[O, InputPacket]])

    if(container.isDefined)
      outputConverter.get.setContainer(container.get)

    outputStream
  }

  def getEntry : StreamEntry[I] = {
    if(input.isEmpty)
      throw new RuntimeException("no entry provided")
    else
      input.get
  }

  def >> [T <: InputPacket](converter : StreamConverter[O, T]) : StreamSupport[I, T] = pipe[T](converter)

  def process(data : I) : O = {
    input.get.innerProcess(data).asInstanceOf[O]
  }

  protected def innerProcess(data : O) : Any = {
    if(output.isDefined) {
      output.get.innerProcess(outputConverter.get.dataConverter(data))
    }
    else {
      GlobalStream.process[N2S3InputPacket](data.asInstanceOf[N2S3InputPacket])
    }

  }

  def append(stream : InputGenerator[I]) : Unit = {
    input.get.append(stream)
  }

  def clean(): Unit = {}

  def atEnd() : Boolean = input.get.atEnd()

  def next() : O = process(input.get.next())

  def reset() : Unit = {
    if(output.isDefined)
      output.get.reset()

    if(outputConverter.isDefined)
      outputConverter.get.reset()
  }

  def innerSetContainer(container : InputLayer): Unit = {

    this.container = Some(container)

    if(outputConverter.isDefined)
      outputConverter.get.setContainer(container)

    if(output.isDefined)
      output.get.innerSetContainer(container)
  }

  def setContainer(container : InputLayer): Unit = {
    if(input.isDefined)
      input.get.innerSetContainer(container)
  }

  override def toString: String = input.get.toString
}

class StreamEntry[I <: InputPacket](shape : Shape) extends StreamSupport[I, I](shape) {

  private val entryStreams = new ArrayBuffer[InputGenerator[I]]
  private var entryCursor = 0

  override def getEntry : StreamEntry[I] = {
    this
  }

  override def append(stream : InputGenerator[I]) : Unit = {
    assert(stream.shape == this.shape)
    entryStreams += stream
  }

  override def clean() : Unit = {
    GlobalStream.reset()
    reset()
    entryStreams.clear()
    entryCursor = 0
  }

  def restart() : Unit = {
    GlobalStream.reset()
    reset()
    entryStreams.foreach(_.reset())
    entryCursor = 0
  }

  def checkCurrentStream() : Unit = {
    while(entryCursor < entryStreams.size && entryStreams(entryCursor).atEnd()) {
      GlobalStream.reset()
      reset()
      entryCursor += 1
    }
  }

  override def atEnd() : Boolean = {
    checkCurrentStream()
    entryCursor >= entryStreams.size
  }

  override def next(): I = {
    if(atEnd())
      throw new EOFException

    if(output == null) {
      GlobalStream.process[N2S3InputPacket](entryStreams(entryCursor).next().asInstanceOf[N2S3InputPacket]).asInstanceOf[I]
    }
    else
      entryStreams(entryCursor).next()
  }

  override def toString: String = {
    if(atEnd()) {
      "No inputs data"
    }
    else {
      entryStreams(entryCursor).toString
    }
  }
}

abstract class InputFormat[T <: InputPacket](val shape: Shape) {
  def Entry : StreamEntry[T] = new StreamEntry[T](shape)
}

object StreamCast {
  def apply[I <: InputPacket, O <: InputPacket] = new StreamCast[I, O]()
}

class StreamCast[I <: InputPacket, O <: InputPacket]() extends StreamConverter[I, O] {
  override def resetConverter(): Unit = {}
  override def dataConverter(in: I): O = in.asInstanceOf[O]
}
