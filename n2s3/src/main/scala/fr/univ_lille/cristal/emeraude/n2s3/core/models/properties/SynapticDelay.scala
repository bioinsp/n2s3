package fr.univ_lille.cristal.emeraude.n2s3.core.models.properties

import fr.univ_lille.cristal.emeraude.n2s3.core.ConnectionProperty
import fr.univ_lille.cristal.emeraude.n2s3.support.Time

/**
  * Created by falezp on 21/03/17.
  */
object SynapticDelay extends ConnectionProperty[Time]