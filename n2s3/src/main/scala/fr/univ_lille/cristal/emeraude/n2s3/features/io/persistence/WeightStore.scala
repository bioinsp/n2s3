package fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence

import java.io.{DataOutputStream, FileOutputStream}

import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapticWeightFloat
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.NeuronGroupRef
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.Shape

/**
  * Created by pfalez on 02/05/17.
  */
object WeightStore {

  def saveShape(shape : Shape, out : DataOutputStream) : Unit = {
    out.writeInt(shape.dimensionNumber)
    shape.dimensions.foreach(d => out.writeInt(d))
  }

  def toFile(from : NeuronGroupRef, to : NeuronGroupRef, filename : String) : Unit = {
    val outputFile = new FileOutputStream(filename)
    val writer = new DataOutputStream(outputFile)

    val inputIndex = from.neuronPaths.zipWithIndex.map{ case(path, index) =>
      path -> index
    }.toMap

    saveShape(from.shape, writer)
    saveShape(to.shape, writer)

    to.neuronPaths.foreach{ output =>
      val connections = ExternalSender.askTo(output, GetAllConnectionProperty(SynapticWeightFloat))
        .asInstanceOf[ConnectionPropertyValues[Float]].values

      writer.writeInt(connections.size)

      connections.foreach{ case (_, input, value) =>
        if(inputIndex.isDefinedAt(input)) {
          writer.writeFloat(value)
          writer.writeInt(inputIndex(input))
        }
      }
    }

    writer.close()

  }

}
