/**************************************************************************************************
 * Contributors:
 * 		- created by emeraude
 *    - (add your name here if you contribute to this code). 
 **************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import java.awt.{Color, Dimension, FlowLayout, Graphics}

import javax.swing.{JFrame, JLabel, JPanel, WindowConstants}
import fr.univ_lille.cristal.emeraude.n2s3.core.NetworkEntityPath
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._

/*************************************************************************************************
 * Class to display input and output in the same time (neurons which are firing)
 ************************************************************************************************/
class InputOutputGraph(rawLayers : Seq[(String, Int, Seq[NetworkEntityPath], Int)]) {
  val frame = new JFrame("Spike viewer")

  val pan = new JPanel

  pan setLayout new FlowLayout

  frame add pan
/*
  val layers = Map(rawLayers.flatMap { case (name, width, actors, caseSize) =>
    val panel = new LayerPanel(width, actors, caseSize)
    pan add new JLabel(name)
    pan add panel
    actors.map(key => (key, panel))
  }:_*)*/

  val layers = Map(rawLayers.map{ case (name, width, actors, caseSize) =>
    val panel = new LayerPanel(width, actors, caseSize)
    pan add new JLabel(name)
    pan add panel
    name -> panel
  }:_*)


  val neuronMapping = Map(rawLayers.flatMap{ case (name, width, actors, caseSize) =>
    actors.map(key => (key, layers(name)))
  }:_*)

  frame setLocation (100, 100)

  frame setVisible true

  frame pack

  frame setDefaultCloseOperation WindowConstants.EXIT_ON_CLOSE

  //private val queue = scala.collection.mutable.PriorityQueue[(Timestamp, NetworkEntityPath)]()(Ordering.by(_._1))
  private var currentTimestamp = 0L
  var lastPaintTime : Long = System.nanoTime()

  def event(timestamp : Timestamp, sender : NetworkEntityPath) : Unit = {
    if(timestamp < currentTimestamp)
      return

    currentTimestamp = timestamp

    neuronMapping.get(sender) match {
      case Some(panel) =>
        panel.event(currentTimestamp, sender)
      case None => throw new RuntimeException("[InputOutputGraph] Unable to find entry")
    }

    val t = System.nanoTime()
    if(t-lastPaintTime > 1000000000/24) {
      //println(layers.size)
      layers.foreach(_._2.updateAllPixel(currentTimestamp))
      //println("1")
      frame.repaint()
      //println("2")
      lastPaintTime = t
    }
  }

}

class LayerPanel(val layerWidth: Int, val sources : Seq[NetworkEntityPath], val neuronSize: Int = 25) extends JPanel {
  private val mapper = Map(sources.zipWithIndex:_*)

  private val pixels = Array.ofDim[(Float, Timestamp)](mapper.size)
  for (i <- pixels.indices)
    pixels(i) = 0f -> 0L


  val leakTime = 100 MilliSecond

  setPreferredSize(new Dimension(neuronSize * layerWidth + 1, neuronSize * math.ceil(mapper.size.toFloat/layerWidth.toFloat).toInt + 1))
  override def paintComponent(g: Graphics): Unit = {
    g.setColor(Color.black)
    g.fillRect(0, 0, layerWidth * neuronSize, math.ceil(mapper.size.toFloat/layerWidth.toFloat).toInt * neuronSize)

    pixels.zipWithIndex.foreach{ case ((v, t), index) =>
      val c = math.min(1f, v)//1.0f-math.exp(-pv).toFloat
      g.setColor(new Color(c, c, c))
      g.fillRect((index/layerWidth) * neuronSize, (index%layerWidth) * neuronSize, neuronSize, neuronSize)
    }
  }
/*
  def setTimestamp(timestamp : Timestamp) : Unit = {
    if(timestamp != currentTimestamp) {
      val leak = math.exp(-(timestamp - currentTimestamp).toFloat / leakTime.timestamp.toFloat).toFloat
      for (i <- pontential.indices)
        pontential(i) *= leak
      currentTimestamp = timestamp
    }
  }*/

  private def updatePixel(deltaT : Timestamp, old : Float, weight : Float) = old*math.exp(-deltaT.toFloat/leakTime.timestamp.toFloat).toFloat+weight

  def event(timestamp : Timestamp, sender : NetworkEntityPath) : Unit = {
    val index = mapper(sender)
    val oldValue = pixels(index)
    if(oldValue._2 != timestamp)
      pixels(index) = updatePixel(timestamp-oldValue._2, oldValue._1, 0.5f) -> timestamp
  }

  def updateAllPixel(timestamp : Timestamp) : Unit = {
    for (i <- pixels.indices) {
      val oldValue = pixels(i)
      pixels(i) = updatePixel(timestamp-oldValue._2, oldValue._1, 0f) -> timestamp
    }
  }

}
