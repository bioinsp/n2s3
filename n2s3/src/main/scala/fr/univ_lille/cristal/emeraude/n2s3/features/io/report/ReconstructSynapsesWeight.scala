/************************************************************************************************************
 * Contributors: 
 * 		- created by Pierre Falez
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.io.report

import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapticWeightFloat

import scala.collection.mutable

object ReconstructSynapsesWeight {

  implicit val timeout = Config.defaultTimeout

  def save(input : Seq[NetworkEntityPath], inputWidth : Int, inputHeight : Int, output : Seq[NetworkEntityPath], pixelSize : Int, borderSize : Int, filename : String) : Unit = {

    val history = mutable.HashMap[(NetworkEntityPath, NetworkEntityPath), Float]()
    def synapseWeight(from : NetworkEntityPath, to : NetworkEntityPath) : Float = {
      val v = history.get(from -> to)
      if(v.isDefined)
        v.get
      else {
        ExternalSender.askTo(to, GetAllConnectionProperty(SynapticWeightFloat)) match {
          case PropertyValue(values: Seq[(NetworkEntityPath, Float)] @unchecked) =>
            values.foreach { case (path, value) =>
              history += (path -> to) -> value
            }
        }
        synapseWeight(from, to)
      }
    }

    def propagate(output  : NetworkEntityPath, input : Seq[NetworkEntityPath]) : Seq[Float] = {

      val neurons = mutable.HashMap[NetworkEntityPath, Float]()
      val queue = mutable.Queue[NetworkEntityPath]()
      neurons += output -> 1f
      queue += output

      while(queue.nonEmpty) {
        val currentNeuron = queue.dequeue()
        ExternalSender.askTo(currentNeuron, GetAllConnectionProperty(SynapticWeightFloat)) match {
          case PropertyValue(values: Seq[(NetworkEntityPath, Float)] @unchecked) =>
            values.foreach{ case (path, value) =>
              queue += path
              val old = neurons.getOrElseUpdate(path, 0)
              neurons.update(path, old+value*neurons(currentNeuron))
            }
        }
      }

      input.map(n => neurons(n))
    }

    val weights = output.map { neuron =>
      val values = propagate(neuron, input)
      val max = values.max
      values.map(_/max)
    }

    val minValue = weights.flatten.min
    val maxValue = weights.flatten.max

    val nWidth = math.ceil(math.sqrt(weights.size)).toInt
    val nHeight = math.ceil(weights.size.toFloat/nWidth).toInt

    val imageWidth = inputWidth
    val imageHeight = inputHeight

    val width = nWidth*(imageWidth*pixelSize+borderSize)-borderSize
    val height = nHeight*(imageHeight*pixelSize+borderSize)-borderSize

    val image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)

    weights.zipWithIndex.foreach { case (list, neuronIndex) =>
      val imageX = (neuronIndex%nWidth)*(imageWidth*pixelSize+borderSize)
      val imageY = (neuronIndex/nWidth)*(imageHeight*pixelSize+borderSize)
      list.zipWithIndex.foreach { case (value, pixelIndex) =>
        for(x <- imageX+(pixelIndex%imageWidth)*pixelSize until imageX+(pixelIndex%imageWidth)*pixelSize+pixelSize;
            y <- imageY+(pixelIndex/imageWidth)*pixelSize until imageY+(pixelIndex/imageWidth)*pixelSize+pixelSize) {



          if(value >= 0) {
            val v = value/math.max(maxValue, 1f)

            val rc = math.max(0f, -2f*v*v+4f*v-1f)
            val bc = math.max(0f, -2f*v*v+1)
            val gc = math.max(0f, 4f*v*(-v+1f))


            image.setRGB(x, y, math.round(bc*255) + (math.round(gc*255) << 8) + (math.round(rc*255) << 16))
          }
          else {
            val v = (value/math.min(minValue, -1f) * 255).toInt
            image.setRGB(x, y, v << 16)
          }
        }
      }
    }

    val outputFile = new File(filename)
    ImageIO.write(image, "png", outputFile)

  }
}
