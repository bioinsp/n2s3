package fr.univ_lille.cristal.emeraude.n2s3.core.exceptions

class InputException extends Exception

class UnknownMessageInputException(msg: String) extends InputException