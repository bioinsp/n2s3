package fr.univ_lille.cristal.emeraude.n2s3.support.io

import java.io.IOException

import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{InputPacket, InputTemporalPacket, StreamTimestampsManager}

import scala.util.Random

/**
  * Created by guille on 5/20/16.
  */

object InputStreamCombinators{
  implicit def streamCombinator[T <: InputPacket](stream: InputStream[T]): InputStreamCombinator[T] = {
    new InputStreamCombinator(stream)
  }
}

class InputStreamCombinator[T <: InputPacket](stream: InputStream[T]) extends {

  /** ******************************************************************************************************************
    * Combinators
    * *****************************************************************************************************************/
  def take(readLimit: Int): TakeInputStream[T] = new TakeInputStream(readLimit, stream)

  def repeat(repetitions: Int): RepeatInputStream[T] = new RepeatInputStream(stream, repetitions)

  def shuffle() : ShuffleInputStream[T] = new ShuffleInputStream(stream)

}

class PositionInputStream[T <: InputPacket](val stream: InputStream[T]) extends InputStreamDecorator[T, T](stream) {
  var position = 1

  override def next() = {
    this.position += 1
    stream.next()
  }
  override def atEnd(): Boolean = stream.atEnd()
  override def reset(): Unit = this.position = 1; stream.reset()
  override def toString = " image=" + this.position + super.toString()
}

class TakeInputStream[T <: InputPacket](val numberOfEntries: Int, val stream: InputStream[T]) extends InputStreamDecorator[T, T](stream){
  var taken = 0
  def next(): T = {
    if (this.atEnd()){
      throw new IOException("Reading after limit of " + numberOfEntries)
    }
    this.taken += 1
    stream.next()
  }

  override  def reset() = {
    this.taken = 0
    stream.reset()
  }

  def atEnd() = stream.atEnd() || (this.taken >= numberOfEntries && numberOfEntries >= 0 )
}

class RepeatInputStream[T <: InputPacket](val stream: InputStream[T], val repetitions: Int = 1) extends InputStreamDecorator[T, T](stream) with StreamTimestampsManager {
  var currentRepetition = 1

  override def next(): T = {
    if (stream.atEnd()){
      maxPrefix()
      stream.reset()
      this.currentRepetition += 1
    }
    val d = stream.next()

    // temporal data case
    d match {
      case dt : InputTemporalPacket =>
        dt.modifyTimestamp(prefix)
        retainMaxPrefix(dt.getStartTimestamp+dt.getDuration)
      case _ =>
    }

    d
  }
  override def atEnd(): Boolean = stream.atEnd() && this.currentRepetition == repetitions

  override def reset(): Unit = {
    stream.reset()
    resetTimestamps()
    this.currentRepetition = 1

  }

  override def toString = super.toString()+" | Repetition " + this.currentRepetition + " / " + repetitions
}

// TODO allow only on non-temporal data
class ShuffleInputStream[T <: InputPacket](val stream: InputStream[T]) extends InputStreamDecorator[T, T](stream){

  val bufferSize = 100
  var buffer = new scala.collection.mutable.Queue[T]()

  override def next(): T = {
    if(buffer.isEmpty) {
      while(!stream.atEnd() && buffer.size < bufferSize)
        buffer += stream.next()
      buffer = Random.shuffle(buffer)
    }

    buffer.dequeue()
  }
  override def atEnd(): Boolean = stream.atEnd() && buffer.isEmpty

  override def reset(): Unit = stream.reset()
  override def toString = stream.toString
}