package fr.univ_lille.cristal.emeraude.n2s3.features.io.report

import java.io.PrintWriter

/**
  * Created by falezp on 15/03/16.
  */
abstract class Report {

  val experimentParameter = scala.collection.mutable.Map[String, String]()
  var writer : PrintWriter = _

  def addExperimentParameter(key : String, value : String) : Unit = {
    experimentParameter += key -> value
  }

  def save(filename : String) : Unit
}
