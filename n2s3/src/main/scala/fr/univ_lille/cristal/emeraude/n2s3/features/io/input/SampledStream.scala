package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import fr.univ_lille.cristal.emeraude.n2s3.core.actors.ElectricSpike
import fr.univ_lille.cristal.emeraude.n2s3.core.event.{TimedEvent, TimedEventResponse}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.{InputDistribution, PoissonDistribution, Time}

import scala.util.Random

/**
  * Created by falezp on 27/06/16.
  */


trait SampleToTemporalConverter[T] {
  protected def getValue(v : T) : Float = v match {
    case f : Float =>
      assert(f >= 0f && f <= 1f, "v="+f)
      f
    case d : Double =>
      assert(d >= 0.0 && d <= 1.0)
      d.toFloat
    case b : Byte =>
      assert(b >= 0 && b <= 255)
      b.toFloat/255f
    case s : Short =>
      assert(s >= 0 && s <= 255)
      s.toFloat/255f
    case i : Int =>
      assert(i >= 0 && i <= 255)
      i.toFloat/255f
    case l : Long =>
      assert(l >= 0 && l <= 255)
      l.toFloat/255f
    case b : Boolean => if(b) 1f else 0f
    case _ => 0f
  }

  protected def convertMetaData(inputMetaData: InputSampleMetaData, start : Timestamp, end : Timestamp) : Option[InputTemporalMetaData] = inputMetaData match {
    case InputLabel(label) => Some(N2S3InputLabel(label, start, end))
    case _ => None
  }
}

object SampleToSpikeTrainEvent extends TimedEvent[SampleToSpikeTrainResponse]
case class SampleToSpikeTrainResponse(packet : InputTemporalPacket) extends TimedEventResponse {
  override val timestamp = packet.startTimestamp
}

object SampleToSpikeTrainConverter {
  def apply[UT, T <: InputSample[UT]](minimumFrequency: Float = 0f,
            maximumFrequency: Float = 10f,
            pauseDuration: Time = 1 Second,
            expositionDuration: Time = 1 Second) =
    new SampleToSpikeTrainConverter[UT, T](minimumFrequency, maximumFrequency, pauseDuration, expositionDuration)
}

class SampleToSpikeTrainConverter[UT, T <: InputSample[UT]](val minimumFrequency: Float = 0f,
                                  val maximumFrequency: Float = 10f,
                                  val pauseDuration: Time = 1 Second,
                                  val expositionDuration: Time = 1 Second)

  extends StreamConverter[T, InputTemporalPacket] with SampleToTemporalConverter[UT] with StreamTimestampsManager {

  def dataConverter(in: T): InputTemporalPacket = {
    val distribution = new PoissonDistribution(0, expositionDuration.timestamp, minimumFrequency, maximumFrequency)

    val start = prefix
    val end = prefix+expositionDuration.timestamp+pauseDuration.timestamp

    val data = in.getAllData.map { case(index, v) =>
      index -> distribution.applyTo(getValue(v)).map { t =>
        N2S3InputSpike(ElectricSpike(), prefix + t)
      }
    }
    if(!data.exists(_._2.nonEmpty)) {

      println("Warning : no spike...")
      //  TMP
      val d = new InputTemporalPacket(
        in.getShape,
        data.map{case(i, _) => i -> Seq(N2S3InputSpike(ElectricSpike(), prefix))},
        start,
        end - start
      ).setMetaData(in.getMetaData.flatMap { m => convertMetaData(m, start, end) }: _*)

      setPrefix(prefix + expositionDuration.timestamp + pauseDuration.timestamp)

      d
    }
    else {

      val d = new InputTemporalPacket(
        in.getShape,
        data,
        start,
        end - start
      ).setMetaData(in.getMetaData.flatMap { m => convertMetaData(m, start, end) }: _*)

      setPrefix(prefix + expositionDuration.timestamp + pauseDuration.timestamp)

      triggerEventWith(SampleToSpikeTrainEvent, SampleToSpikeTrainResponse(d))

      d
    }
  }

  def resetConverter() : Unit = {
    resetTimestamps()
  }
}

object SampleToSpikeTrainBinaryConverter {
  def apply[UT, T <: InputSample[UT]](binaryThreshold: Float = 0.5f,
                                      intervalDuration: Time = 1 Second,
                                      StandardDeviation : Time = 1 MilliSecond) =
    new SampleToSpikeTrainBinaryConverter[UT, T](binaryThreshold, intervalDuration, StandardDeviation)
}

/**
  * If value > th, generate 1 spike, otherwise, 0
  * @param binaryThreshold
  * @param intervalDuration
  * @param StandardDeviation
  * @tparam UT
  * @tparam T
  */

class SampleToSpikeTrainBinaryConverter[UT, T <: InputSample[UT]](binaryThreshold: Float = 0.5f,
                                                                  intervalDuration: Time = 1 Second,
                                                                  StandardDeviation : Time = 1 MilliSecond)

  extends StreamConverter[T, InputTemporalPacket] with SampleToTemporalConverter[UT] with StreamTimestampsManager {

  def dataConverter(in: T): InputTemporalPacket = {

    val start = prefix
    val end = prefix+intervalDuration.timestamp

    val data = in.getAllData.map { case(index, v) =>
      val t = math.max(0, math.min(intervalDuration.timestamp, intervalDuration.timestamp/2+(Random.nextGaussian()*StandardDeviation.timestamp.toDouble).toLong))
      index -> (if(getValue(v) >= binaryThreshold) Seq(N2S3InputSpike(ElectricSpike(), prefix + t)) else Seq())
    }
    if(!data.exists(_._2.nonEmpty)) {

      println("Warning : no spike...")
      //  TMP
      val d = new InputTemporalPacket(
        in.getShape,
        data.map{case(i, _) => i -> Seq(N2S3InputSpike(ElectricSpike(), prefix))},
        start,
        end - start
      ).setMetaData(in.getMetaData.flatMap { m => convertMetaData(m, start, end) }: _*)

      setPrefix(prefix + intervalDuration.timestamp)

      d
    }
    else {

      val d = new InputTemporalPacket(
        in.getShape,
        data,
        start,
        end - start
      ).setMetaData(in.getMetaData.flatMap { m => convertMetaData(m, start, end) }: _*)

      setPrefix(prefix + intervalDuration.timestamp)

      triggerEventWith(SampleToSpikeTrainEvent, SampleToSpikeTrainResponse(d))

      d
    }
  }

  def resetConverter() : Unit = {
    resetTimestamps()
  }
}

class SampleToSpikeTrainRankOrderConverter[UT, T <: InputSample[UT]](binaryThreshold: Float = 0.1f,
                                                                      intervalDuration: Time = 1 Second,
                                                                      pauseDuration: Time = 1 Second,
                                                                      StandardDeviation : Time = 1 MilliSecond)

  extends StreamConverter[T, InputTemporalPacket] with SampleToTemporalConverter[UT] with StreamTimestampsManager {

  def dataConverter(in: T): InputTemporalPacket = {

    val start = prefix
    val end = prefix+intervalDuration.timestamp+pauseDuration.timestamp



    val data = in.getAllData.map { case(index, v) =>
      val t = math.max(0, math.min(intervalDuration.timestamp, ((1.0-getValue(v))*intervalDuration.timestamp.toDouble*(1.0/(1.0-binaryThreshold))).toLong+(Random.nextGaussian()*StandardDeviation.timestamp.toDouble).toLong))
      index -> (if(getValue(v) >= binaryThreshold) Seq(N2S3InputSpike(ElectricSpike(), prefix + t)) else Seq())
    }
    if(!data.exists(_._2.nonEmpty)) {

      println("Warning : no spike...")
      //  TMP
      val d = new InputTemporalPacket(
        in.getShape,
        data.map{case(i, _) => i -> Seq(N2S3InputSpike(ElectricSpike(), prefix))},
        start,
        end - start
      ).setMetaData(in.getMetaData.flatMap { m => convertMetaData(m, start, end) }: _*)

      setPrefix(end)

      d
    }
    else {

      val d = new InputTemporalPacket(
        in.getShape,
        data,
        start,
        end - start
      ).setMetaData(in.getMetaData.flatMap { m => convertMetaData(m, start, end) }: _*)

      setPrefix(end)

      triggerEventWith(SampleToSpikeTrainEvent, SampleToSpikeTrainResponse(d))

      d
    }
  }

  def resetConverter() : Unit = {
    resetTimestamps()
  }
}

object IntervalSampleToSpikeTrainConverter {
  def apply[UT, T <: InputSample[UT]]() = new IntervalSampleToSpikeTrainConverter[UT, T]
}

class IntervalSampleToSpikeTrainConverter[UT, T <: InputSample[UT]] extends StreamConverter[T, InputTemporalPacket] with SampleToTemporalConverter[UT] with StreamTimestampsManager {

  var intervalDuration = 1 Second
  var intervalInterDuration = 1 Second
  var intervalPerInput = 1

  var pauseDuration = 1 Second

  var distributionFunction : (Timestamp, Timestamp) => InputDistribution = (s,e) =>  new PoissonDistribution(s,e,0f,1.0/intervalDuration.asSecond)


  def dataConverter(in: T): InputTemporalPacket = {

    val start = prefix
    val end = prefix+(intervalDuration.timestamp+intervalInterDuration.timestamp)*intervalPerInput + pauseDuration.timestamp


    val d = new InputTemporalPacket(
      in.getShape,
      in.getAllData.map { case(index, data) =>
        (index, (0 until intervalPerInput).flatMap { i =>
          val v = getValue(data)
          val s_v = math.min(1f, math.max(0f, (1f-v)+Random.nextFloat()/100f))
          val t = 1L+(s_v*intervalDuration.timestamp.toFloat).toLong
          if(t <= intervalDuration.timestamp && v > 0.01)
            Some(N2S3InputSpike(ElectricSpike(), prefix + i*(intervalDuration.timestamp+intervalInterDuration.timestamp) + t))
          else
            None
        })
      },
      start,
      end-start
    ).setMetaData(in.getMetaData.flatMap{m => convertMetaData(m, start, end)}:_*)

    setPrefix(prefix + (intervalDuration.timestamp+intervalInterDuration.timestamp)*intervalPerInput + pauseDuration.timestamp)
    d
  }

  def setIntervalDuration(duration : Time) : this.type = {
    intervalDuration = duration
    this
  }

  def setIntervalInterDuration(duration : Time) : this.type = {
    intervalInterDuration = duration
    this
  }


  def setPauseDuration(duration : Time) : this.type = {
    pauseDuration = duration
    this
  }

  def setIntervalPerInput(n : Int) : this.type = {
    intervalPerInput = n
    this
  }
  def resetConverter() : Unit = {
    resetTimestamps()
  }


}
