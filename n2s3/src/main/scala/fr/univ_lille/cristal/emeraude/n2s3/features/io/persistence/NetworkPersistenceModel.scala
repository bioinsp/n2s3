package fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence

import java.io._

import fr.univ_lille.cristal.emeraude.n2s3.core.{ConnectionProperty, Property}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.Shape

import scala.collection.mutable

/**
  * Created by falezp on 23/11/16.
  */

object NetworkPersistenceModel {
  case class Layer(
                    identifier : String,
                    shape : Shape,
                    isInput : Boolean
                  )

  case class Neuron(
                   layer : Int,
                   identifier : String,
                   properties : Seq[(Int, Any)],
                   className : Int
                   )

  case class Connection(
                       from : Int,
                       to : Int,
                       properties : Seq[(Int, Any)],
                       className : Int
                       )
}

class NetworkPersistenceModel {
  import fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence.NetworkPersistenceModel._


  val layers = mutable.ArrayBuffer[Layer]()
  val neurons = mutable.ArrayBuffer[Neuron]()
  val connections = mutable.ArrayBuffer[Connection]()
  val className = mutable.ArrayBuffer[String]()

  private def getClassNameId(name : String) : Int = {
    val id = className.indexOf(name)
    if(id == -1) {
      className += name
      className.size-1
    }
    else
      id
  }

  def addLayer(name : String, shape : Shape, isInput : Boolean) : Int = {
    assert(!layers.exists(_.identifier == name))
    layers += Layer(name, shape, isInput)
    layers.size-1
  }

  def addNeuron(layer : Int, name : String, properties : Seq[(Property[Any], Any)], className : String) : Int = {
    assert(layer >= 0 && layer < layers.size)
    neurons += Neuron(
      layer,
      name,
      properties.map(entry => (getClassNameId(entry._1.getClass.getCanonicalName), entry._2)),
      getClassNameId(className)
    )
    neurons.size-1
  }

  def addConnection(from : Int, to : Int, properties : Seq[(ConnectionProperty[Any], Any)], className : String) : Int = {
    assert(from >= 0 && from < neurons.size)
    assert(to >= 0 && to < neurons.size)
    connections += Connection(
      from,
      to,
      properties.map(entry => (getClassNameId(entry._1.getClass.getCanonicalName), entry._2)),
      getClassNameId(className)
    )
    neurons.size-1
  }

  def saveTo(filename : String) = {
    val writer = new ObjectOutputStream(new FileOutputStream(filename))

    // class name
    writer.writeInt(className.size)
    className.foreach { name =>
      writer.writeInt(name.length)
      writer.write(name.getBytes)
    }

    // layers
    writer.writeInt(layers.size)
    layers.foreach { layer =>
      writer.writeInt(layer.identifier.length)
      writer.write(layer.identifier.getBytes)
      writer.writeInt(layer.shape.dimensionNumber)
      layer.shape.dimensions.foreach { d =>
        writer.writeInt(d)
      }
      writer.writeBoolean(layer.isInput)
    }

    // neurons
    writer.writeInt(neurons.size)
    neurons.foreach { neuron =>
      writer.writeInt(neuron.identifier.length)
      writer.write(neuron.identifier.getBytes)
      writer.writeInt(neuron.layer)
      writer.writeInt(neuron.className)


      writer.writeInt(neuron.properties.size)
      neuron.properties.foreach { case(key, value) =>
        writer.writeInt(key)
        writer.writeObject(value)
      }

    }

    //connection
    writer.writeInt(connections.size)
    connections.foreach { connection =>
      writer.writeInt(connection.from)
      writer.writeInt(connection.to)
      writer.writeInt(connection.className)

      writer.writeInt(connection.properties.size)
      connection.properties.foreach { case(key, value) =>
        writer.writeInt(key)
        writer.writeObject(value)
      }

    }

    writer.close()
  }

  def loadFrom(filename : String) = {

    def readNextString(reader : ObjectInputStream) : String = {
      val stringSize = reader.readInt()
      val buffer = new Array[Byte](stringSize)
      assert(stringSize > 0)
      assert(reader.read(buffer, 0, stringSize) == stringSize)
      new String(buffer)
    }

    val reader = new ObjectInputStream(new FileInputStream(filename))

    val classNameNumber = reader.readInt()
    for(i <- 0 until classNameNumber) {
      className += readNextString(reader)
    }

    val layerNumber = reader.readInt()
    for(i <- 0 until layerNumber) {
      val name = readNextString(reader)
      val dimensionNumber = reader.readInt()
      val shape = Shape((for(i <- 0 until dimensionNumber) yield reader.readInt()):_*)
      val isInput = reader.readBoolean()

      layers += Layer(name, shape, isInput)
    }

    val neuronNumber = reader.readInt()
    for(i <- 0 until neuronNumber) {
      val name = readNextString(reader)
      val layer = reader.readInt()
      val className = reader.readInt()

      val propertyNumber = reader.readInt()
      val properties = for(i <- 0 until propertyNumber) yield {
        val key = reader.readInt()
        val value = reader.readObject()
        (key, value)
      }

      neurons += Neuron(layer, name, properties, className)
    }

    val connectionNumber = reader.readInt()
    for(i <- 0 until connectionNumber) {
      val from = reader.readInt()
      val to = reader.readInt()
      val className = reader.readInt()

      val propertyNumber = reader.readInt()
      val properties = for(i <- 0 until propertyNumber) yield {
        val key = reader.readInt()
        val value = reader.readObject()
        (key, value)
      }

      connections += Connection(from, to, properties, className)
    }

    reader.close()


  }


}
