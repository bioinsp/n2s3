/************************************************************************************************************
 * Contributors:
 * 		- created by Pierre Falez on 02/05/16
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import akka.actor.{Actor, Props}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.NetworkEntityActor.{CheckMailbox, ExplicitSenderRoutedMessage, ImplicitSenderRoutedMessage}
import fr.univ_lille.cristal.emeraude.n2s3.core.exceptions.{UnknownDestinationException, UnknownPathException}
import fr.univ_lille.cristal.emeraude.n2s3.support
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{Message, PropsBuilder}

import scala.collection.mutable

/**
  *  Companion object of [[NetworkEntityActor]].
  *  Includes common messages used by [[NetworkEntityActor]] and a builder to instantiate actors.
  */
object NetworkEntityActor extends support.actors.ActorCompanion {

  /**
    * Message used to add a child to the target NetworkContainer.
    *
    * @param identifier of the entity, which will be used to resolve the local path to the entity
    * @param entity which will be added to the NetworkContainer
    */
  case class AddChildEntity(identifier: Any, entity: NetworkEntity) extends Message

  /**
    * Message used to communicate between two NetworkEntity
    * This message allow the receiver to know and response to the NetworkEntity sender by generating another InternalRoutedMessage
    *
    * @param path is the local URI which designates the target of the message
    * @param message is the content to the target
    * @param sender is the remote URI of the sender
    */
  case class ExplicitSenderRoutedMessage(path: Traversable[Any], message: Message, sender : NetworkEntityPath) extends  Message

  /**
    * Message used to communicate between a target of type NetworkEntity and a sender which is a general Actor
    * The responses messages are returned without encapsulate class to the sender Actor
    *
    * @param path is the local URI which designates the target of the message
    * @param message is the content to the target
    */
  case class ImplicitSenderRoutedMessage(path : Traversable[Any], message: Message) extends  Message

  case class SynchronizedRoutedMessage(to: Traversable[Any], message: Message, sender : NetworkEntityPath)

  /**
    * Message used when the limits of local messages processed is reached.
    * This one alone to resume immediately the messageQueue processing in case of there's no incoming messages
    */
  private object CheckMailbox

  /**
    * Returns a PropsBuilder for a [[NetworkEntityActor]]
    */
  def newPropsBuilder() = new NetworkEntityActorPropsBuilder
}

/**
  * Specialized PropsBuilder used to construct a NetworkEntityActor.
  */
class NetworkEntityActorPropsBuilder extends PropsBuilder {
  private var entity: NetworkEntity = _

  def setEntity(entity: NetworkEntity) : this.type = {
    this.entity = entity
    this
  }

  override def build(): Props = Props(classOf[NetworkEntityActor], this.entity)
}

/**
  * Actor to interact with a network entity (e.g. neurons, neuron containers)
  * Receives messages that it may treat itself or forward to it's children entities
  * The latter handle two kind of messages :
  *   - [[ExplicitSenderRoutedMessage]] includes the actor where the response should be sent
  *   - [[ImplicitSenderRoutedMessage]] will send the message's response to de implicit sender set by Akka
  *
  * Other messages are processed by the actor itself.
  *
  * This class keeps a queue of messages that are not yet processed.
  *
  * Each message generated by a local NetworkEntity is added to this queue in order to let the current entity
  * finish processing before starting the following. However, in the case of local ask pattern, the message is instantly processed
  * and the response can be returned immediately.
  *
  * Local ask should be avoided if possible
  * In case of mutual local ask, the actor will be in state of deadlock
  *
  * For examples of usages, see [[NetworkContainer]]
  * @param entity is the root Networkentity contained by the actor
  */
class NetworkEntityActor(val entity : NetworkEntity) extends Actor  {

  entity.container = this

  /**
    * Queue which contains next message to process
    * Each message are accompanied by the target NetworkEntity and the sender NetworkEntityReference
    */
  private val messageQueue = mutable.Queue[(Message, NetworkEntity, NetworkEntityReference)]()

  /**
    * Parameter used to set the maximum limits of local messages processed before check for incoming messages in actor
    */
  private val maxMessageProcess = 10

  override def preRestart(reason: Throwable, message: Option[Any]): Unit = {
    println("[DEBUG] Restart Actor " + self + " : " + reason + (if(message.isDefined) " | " + message.get else "" ))
    super.preRestart(reason, message)
  }

  def resolvePath(target : Traversable[Any]) : NetworkEntity = {
    entity.resolvePath(target)
  }


  /**
    * Process local message by sending the content to the target NetworkEntity.
    * The maximum of message processed by this method during a single call is fixed by maxMessageProcess attribute.
    * If the limit is reached, a CheckMailbox is enqueued in current actor mailbox
    */
  protected def processQueue() : Unit = {
    var messageCount = 0
    while(messageQueue.nonEmpty && messageCount < maxMessageProcess) {
      val(message, target, sender) = messageQueue.dequeue()
      target.sendMessageFrom(message, sender)
      messageCount += 1
    }

    if(messageCount == maxMessageProcess)
      self ! CheckMailbox
  }

  /**
    * Add a message to process in local queue
    *
    * @param message is the content to the target
    * @param destination is the target NetworkEntity which will receive the message
    * @param sender is the NetworkEntityReference used to response
    */
  def addToQueue(message : Message, destination : NetworkEntity, sender : NetworkEntityReference) : Unit = {
    messageQueue += ((message, destination, sender))
  }

  def processMessage(message: Message, destinyPath: Traversable[Any], sender: NetworkEntityReference): Unit = {
    this.processMessage(message, resolvePath(destinyPath), sender)
  }

  def processMessage(message: Message, destinyEntity: NetworkEntity, sender: NetworkEntityReference): Unit = {
    try {
      addToQueue(message, destinyEntity, sender)
      processQueue()
    } catch {
      case e : UnknownPathException => throw new UnknownDestinationException(message, e.path)
    }
  }

  /**
    * Receive method of the actor
    *
    * Hhen receiving an InternalRoutedMessage or an ExternalRoutedMessage, the NetworkEntityReference of the local target
    * is resolved and the reference of the sender is generated, then the message is queued waiting for process
    *
    * Other message are added to the local queue with the root NetworkEntity as target.
    *
    * @throws UnknownDestinationException when the local path requested can't be found
    */
  def receive = {
    case ExplicitSenderRoutedMessage(path, message, sender) =>
      this.processMessage(message, path, entity.getReferenceOf(sender))
    case ImplicitSenderRoutedMessage(path, message) =>
      this.processMessage(message, path, new ExternalActorReference(sender))
    case message : Message =>
      this.processMessage(message, entity, new ExternalActorReference(sender))
    case CheckMailbox =>
      processQueue()
  }
}