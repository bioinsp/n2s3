package fr.univ_lille.cristal.emeraude.n2s3.core.event

/**
  * Created by guille on 10/14/16.
  */
object NeuronFireEvent extends TimedEvent[NeuronFireResponse]