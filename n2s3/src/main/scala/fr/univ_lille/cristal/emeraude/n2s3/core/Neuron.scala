/************************************************************************************************************
 * Contributors: 
 * 		- Pierre Falez
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.core

import fr.univ_lille.cristal.emeraude.n2s3.core.NetworkEntity.AskReference
import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection.ConnectionEnds
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.SynchronizedMessage
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Done, WrapMessage}
import fr.univ_lille.cristal.emeraude.n2s3.core.event.{EventHolder, EventHolderMessage, NeuronFireEvent}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.FixedParameter
import fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse.FloatSynapse
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

import scala.collection.{immutable, mutable}
import scala.language.postfixOps

/**
  * Neuron companion object
  */
object Neuron {

  /**
    * Identification type of a neuron connection
    */
  type ConnectionID = Int

  /**
    * Message send to create connection with an output neuron, designated by target.
    * If no connection is given in this message, neuron will use his default constructor to create the connection object.
    *
    * @param target is the output neuron path
    * @param connection is an optional parameter for specify the connection type
    */
  case class CreateNeuronConnectionWith(target : NetworkEntityPath, connection : NeuronConnection, connectionProperties: Seq[(ConnectionProperty[Any], Any)] = Seq(), preDelay : Timestamp = 0) extends Message
  case class NeuronConnectionRequests(seq : Seq[CreateNeuronConnectionWith]) extends Message


  case class NewConnection(path : ConnectionPath) extends Message
  case class NewConnections(paths : Seq[ConnectionPath]) extends Message

  /**
    * Message send to create a connection with an input neuron, designated by path.
    * If no connection is given in this message, neuron will use his default constructor to create the connection object.
    *
    * @param path is target of the connection
    * @param synchronizer is entity of output neuron which manage synchronization
    * @param connection is an optional parameter for specify the connection type
    */
  case class CreateNeuronInputConnectionWith(path: NetworkEntityPath, synchronizer : NetworkEntityPath, syncToInputRef : Option[NetworkEntityReference], connection: NeuronConnection, connectionProperties: Seq[(ConnectionProperty[Any], Any)], preDelay : Timestamp) extends Message
  case class NeuronInputConnectionRequests(seq : Seq[CreateNeuronInputConnectionWith]) extends Message
  /**
    * Response to CreateNeuronInputConnectionWith message
    *
    * @param id is the getIdentifier of the new connection
    * @param synchronizer is the entity of input neuron which manage synchronization
    */
  case class ConnectionCreationResponse(id : Int, synchronizer : NetworkEntityPath, preDelay : Timestamp) extends Message
  case class ConnectionCreationResponses(seq : Seq[ConnectionCreationResponse]) extends Message


  //case class RemoveNeuronConnectionWith(target : NetworkEntityPath) extends Message
  case class RemoveNeuronInputConnection(connectionId : ConnectionID) extends Message
  case class RemoveNeuronOutputConnection(path : NetworkEntityPath, connectionId : ConnectionID) extends Message


  object RemoveConnection extends Message

  /**
    * Message send to destroy the target neuron
    * All incoming and outgoing connection from target neuron will be deleted too
    */
  object Destroy extends Message

  /**
    * Message used to communicate between neurons.
    *
    * @param timestamp of the message
    * @param message is the content
    */
  class NeuronMessage(val timestamp: Timestamp, val message : Message, val preSync : NetworkEntityReference, val postSync : NetworkEntityReference, val entryConnection : Option[ConnectionID]) extends SynchronizedMessage {
    override def toString = "NeuronMessage("+timestamp+", "+message+", "+preSync+", "+postSync+", "+entryConnection+")"
  }
  object NeuronMessage {
    def apply(timestamp: Timestamp, message : Message, preSync : NetworkEntityReference, postSync : NetworkEntityReference, entryConnection : Option[Int]) = new NeuronMessage(timestamp, message, preSync, postSync, entryConnection)
  }

  case class ConnectionMessage(override val timestamp: Timestamp, override val message : Message, override val preSync : NetworkEntityReference, override val postSync : NetworkEntityReference, connectionId : ConnectionID) extends NeuronMessage(timestamp, message, preSync, postSync, None)
  /**
    * Represent an inhibition spike
    */
  case class Inhibition(ratio : Float) extends Message

  /**
    * Allow to set a synchronizer to a neuron
    */
  case class SetSynchronizer(synchronizer: NetworkEntityPath) extends Message

  abstract class Connection {
    val path : NetworkEntityPath
    val preSyncRef : NetworkEntityReference
    val postSyncRef : NetworkEntityReference
  }

  /**
    * Class used to store data of an incoming connection
    *
    * @param connection is the object which manage the connection
    * @param path of the input neuron
    * @param preSyncRef is the reference of the input synchronizer according to current neuron
    * @param postSyncRef is the reference of the input neuron according input synchronizer
    */
  case class InputConnection(connection : NeuronConnection, override val path : NetworkEntityPath, override val preSyncRef : NetworkEntityReference, override val postSyncRef : NetworkEntityReference) extends Connection

  /**
    * Class use to store data of an outgoing connection
    *
    * @param connectionId is the id of the connection
    * @param path of the input neuron
    * @param preSyncRef is the reference of the output synchronizer according to current neuron
    * @param postSyncRef is the reference of the output neuron according output synchronizer
    */
  case class OutputConnection(connectionId : ConnectionID, override val path : NetworkEntityPath, override val preSyncRef : NetworkEntityReference, override val postSyncRef : NetworkEntityReference, preDelay : Timestamp) extends Connection

  /**
    * Class used to manage communication of a neuron with input and output connected synapses
    *
    * @param neuron is the current neuron
    * @param currentTimestamp is the timestamp of the current process
    */
  class NeuronEnds(neuron : Neuron, currentTimestamp : Timestamp) {


    /**
      * Send a message at a timestamp to all incoming connections.
      * In the case of no delay, the message will be process immediately by all the input connection
      *
      * @param timestamp is the timestamp at which the message will be processed
      * @param message is the content which will be sent
      */
    def sendToAllInput(timestamp : Timestamp, message : Message) : Unit = {
      neuron.inputConnections.foreach { case (connectionID, _) =>
        sendToInput(timestamp, message, connectionID)
      }
    }

    def sendToInput(timestamp : Timestamp, message : Message, connectionID: ConnectionID) : Unit = {
      if(timestamp == currentTimestamp)
        neuron.inputConnections(connectionID).connection.processConnectionMessage(timestamp, message)
      else if(timestamp > currentTimestamp)
        neuron.thisToSyncRef.send(ConnectionMessage(timestamp, message, neuron.thisToSyncRef, neuron.syncToThisRef, connectionID))
      else
        throw new RuntimeException("can't send message with past timestamp")
    }


    /**
      * Send a message at a timestamp to all outgoing connections.
      *
      * @param timestamp is the timestamp at which the message will be processed
      * @param message is the content which will be sent
      */
    def sendToAllOutput(timestamp : Timestamp, message : Message): Unit = {
      if(timestamp >= currentTimestamp) {
        neuron.outputConnections.zipWithIndex.foreach { case (connection, id) =>
          connection.preSyncRef.send(ConnectionMessage(timestamp+connection.preDelay, message, connection.preSyncRef, connection.postSyncRef, connection.connectionId))
        }
      }
      else
        throw new RuntimeException("can't send message with past timestamp")
    }
  }
}

object ConnectionClassName extends ConnectionProperty[String]
object ConnectionList extends ConnectionProperty[Any]

trait NeuronModel {
  def createNeuron(): Neuron
}

/**
  * Basic trait for all neuron models
  * This class manage the interconnections between two neurons.
  * The connection object is always stored in the output neuron.
  * All connection are managed by a synchronizer
  */
trait Neuron extends NetworkEntity with EventHolder with PropertyHolder with Serializable {

  import Neuron._

  addEvent(NeuronFireEvent)

  private var fixedParameter = false
  addProperty[Boolean](FixedParameter, () => fixedParameter, b => {
    fixedParameter = b
    inputConnections.foreach { case(id, obj) =>
      obj.connection.setFixedParameter(b)
    }
  })


  var thisToSyncRef : NetworkEntityReference = _
  var syncToThisRef : NetworkEntityReference = _
  var synchronizerPath : NetworkEntityPath = _

  /**
    * List all incoming connection from input neurons to this neuron
    */
  protected[Neuron] val inputConnections : mutable.HashMap[ConnectionID, InputConnection] =
    mutable.HashMap[ConnectionID, InputConnection]()

  /**
    * Index the input connection by path
    * the value attribute is the index in inputConnections of the associated connection
    */
  protected[Neuron] val inputConnectionsPathIndex : mutable.HashMap[NetworkEntityPath, mutable.ArrayBuffer[InputConnection]] =
    mutable.HashMap[NetworkEntityPath, mutable.ArrayBuffer[InputConnection]]()

  /**
    * List all outgoing connection from this neuron to output neurons
    */
  protected[Neuron] val outputConnections : mutable.Set[OutputConnection] = mutable.Set[OutputConnection]()

  /**
    * Index the output connection by path
    * the value attribute is the index in outputConnections of the associated connection
    */
  protected[Neuron] val outputConnectionsPathIndex : mutable.HashMap[NetworkEntityPath, mutable.ArrayBuffer[OutputConnection]] =
    mutable.HashMap[NetworkEntityPath, mutable.ArrayBuffer[OutputConnection]]()

  private var currentTimestamp : Timestamp = 0
  def getCurrentTimestamp : Timestamp  = currentTimestamp

  /**
    * Retain the next id available for aa new connection
    */
  var nextFreeConnectionId : ConnectionID = 0

  /****************************************************************************************************
   * 																			SETUP CONNECTION
   ****************************************************************************************************/

  /**
    * generate the next id of connection
 *
    * @return a free connection id
    */
  def nextConnectionID : ConnectionID = {
    val nextId = this.nextFreeConnectionId
    this.nextFreeConnectionId += 1
    nextId
  }

  def createConnection(target : NetworkEntityPath, connection : NeuronConnection, properties : Seq[(ConnectionProperty[Any], Any)], preDelay : Timestamp) : ConnectionPath = {
    val response = getReferenceOf(target).ask(
      CreateNeuronInputConnectionWith(
        getNetworkAddress,
        synchronizerPath,
        if (isLocalReference(synchronizerPath)) Some(getReferenceOf(synchronizerPath).ask(AskReference(getNetworkAddress)).asInstanceOf[WrapMessage].content.asInstanceOf[NetworkEntityReference]) else None,
        connection,
        properties,
        preDelay
      )
    ).asInstanceOf[ConnectionCreationResponse]
    addOutputConnection(target, response.synchronizer, response.id, preDelay)
    ConnectionPath(getNetworkAddress, response.id)
  }

  /**
    * Create an incoming connection from sender with the specified synchronizer
    * This method will resolve the reference with the output neuron and the synchronizer.
    * It will create entry in inputConnections and inputConnectionsPathIndex attributes for the new connection
 *
    * @param sender is the input neuron path of the incoming connection
    * @param synchronizer is the synchronizer path in charge of this connection
    * @param connection is an optional parameter with the connection object. if no connection are provided, the connection returned by defaultConnection will be used
    * @return the id of the new connection
    */
  def addInputConnection(sender : NetworkEntityPath, synchronizer : NetworkEntityPath, syncToInputRef : Option[NetworkEntityReference], connection : NeuronConnection,  connectionProperties : Seq[(ConnectionProperty[Any], Any)]) : ConnectionID = {
    val newConnectionID = this.nextConnectionID
    val connectionObject = connection
    val synchronizerReference = getReferenceOf(synchronizer)
    val synchronizerToInputNeuron = syncToInputRef.getOrElse(
      synchronizerReference.ask(AskReference(sender)).asInstanceOf[WrapMessage].content.asInstanceOf[NetworkEntityReference]
    )
    if(connectionObject.ends.isDefined)
      throw new RuntimeException("Synapse already connected")

    val connectionObj = InputConnection(
      connectionObject,
      sender,
      synchronizerReference,
      synchronizerToInputNeuron
    )

    connectionObject.ends = Some(new ConnectionEnds(connectionObj, this, newConnectionID))
    connectionObject.initialize()

    this.inputConnections += newConnectionID -> connectionObj
    this.inputConnectionsPathIndex.getOrElseUpdate(sender, mutable.ArrayBuffer[InputConnection]()) += connectionObj

    connectionProperties.foreach { case(property, value) =>
      setConnectionProperty(property, value, newConnectionID)
    }

    newConnectionID
  }

  /**
    * Create an outgoing connection to neuron with the specified synchronizer
 *
    * @param neuron is the output neuron path of the outgoing connection
    * @param synchronizer is the synchronizer path in charge of this connection
    * @param connectionId is the id of the connection stored in the output neuron
    */
  def addOutputConnection(neuron : NetworkEntityPath, synchronizer : NetworkEntityPath, connectionId : ConnectionID, preDelay : Timestamp): Unit = {
    val syncRef = getReferenceOf(synchronizer)
    val connectionObj = OutputConnection(
      connectionId,
      neuron,
      syncRef,
      syncRef.ask(AskReference(neuron)).asInstanceOf[WrapMessage].content.asInstanceOf[NetworkEntityReference],
      preDelay
    )
    this.outputConnections += connectionObj
    this.outputConnectionsPathIndex.getOrElseUpdate(neuron, mutable.ArrayBuffer[OutputConnection]()) += connectionObj
  }

  def removeInputConnection(connectionId : ConnectionID) : Unit = {
    val obj = getInputConnection(connectionId)
    this.inputConnections.remove(connectionId)
    this.inputConnectionsPathIndex(obj.path) -= obj
  }

  def removeOutputConnection(path : NetworkEntityPath, connectionId : ConnectionID) : Unit = {
    this.outputConnectionsPathIndex.getOrElse(path, Seq()).filter(_.connectionId == connectionId).foreach{ connection =>
      this.outputConnections -= connection
      this.outputConnectionsPathIndex(path) -= connection
    }
  }

  def removeAllInputConnection() {
    this.inputConnections.foreach{ case(connectionId, connection) =>
      getReferenceOf(connection.path).ask(
        RemoveNeuronOutputConnection(getNetworkAddress, connectionId)
      )
    }

    this.inputConnections.clear()
    this.inputConnectionsPathIndex.clear()
  }

  def removeAllOutputConnection() {
    this.outputConnections.foreach { connection =>
      getReferenceOf(connection.path).ask(RemoveNeuronInputConnection(connection.connectionId))
    }

    this.outputConnections.clear()
    this.outputConnectionsPathIndex.clear()
  }

  def getInputConnection(id : ConnectionID): InputConnection = {
    if(!this.inputConnections.isDefinedAt(id)) {
      throw new RuntimeException("Unable to find input connection "+id+" in neuron "+getNetworkAddress)
    }

    this.inputConnections(id)
  }

  def getOutputConnection(connectionPath : ConnectionPath): OutputConnection = {
    val connection = this.outputConnectionsPathIndex.getOrElse(connectionPath.outputNeuron, Seq()).find(_.connectionId == connectionPath.connectionID)

    if(connection.isDefined) {
      throw new RuntimeException("Unable to find output connection "+connectionPath+" in neuron "+getNetworkAddress)
    }

    connection.get
  }

  /**
    * @return the number of incoming connections of this neuron
    */
  def getNumberOfInputConnections : Int = this.inputConnections.size

  /**
    * @return the number of outgoing connections of this neuron
    */
  def getNumberOfOutputConnections : Int = this.outputConnections.size

  def getOutputConnections  : Seq[OutputConnection] = this.outputConnections.toSeq
  def getOutputConnectionsByID(index: Int) : Seq[OutputConnection] = this.outputConnections.filter(_.connectionId == index).toSeq
  def getOutputConnectionsByPath(path: NetworkEntityPath) : Seq[OutputConnection] = this.outputConnections.filter(_.path == path).toSeq


  def getInputConnections  : immutable.Map[ConnectionID, InputConnection] = this.inputConnections.toMap

  /*

  def getOutputConnectionByPath(path: NetworkEntityPath) = this.outputConnectionsPathIndex(path)

  def getInputConnectionById(index: Int) = this.inputConnections(index)
  def getInputConnectionByPath(path: NetworkEntityPath) = this.inputConnectionsPathIndex(path)
*/
  /********************************************************************************************************************
    * Neuron Initialization
  	* Hook to initialize the neurons and its connected synapses.
    ******************************************************************************************************************/
    initializePropertyHolder(inputConnections)

  /********************************************************************************************************************
    * Spike Processing
    * *****************************************************************************************************************/

  /**
    * Redirect EventHolderMessage and PropertyMessage to their process method
    * Handle CreateNeuronConnectionWith and CreateNeuronInputConnectionWith messages for the connection creation
    * manage NeuronMessage by redirect content to the processSomaMessage method
 *
    * @param message is the content to be processed
    * @param sender is a reference of the sender, which can be used to send response
    */
  def receiveMessage(message : Message, sender : NetworkEntityReference) : Unit = message match {
    case m : EventHolderMessage => processEventHolderMessage(m)
    case m : PropertyMessage => processPropertyMessage(m, sender)

    /********************************************************************************************************************
      * Connection messages
      ******************************************************************************************************************/

    case NeuronConnectionRequests(requests) =>
      sender.send(NewConnections(requests.groupBy(_.target).flatMap{ case(target, list) =>
        getReferenceOf(target).ask(
          NeuronInputConnectionRequests(
            list.map { request =>
              CreateNeuronInputConnectionWith(
                getNetworkAddress,
                synchronizerPath,
                if (isLocalReference(synchronizerPath)) Some(getReferenceOf(synchronizerPath).ask(AskReference(getNetworkAddress)).asInstanceOf[WrapMessage].content.asInstanceOf[NetworkEntityReference]) else None,
                request.connection,
                request.connectionProperties,
                request.preDelay
              )
            }
          )).asInstanceOf[ConnectionCreationResponses].seq.map { response =>
            addOutputConnection(target, response.synchronizer, response.id, response.preDelay)
            ConnectionPath(target, response.id)
          }
      }.toSeq))
    case CreateNeuronConnectionWith(targetPath, connection, newProperties, preDelay) =>
      sender.send(NewConnection(createConnection(targetPath, connection, newProperties, preDelay)))
    case NeuronInputConnectionRequests(requests) =>
      sender.send(ConnectionCreationResponses(requests.map{ request =>
        ConnectionCreationResponse(addInputConnection(request.path, request.synchronizer, request.syncToInputRef, request.connection, request.connectionProperties), synchronizerPath, request.preDelay)
      }))
    case CreateNeuronInputConnectionWith(origin, synchronizer, syncToInputRef, connection, newProperties, _) =>
      sender.send(ConnectionCreationResponse(addInputConnection(origin, synchronizer, syncToInputRef, connection, newProperties), synchronizerPath, 0))
/*
    case RemoveNeuronConnectionWith(target) =>
      getReferenceOf(target).ask(RemoveInputRemoveNeuronConnection(removeOutputConnection(target)))
*/
    case RemoveNeuronInputConnection(id) =>
      removeInputConnection(id)
      sender.send(Done)
    case RemoveNeuronOutputConnection(path, id) =>
      removeOutputConnection(path, id)
      sender.send(Done)
    case Destroy =>
      removeAllInputConnection()
      removeAllOutputConnection()
      sender.send(Done)
      // TODO destroy current object

    case RoutedConnectionMessage(id, innerMessage) =>
      innerMessage match {
        case m : PropertyMessage =>
          processConnectionPropertyMessage(m, sender, id)
        case RemoveConnection =>
          getReferenceOf(inputConnections(id).path).ask(
            RemoveNeuronOutputConnection(getNetworkAddress, id)
          )
          removeInputConnection(id)
          sender.send(Done)
      }

    case m : ConnectionMessage =>
      currentTimestamp = m.timestamp
      inputConnections(m.connectionId).connection.processConnectionMessage(m.timestamp, m.message)
      sender.send(Done)

    case m : NeuronMessage =>
      currentTimestamp = m.timestamp
      val ends = new NeuronEnds(this, m.timestamp)

      val connection = if(m.entryConnection.isDefined && getInputConnection(m.entryConnection.get) != null)
        Some(m.entryConnection.get -> getInputConnection(m.entryConnection.get).connection)
      else
        None
      processSomaMessage(m.timestamp, m.message, connection, ends)
      sender.send(Done)
    case SetSynchronizer(path) =>
      synchronizerPath = path
      thisToSyncRef = getReferenceOf(path)
      syncToThisRef = thisToSyncRef.ask(AskReference(getNetworkAddress)).asInstanceOf[WrapMessage].content.asInstanceOf[NetworkEntityReference]
    case m =>
      throw new RuntimeException("Unknown message "+m)
  }

  /**
    * Send a message to the current soma
    */
  def sendToMyself(timestamp : Timestamp, message : Message): Unit = {
    if(timestamp >= currentTimestamp) {
      thisToSyncRef.send(NeuronMessage(timestamp, message, thisToSyncRef, syncToThisRef, None))
    }
    else
      throw new RuntimeException("can't send message with past timestamp")
  }

  /**
    * Method used to describe the behavior of the neuron model
    *
    * @param timestamp is the current time
    * @param message is the content
    */
  def processSomaMessage(timestamp : Timestamp, message : Message, fromConnection : Option[(ConnectionID, NeuronConnection)], ends : NeuronEnds) : Unit

  def hasFixedParameter : Boolean = fixedParameter
}

