package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import java.io.File
import javax.imageio.ImageIO
/*
object FontInput {

  def distributedInput(filename: String, characterWidth : Int, characterHeight : Int, labels : List[String], oversampling : Int, distribution: InputDistribution = InputDistribution.defaultPoissonDistribution) = {
    new SpikeGeneratorStream(this.rawInput(filename, characterWidth, characterHeight, labels, oversampling), distribution)
  }

  def rawInput(filename: String, characterWidth : Int, characterHeight : Int, labels : List[String], oversampling : Int) = {
    new FontInputStream(filename, characterWidth, characterHeight, labels, oversampling)
  }
}
*/
/**
  * Created by falezp on 20/04/16.
  */
class FontInputStream(filename : String, characterWidth : Int, characterHeight : Int, labels : List[String]) extends
  InputGenerator[InputSample2D[Float]]{

  val image = ImageIO.read(new File(filename))
  val characters = labels.zipWithIndex.map{ case (name, c) =>
    (0 until characterHeight).map{ y =>
      (0 until characterWidth).map(x => if((image.getRGB(c*characterWidth+x, y) & 0xFF) > 128) 0f else 1f)
    }
  }

  val duplicate = 100
  var cursor = 0

  def atEnd() : Boolean = cursor < characters.size

  def next() = {
    val next = new InputSample2D(
      for(i <- 0 until characterWidth) yield {
        for (j <- 0 until characterHeight) yield characters(cursor)(i)(j)
      }
    ).setMetaData(InputLabel(labels(cursor)))
    cursor += 1
    next
  }

  def reset() = {
    cursor = 0
  }

  override def shape: Shape = Shape(characterWidth, characterHeight)
}
