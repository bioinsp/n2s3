package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

/**
  * Created by falezp on 26/07/16.
  */

import java.io.{EOFException, File, FileInputStream}
import java.nio.ByteBuffer

/**
  * Represents an event of an AER input (e.g., a camera).
  * An AER event possess the address where the event occurred (e.g., which pixel in the image) and the timestamp when this happened.
  */
case class AEREvent(address : Short, Timestamp : Int)

/**
  * Reader of files in AER format.
  * This reader assumes that events inside the file are 6 bytes long.
  * Events are read in a buffered way in chunks of 64 events.
  *
  * Read events are returned as [[AEREvent]] objects with the address of the event in the file and the timestamp where it occurred.
  */
class AERFileReader(filename : String) {

  private val eventSize = 6
  private val chunkSize = 64

  private val file = new File(filename)

  private val reader = new FileInputStream(file).getChannel

  private val currentChunk = ByteBuffer.allocate(chunkSize*eventSize)
  private var currentChunkSize = 0

  readNextChunk()
  currentChunk.getShort

  private val firstTimestamp = currentChunk.getInt

  currentChunk.clear()



  def nextEvents(number : Int) : Traversable[AEREvent] = {
    (0 until number).map(_ => readNextEvent())
  }

  def readNextEvent() : AEREvent = {
    if(!currentChunk.hasRemaining)
      readNextChunk()

    val address = currentChunk.getShort
    val timestamp = currentChunk.getInt

    AEREvent(address, timestamp-firstTimestamp)
  }

  def readNextChunk() : Unit = {
    currentChunk.clear()

    do {
      currentChunkSize = reader.read(currentChunk)

      if(currentChunkSize == -1) {
        if(currentChunk.position() == 0)
          throw new EOFException
        else
          return
      }

    } while (currentChunk.hasRemaining)

    currentChunk.flip()
  }

  def close() : Unit = {
    reader.close()
  }
}
