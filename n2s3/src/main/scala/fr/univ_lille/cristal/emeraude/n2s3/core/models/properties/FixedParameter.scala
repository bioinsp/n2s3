package fr.univ_lille.cristal.emeraude.n2s3.core.models.properties

import fr.univ_lille.cristal.emeraude.n2s3.core.Property

/**
  * Created by pfalez on 24/05/17.
  */
object FixedParameter extends Property[Boolean]