package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import java.awt.{Dimension, FlowLayout, Graphics}

import javax.swing.{JFrame, JPanel, WindowConstants}
import akka.actor.{ActorRef, Props}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Done
import fr.univ_lille.cristal.emeraude.n2s3.core.event.{LabelChangeEvent, LabelChangeResponse, SubscribeSynchronized, SynchronizedEvent}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapticWeightFloat
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupObserverRef}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{LocalActorDeploymentStrategy, Message}

/**
  * Created by pfalez on 10/05/17.
  */
class SynapticWeightDiffSelectGraphRef(n2s3 : N2S3, list : Seq[Seq[Seq[ConnectionPath]]], normalize : Boolean = false, synapseSize : Int = 4, refreshRate : Int = 1000/24, name : String = "") extends NeuronGroupObserverRef {
  var actor: Option[ActorRef] = None
  def getActors : Seq[ActorRef] = this.actor.toSeq


  override def deploy(n2s3: N2S3): Unit = {
    this.actor = Some(n2s3.system.actorOf(Props(new SynapticWeightDiffSelectGraph(n2s3, list, normalize, synapseSize, refreshRate, name)), LocalActorDeploymentStrategy))
  }
}

class SynapticWeightDiffSelectGraph(n2s3 : N2S3, list : Seq[Seq[Seq[ConnectionPath]]], normalize : Boolean, synapseSize : Int = 4, refreshRate : Int = 1000/24, name : String) extends NetworkActor {

  class WeightPanel(list : Seq[Seq[ConnectionPath]]) extends JPanel {

    val synapseList = list.zipWithIndex.flatMap{case(l, x) => l.zipWithIndex.map{case(p, y) => (x, y, p)}}.groupBy(_._3.outputNeuron)
    val xMax = synapseList.flatMap(_._2.map(_._1)).max
    val yMax = synapseList.flatMap(_._2.map(_._2)).max

    setPreferredSize(new Dimension((xMax+1)*synapseSize, (yMax+1)*synapseSize))
    setMinimumSize(new Dimension((xMax+1)*synapseSize, (yMax+1)*synapseSize))

    var values = synapseList.flatMap(_._2.map(e => (e._1, e._2, 0.5f)))
    var oldEntry = Map[ConnectionPath, Float]()

    def update(): Unit = {
      val newEntry = synapseList.flatMap{ case(neuron, connectionList) =>
        if(connectionList.size == 1) {
          Some((connectionList.head._3,
            ExternalConnectionSender.askTo(connectionList.head._3, GetConnectionProperty(SynapticWeightFloat))
              .asInstanceOf[PropertyValue[Float]].value))
        }
        else {
          val values = ExternalSender.askTo(neuron, GetAllConnectionProperty(SynapticWeightFloat))
            .asInstanceOf[ConnectionPropertyValues[Float]].values.groupBy(_._1)
          connectionList.map{case(x, y, p) => (p, values(p.connectionID).head._3)}
        }
      }

      values = synapseList.flatMap{ case(neuron, connectionList) =>
        connectionList.map { case(x, y, path) =>
            (x, y, if(oldEntry.isDefinedAt(path) && newEntry.isDefinedAt(path)) newEntry(path)-oldEntry(path) else 0f)
        }
      }

      val maxValue = values.map(e => math.abs(e._3)).max
      values = values.map{case(x, y, value) => (x, y, if(maxValue == 0f) 0.5f else (value/maxValue)/2f+0.5f)}

      oldEntry = newEntry
    }

    override def paintComponent(g: Graphics): Unit = {
      SynapticWeightSelectGraph.paint(values, g, synapseSize, SynapticWeightSelectGraph.grayScale)
    }

  }

  val frame = new JFrame()
  frame.setLayout(new FlowLayout())

  val panels : Seq[WeightPanel] = list.map{ l =>
    val panel = new WeightPanel(l)
    frame.add(panel)
    panel
  }

  frame.setTitle(name)

  frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
  frame.validate()
  //frame.setSize(600, 400)
  frame.pack()
  frame.setVisible(true)

  override def initialize(): Unit = {
    askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))

  }

  override def destroy(): Unit = {
    askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))
  }

  override def process(message: Message, sender : ActorRef): Unit = message match {
    case SynchronizedEvent(s, _, m, _) =>
      m match {
        case LabelChangeResponse(start, end, label) =>

          panels.foreach{ panel =>
            panel.update()
            panel.repaint()
            panel.revalidate()
          }
      }
      ExternalSender.sendTo(s, Done)
  }
}
