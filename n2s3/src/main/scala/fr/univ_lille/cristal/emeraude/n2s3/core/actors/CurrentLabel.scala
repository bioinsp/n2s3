package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import akka.actor.ActorRef
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**********************************************************************************************
 * Message send for wait end of computation of the receiver
 *********************************************************************************************/
case class CurrentLabel(timestamp : Timestamp, end : Timestamp, destination : ActorRef) extends Message
