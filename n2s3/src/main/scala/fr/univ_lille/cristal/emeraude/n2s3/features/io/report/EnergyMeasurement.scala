package fr.univ_lille.cristal.emeraude.n2s3.features.io.report

import akka.actor.{Actor, Props}
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done}
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapticWeightFloat
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy
import squants.time.Microseconds
import squants.{Energy, Power}

import scala.collection.mutable
import scala.concurrent.Await
/**
  * Created by falezp on 20/01/17.
  */
abstract class EnergyConsumptionModel {
  def energyPerSpike : Energy
  def energyPerFire : Energy
  def defaultNeuronStaticPower : Power
  def defaultSynapseStaticPower : Power
}

class EnergyConsumptionResult(model : EnergyConsumptionModel,
                              duration : squants.Time, val spikeCount : Int, val fireCount : Int, neuronCount : Int, synapseCount : Int) {
  val totalFireEnergy : Energy = model.energyPerFire*fireCount
  val totalSpikeEnergy : Energy = model.energyPerSpike*spikeCount

  val totalNeuronStaticEnergy : Energy = model.defaultNeuronStaticPower*duration*neuronCount
  val totalSynapseStaticEnergy : Energy = model.defaultSynapseStaticPower*duration*synapseCount

  val totalEnergy : Energy = totalFireEnergy+totalSpikeEnergy+totalNeuronStaticEnergy+totalSynapseStaticEnergy
  val totalPower : Power = totalEnergy/duration
}

class EnergyMeasurement(n2s3 : N2S3, model : EnergyConsumptionModel) {

  object GetResult
  object Close

  implicit val timeout = Config.longTimeout

  class EnergyMeasurementActor(n2s3 : N2S3, model : EnergyConsumptionModel) extends Actor {

    val successors = mutable.HashMap[NetworkEntityPath, Int]()

    var fireCounter = 0
    var spikeCounter = 0

    n2s3.layers.flatMap(_.neuronPaths).foreach { path =>
      (ExternalSender.askTo(path, GetAllConnectionProperty(SynapticWeightFloat)) match {
        case PropertyValue(values: Seq[(NetworkEntityPath, Float)]@unchecked) => values
      }).foreach{ case (input, _) =>
        successors(input) = successors.getOrElse(input, 0)+1
      }

      ExternalSender.askTo(path, Subscribe(NeuronFireEvent, ExternalSender.getReference(self)))
    }

    val neuronCount = n2s3.layers.tail.map(_.neuronPaths.size).sum
    val synapseCount = successors.map(_._2).sum

    var firstTimestamp : Option[Timestamp] = None
    var lastTimestamp : Option[Timestamp] = None



    override def receive = {
      case NeuronFireResponse(timestamp, source) =>
        fireCounter += 1
        spikeCounter += successors.getOrElse(source, 0)

        if(firstTimestamp.isEmpty)
          firstTimestamp = Some(timestamp)

        lastTimestamp = Some(math.max(timestamp, lastTimestamp.getOrElse(0L)))

      case GetResult => sender ! new EnergyConsumptionResult(model, Microseconds(lastTimestamp.get-firstTimestamp.get), spikeCounter, fireCounter, neuronCount, synapseCount)
      case Done => sender ! Done
      case Close =>
        n2s3.layers.flatMap(_.neuronPaths).foreach { path =>
          ExternalSender.askTo(path, Unsubscribe(NeuronFireEvent, ExternalSender.getReference(self)))
        }
        sender ! Done
        context stop self
      case m => throw new RuntimeException("Unrecognized message : "+m)
    }

  }

  val actor = n2s3.system.actorOf(Props(new EnergyMeasurementActor(n2s3, model)), LocalActorDeploymentStrategy)
  Await.result(actor ? Done, timeout.duration)

  def results : EnergyConsumptionResult = Await.result(actor ? GetResult, timeout.duration).asInstanceOf[EnergyConsumptionResult]

  def destroy() : Unit = Await.result(actor ? Close, timeout.duration)
}
