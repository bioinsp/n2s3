package fr.univ_lille.cristal.emeraude.n2s3.support

import java.io.{File, FileInputStream}
import java.net.URL
import java.util.Properties

import org.apache.commons.io.{FileUtils, FilenameUtils}

import scala.sys.process._

/**
  * Created by guille on 3/15/17.
  */

object ResourceAddress {

  def get(name : String) : Option[String] = name match {

    case "mnist-train-images" => Some("http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz")
    case "mnist-train-labels" => Some("http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz")
    case "mnist-test-images" => Some("http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz")
    case "mnist-test-labels" => Some("http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz")
    case "freeway-dvs" => Some("http://www.ini.uzh.ch/~tobi/dvs/events20051221T014416%20freeway.mat.dat")
    case "freeway-test-dvs" => Some("https://sourcesup.renater.fr/frs/download.php/file/5283/freeway2.mat.dat")
    case _ => None
  }

}
object N2S3ResourceManager {

  def isCompressedFile(url : URL) : Boolean = FilenameUtils.getExtension(url.getPath) match {
    case "zip" => true
    case "gz" => true
    case _ => false
  }

  def uncompress(input : File, output : File) : Unit = FilenameUtils.getExtension(input.getPath) match {
    case "zip" => s"unzip ${input.getAbsolutePath} -d ${output.getAbsolutePath}" !!
    case "gz" =>
      println(input.getAbsolutePath)
      s"gzip  -d ${input.getAbsolutePath}" !!
      val newFile = new File(input.getAbsolutePath.substring(0, input.getAbsolutePath.length-3))
      newFile.renameTo(output)
  }

  def getByName(name : String) : File = {
    val resourceCache = new File("resource-cache")
    FileUtils.forceMkdir(resourceCache)

    //val file = FilenameUtils.getName(url.getPath)
    val destinationFile = new File(resourceCache, name)

    if (destinationFile.exists()) {
      destinationFile
     /* if(FilenameUtils.getExtension(url.getPath) == "zip") {
        destinationFile = new File(resourceCache, FilenameUtils.getBaseName(file))
      }
      return destinationFile*/
    }
    else { // Resolve
      val address = ResourceAddress.get(name).orElse {
        val resourceList = new Properties()

        val file = new File("resource.conf")

        if (!file.exists()) {
          println("[Warning] No resource.conf file found")
        }

        resourceList.load(new FileInputStream(file))
        Option(resourceList.getProperty(name))
      }

      if (address.isEmpty)
        throw new RuntimeException("Unable to resolve resource address \"" + name + "\" (checked in ResourceAddress and resource.conf)")

      val url = new URL(address.get)

      if (isCompressedFile(url)) {
        val tmpFile = new File(resourceCache, FilenameUtils.getName(url.getPath))
        println("Downloading "+url.getPath+" to "+tmpFile.getPath)
        url #> tmpFile !!

        println("Uncompress "+tmpFile.getPath+" to "+destinationFile.getPath)
        uncompress(tmpFile, destinationFile)
      }
      else {
        println("Downloading "+url.getPath+" to "+destinationFile.getPath)
        url #> destinationFile !!
      }
      destinationFile
    }
  }

  def get(urlString: String): File = this.get(new URL(urlString))

  def get(url: URL  ): File = {

    val resourceCache = new File("resource-cache")
    FileUtils.forceMkdir(resourceCache)

    val file = FilenameUtils.getName(url.getPath)
    var destinationFile = new File(resourceCache, file)

    if (destinationFile.exists()){
      if(FilenameUtils.getExtension(url.getPath) == "zip") {
        destinationFile = new File(resourceCache, FilenameUtils.getBaseName(file))
      }
      return destinationFile
    }

    // If the file does not exist, we download the file
    url #> destinationFile !!

    // If it is a zip file we unzip it and return the unzipped file
    if(FilenameUtils.getExtension(url.getPath) == "zip") {
      destinationFile = new File(resourceCache, FilenameUtils.getBaseName(file))
      s"unzip $file -d ${destinationFile.getAbsolutePath}" !!
    }
    destinationFile
  }
}
