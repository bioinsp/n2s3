package fr.univ_lille.cristal.emeraude.n2s3.core.models.properties

import fr.univ_lille.cristal.emeraude.n2s3.core.Property
import fr.univ_lille.cristal.emeraude.n2s3.support.Time

/**
  * Created by pfalez on 09/05/17.
  */
object MembraneInhibitionDuration extends Property[Time]