package fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence

import java.io._

import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.persistence.ActivityPersistenceModel.EventFactory
import fr.univ_lille.cristal.emeraude.n2s3.support.FormatTimestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

import scala.collection.mutable

/**
  * Created by pfalez on 26/04/17.
  */

case class ActivityLoadEntry(s : Shape) extends InputFormat[InputTemporalPacket](s) {
  def DataFrom(filename : String, chunkSize : Int = 64) = new ActivityLoad(filename, chunkSize)
}

object ActivityLoad {
  def getShapeOf(filename : String) : Shape = {
    val reader = new DataInputStream(new FileInputStream(filename))
    val dimNumber : Int = reader.readInt()
    val shape = Shape((0 until dimNumber).map(_ => reader.readInt()):_*)
    reader.close()
    shape
  }
}

class ActivityLoad(filename: String, chunkSize : Int = 64) extends InputGenerator[InputTemporalPacket] {

  var reader = new DataInputStream(new FileInputStream(filename))
  val dimNumber : Int = reader.readInt()
  val s = Shape((0 until dimNumber).map(_ => reader.readInt()):_*)
  val totalEvent : Long = reader.readLong()
  println(totalEvent+" events")
  var currentEvent : Long = 0
  var currentTimestamp : Timestamp = 0

  val metaDataList : mutable.ArrayBuffer[N2S3InputMetaData] = mutable.ArrayBuffer()
  val dataList : mutable.ArrayBuffer[(Long, N2S3InputData)] = mutable.ArrayBuffer()

  override def shape: Shape = s
/*
  override def next(): InputTemporalPacket = {
    val metaDataList : mutable.ArrayBuffer[N2S3InputMetaData] = mutable.ArrayBuffer()
    val dataList : mutable.ArrayBuffer[(Long, N2S3InputData)] = mutable.ArrayBuffer()

    while(dataList.size < chunkSize && !atEnd()) {
      val(currMetaData, currData) = nextTimestamp()
      metaDataList ++= currMetaData
      dataList ++= currData
      currentEvent += currMetaData.size+currData.size
    }

    new InputTemporalPacket(s, dataList.groupBy(_._1).map{ case(index, list) =>
      s.fromIndex(index) -> list.map(_._2)
    }).setMetaData(metaDataList:_*)
  }
*/

  override def next(): InputTemporalPacket = {

    var packet : InputTemporalPacket  = null

    var ok = false

    while(!ok && !atEnd()) {
      val(currMetaData, currData) = nextTimestamp()

      if(currMetaData.nonEmpty && currMetaData.head.isInstanceOf[N2S3InputLabel]) {
        if(dataList.nonEmpty && metaDataList.nonEmpty) {
          val mlist = metaDataList.toArray
          packet = new InputTemporalPacket(s, dataList.groupBy(_._1).map{ case(index, list) =>
            s.fromIndex(index) -> list.map(_._2)
          }, metaDataList.head.getStartTimestamp, metaDataList.head.getDuration).setMetaData(mlist:_*)
          ok = true
        }



        metaDataList.clear()
        dataList.clear()

      }

      metaDataList ++= currMetaData
      dataList ++= currData
      currentEvent += currMetaData.size+currData.size
    }
    if(packet == null) new InputTemporalPacket(s, Map()) else packet
  }

  def nextTimestamp() : (Seq[N2S3InputMetaData], Seq[(Long, N2S3InputData)]) = {
    currentTimestamp = reader.readLong()
    val metaDataSize = reader.readInt()
    val dataSize = reader.readInt()

    (
      (0 until metaDataSize).map { _ =>
        EventFactory.createMetaData(reader, currentTimestamp)
      },
      (0 until dataSize).map { _ =>
        EventFactory.createData(reader, currentTimestamp)
      }
    )
  }

  override def reset(): Unit = {
    reader.close()
    reader = new DataInputStream(new FileInputStream(filename))
    reader.readInt()
    (0 until dimNumber).foreach(_ => reader.readInt())
    reader.readLong()
    currentEvent = 0
    currentTimestamp = 0
  }

  override def atEnd(): Boolean = {
    currentEvent >= totalEvent
  }

  override def toString: String = {
    "[ActivityLoad] "+filename+" : "+ currentEvent + " / " + totalEvent+" ("+FormatTimestamp.format(currentTimestamp)+")"
  }

}