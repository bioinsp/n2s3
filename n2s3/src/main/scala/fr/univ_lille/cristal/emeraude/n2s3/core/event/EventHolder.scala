package fr.univ_lille.cristal.emeraude.n2s3.core.event

import fr.univ_lille.cristal.emeraude.n2s3.core.exceptions.EventNotFoundException
import fr.univ_lille.cristal.emeraude.n2s3.core.{NetworkEntityPath, NetworkEntityReference}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
 * Class which have a behavior close of Observable design pattern
 * Output entities can subscribe to available events of the subclass
 */
trait EventHolder {

	/**
		* Contains the list of declared event, associated with all observers
		*/
	private val events = mutable.HashMap[Event[_ <: EventResponse], mutable.ArrayBuffer[NetworkEntityReference]]()
	private val synchronizedEvents = mutable.HashMap[TimedEvent[_ <: TimedEventResponse], mutable.ArrayBuffer[(NetworkEntityReference, NetworkEntityPath, NetworkEntityReference)]]()

	/**
		* This method allow to declare an event which can be handled
		*
		* @param event is the type of the event
		*/
	def addEvent(event : Event[_ <: EventResponse]): Unit = {
		if(!events.isDefinedAt(event)) {
			events += event -> ArrayBuffer[NetworkEntityReference]()

			if (event.isInstanceOf[TimedEvent[_]])
				synchronizedEvents += event.asInstanceOf[TimedEvent[TimedEventResponse]] -> ArrayBuffer[(NetworkEntityReference, NetworkEntityPath, NetworkEntityReference)]()
		}
	}

	/**
		* Subscribe an observer to the specified event
		*
		* @param entity is the entity which will receive response when event is triggered
		* @param event is the event to be observed
		* @throws EventNotFoundException is the event is not declared in this class
		*/
	def subscribeTo(entity : NetworkEntityReference, event : Event[_ <: EventResponse]) : Unit = {
		events.get(event) match {
			case Some(list) => list += entity
			case None => throw new EventNotFoundException(event, this.getClass)
		}
	}

	/**
		* Subscribe an observer to the specified event. Triggered message will pass through the synchronizer
		*
		* @param entity is the entity which will receive response when event is triggered
		* @param event is the event to be observed
		* @throws EventNotFoundException is the event is not declared in this class
		*/
	def subscribeSynchronizedTo(entity : NetworkEntityReference, event : TimedEvent[_ <: TimedEventResponse], synchronizer : NetworkEntityPath) : Unit = {
		synchronizedEvents.get(event) match {
			case Some(list) => list += ((referenceToSynchronizer(synchronizer), synchronizer, entity))
			case None => println("Warning : Event \"" + event.getClass + "\" can't be found in " + this.getClass)  //throw new EventNotFoundException(event, this.getClass)
		}
	}

	/**
		* Unsubscribe an observer to the specified event
		*
		* @param entity is the entity which will receive response when event is triggered
		* @param event is the event to be observed
		* @throws EventNotFoundException is the event is not declared in this class
		*/
	def unsubscribeTo(entity : NetworkEntityReference, event : Event[_ <: EventResponse]) : Unit = {
		events.get(event) match {
			case Some(list) => list -= entity
			case None => //Nothing
		}

		if(event.isInstanceOf[TimedEvent[_]]) {
			synchronizedEvents.get(event.asInstanceOf[TimedEvent[TimedEventResponse]]) match {
				case Some(list) => synchronizedEvents.update(event.asInstanceOf[TimedEvent[TimedEventResponse]], list.filter(_._3 != entity))
				case None => //Nothing
			}
		}
	}

	/**
		* Trigger an event by sent default response of this event to all observers.
		* if this event is single usage, all observers will be removed
		*
		* @param event is the event to trigger
		* @tparam Response is the response type of the event
		*/
	def triggerEvent[Response <: EventResponse](event : Event[Response]) : Unit = {
		triggerEventWith(event, event.defaultResponse)
	}

	/**
		* Trigger an event by sent specified response to all observers.
		* if this event is single usage, all observers will be removed
		*
		* @param event is the event to trigger
		* @param response is the response which will be sent to observers
		* @tparam Response is the response type of the event
		*/
	def triggerEventWith[Response <: EventResponse](event : Event[Response], response : Response) : Unit = {
		events.get(event) match {
			case Some(subscribers) =>
				subscribers.foreach(subscriber => subscriber.send(response))
				if(event.isSingleUsage)
					subscribers.clear()
			case None =>
		}

		response match {
			case message : TimedEventResponse =>
				synchronizedEvents.get(event.asInstanceOf[TimedEvent[TimedEventResponse]]) match {
					case Some(subscribers) =>
						subscribers.foreach{ case (synchronizer, path, subscriber) => synchronizer.send(SynchronizedEvent(path, subscriber, message, message.timestamp)) }
						if(event.isSingleUsage)
							subscribers.clear()
					case None =>
				}
			case _ =>
		}
	}

	/**
		* method which allow to process all EventHolderMessage
		*
		* @param message is the content
		*/
	def processEventHolderMessage(message : EventHolderMessage) : Unit = {
		message match {
			case Subscribe(event, ref) =>
				subscribeTo(ref, event)
				/*if(event == LabelChangeEvent)
					println("subscribe to LabelChangeEvent : "+this+" => "+ref)*/
			case SubscribeSynchronized(event, ref, synchronizer) =>
				subscribeSynchronizedTo(ref, event, synchronizer)
				/*if(event == LabelChangeEvent)
					println("subscribe sync to LabelChangeEvent : "+this+" => "+ref)*/
			case Unsubscribe(event, ref) =>
				/*if(event == LabelChangeEvent)
					println("unsubscribe to LabelChangeEvent : "+this+" => "+ref)*/
				unsubscribeTo(ref, event)
		}
	}

	def referenceToSynchronizer(synchronizer : NetworkEntityPath) : NetworkEntityReference

}
