package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import java.io.File
import javax.imageio.ImageIO

import scala.util.Random
/**
  * Created by falezp on 18/08/16.
  */
object LFWEntry {
  val width = 250
  val height = 250

  def apply() = new StreamEntry[InputSample2D[Float]](Shape(width, height))
}

class LFWReader(directoryPath : String) extends InputGenerator[InputSample2D[Float]] {

  val mainDirectory = new File(directoryPath)

  if(!mainDirectory.isDirectory)
    throw new RuntimeException("Bad directory : "+directoryPath)

  override def shape: Shape = Shape(LFWEntry.width, LFWEntry.height)

  override def next(): InputSample2D[Float] = {

    val subDirectories = mainDirectory.listFiles()
    val selectedDirectory = subDirectories(Random.nextInt(subDirectories.length))
    val images = selectedDirectory.listFiles()
    val selectedImage = images(Random.nextInt(images.length))
    val image = ImageIO.read(selectedImage)

    val label =  "_"
    println("[LWF] "+selectedImage.getPath)
    new InputSample2D(for(i <- 0 until LFWEntry.width) yield {
      for(j <- 0 until LFWEntry.height) yield (image.getRGB(i, j) & 0xFF).toFloat/255f
    }).setMetaData(InputLabel(label))
  }

  override def atEnd(): Boolean = false

  override def reset(): Unit = {}
}
