package fr.univ_lille.cristal.emeraude.n2s3.features.learning

import akka.actor.{Actor, Props}
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done}
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapseWeightAndDelay
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias._
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy

import scala.collection.mutable
import scala.concurrent.Await
/**
  * Created by falezp on 26/10/16.
  */

/*
 * From "A supervised learning approach based on STDP and polychronization in spiking neuron network"
 * http://publications.idiap.ch/downloads/papers/2007/paugam-esann-2007.pdf
 */

class DelayAdaptationExtended(n2s3 : N2S3, outputs : Map[String, NetworkEntityPath], learningRate : Double, expectedDelay : Time, unexpectedDelay : Time) {

  object Close

  implicit val timeout = Config.longTimeout

  class DelayAdaptationActor(n2s3 : N2S3, outputs : Map[String, NetworkEntityPath], learningRate : Double, expectedDelay : Time) extends Actor {

    var currentInputTimestamp : Option[Timestamp] = None
    var currentLabel : String = ""

    var spikeHistory = mutable.HashMap[NetworkEntityPath, Timestamp]()

    ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))

    n2s3.layers.flatMap(_.neuronPaths).foreach { path =>
      ExternalSender.askTo(path, SubscribeSynchronized(NeuronFireEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))
    }

    ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))


    override def receive = {
      case SynchronizedEvent(s, _, m, _) =>
        m match {
          case LabelChangeResponse(start, end, label) =>
            currentInputTimestamp = Some(start)
            currentLabel = label
            spikeHistory.clear()
          case NeuronFireResponse(timestamp, source) =>

            if(currentInputTimestamp.isDefined) {
              outputs.foreach { case (label, path) =>
                if (path == source && !spikeHistory.isDefinedAt(source)) {
                  //println(path+" fire d="+Time(timestamp-currentInputTimestamp.get).asMilliSecond+" ["+(if (label == currentLabel) "GOOD" else "BAD")+"]")


                  //println("### "+path)

                  val current_synapses = ExternalSender.askTo(source, GetAllConnectionProperty(SynapseWeightAndDelay))
                    .asInstanceOf[ConnectionPropertyValues[(Float, Time)]].values

                  ExternalSender.askTo(source, SetAllConnectionProperty(SynapseWeightAndDelay, current_synapses.map { case (connectionId, inputNeuron, (w, d)) =>
                    val last_input_fire = spikeHistory.getOrElse(inputNeuron, -1L)
                    val desired_timestamp = currentInputTimestamp.get+(if (label == currentLabel) expectedDelay.timestamp else unexpectedDelay.timestamp)
                    val desired_delay = if(last_input_fire < 0) d.timestamp else desired_timestamp-last_input_fire
                    // todo -> weight

                    val n_d = if(d.timestamp > desired_delay)
                      d.timestamp-(1 MilliSecond).timestamp//d.timestamp-learningRate*(d.timestamp-desired_delay).toDouble
                    else
                      d.timestamp+(1 MilliSecond).timestamp//d.timestamp+learningRate*(desired_delay-d.timestamp).toDouble
/*
                    if(n_d != d.timestamp)
                    println(input_neuron+"->"+path+" ["+label+"<>"+currentLabel+"] desired_delay="+Time(desired_delay).asMilliSecond+", d="+d.asMilliSecond+",  n_d="+Time(math.max(n_d.toLong, 0L)).asMilliSecond)
*/
                    (connectionId, (w, Time(math.max(n_d.toLong, 0L))))
                  }))
                }
              }
              spikeHistory(source) = timestamp
            }
          case _ => throw new RuntimeException("Unrecognized message : "+m)
        }
        ExternalSender.sendTo(s, Done)
      case Close =>
        ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, Unsubscribe(LabelChangeEvent, ExternalSender.getReference(self)))

        n2s3.layers.flatMap(_.neuronPaths).foreach { path =>
          ExternalSender.askTo(path, Unsubscribe(NeuronFireEvent, ExternalSender.getReference(self)))
        }
        sender ! Done
        context stop self
      case Done => sender ! Done
      case m => throw new RuntimeException("Unrecognized message : "+m)
    }
  }

  val actor = n2s3.system.actorOf(Props(new DelayAdaptationActor(n2s3, outputs, learningRate, expectedDelay)), LocalActorDeploymentStrategy)
  Await.result(actor ? Done, timeout.duration)

  def destroy() : Unit = {
    Await.result(actor ? Close, timeout.duration)
  }
}