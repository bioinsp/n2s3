/************************************************************************************************************
 * Contributors: 
 * 		- created by Pierre Falez
 ***********************************************************************************************************/

package fr.univ_lille.cristal.emeraude.n2s3.features.io.report

import java.io.{File, PrintWriter}
import java.text.SimpleDateFormat
import java.util.Date

abstract class ReportHTML extends Report {

  def addCategory(name : String, f : PrintWriter => Unit) : Unit = {
    writer.println(s"<h2>$name</h2>")
    f(writer)
  }

  def save(filename : String) : Unit = {
    writer = new PrintWriter(new File(filename))

    val dateFormater = new SimpleDateFormat("dd/MM/yyyy 'at' hh:mm:ss");
    val datestr = dateFormater.format(new Date())
    writer.println(
      s"""<!DOCTYPE html>
         <html lang="fr">
         <head>
         <meta charset="utf-8">
         <title>Report</title>
         <style>
         table { min-width: 100%; border-collapse: collapse; }
         th { background-color : #222; color: #fff; border : 1px solid white;}
         td { border : 1px solid black; }
         </style>
         </head>
         <body>
         <h1>N2S3 Experiment Report</h1>
         <b>Generated </b> : $datestr""" )

    addCategory("Experiment configuration", { writer =>
      experimentParameter.foreach { case (key, value) =>
        writer.println(s"<b>$key</b> : $value<br/>")
      }

    })

    generateContent(writer)

    writer.println(
      """</body>
         </html>""")

    writer.close()
  }

  def generateContent(writer : PrintWriter) : Unit
}
