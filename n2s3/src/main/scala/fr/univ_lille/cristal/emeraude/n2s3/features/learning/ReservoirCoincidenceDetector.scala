package fr.univ_lille.cristal.emeraude.n2s3.features.learning

import akka.actor.{Actor, Props}
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done}
import fr.univ_lille.cristal.emeraude.n2s3.core.event._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.{MembraneLeakTime, MembraneThresholdFloat, SynapseWeightAndDelay}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy

import scala.collection.mutable
import scala.concurrent.Await

/**
  * Created by falezp on 15/12/16.
  */
class ReservoirCoincidenceDetector(n2s3 : N2S3, targetNeuron : Seq[NetworkEntityPath], learningRate : Double, updateNetwork : Boolean = false) {

  object Close

  implicit val timeout = Config.longTimeout

  class ReservoirCoincidenceDetectorActor(n2s3 : N2S3, targetNeuron : Seq[NetworkEntityPath], learningRate : Double) extends Actor {

    val initialMembraneTimeConstant : Time = 5 MilliSecond // useful ?
    val noise_tolerance : Time = 1 MilliSecond
    val minPreInfluence = 0.5

    val connections = mutable.ArrayBuffer[(NetworkEntityPath, NetworkEntityPath, Timestamp, Double)]()
    val forwardConnections = mutable.HashMap[NetworkEntityPath, Seq[Int]]()
    val backwardConnections = mutable.HashMap[NetworkEntityPath, Seq[Int]]()


    val neurons = mutable.HashMap[NetworkEntityPath, (Double, Timestamp)]()

    val neuronSilenceCorrection = mutable.HashMap[NetworkEntityPath, Int]()

    val inputPattern = mutable.HashMap[NetworkEntityPath, Timestamp]()
    var currentInputStart : Option[Timestamp] = None
    var currentInputLabel : Option[String] = None

    var currentTimestamp : Timestamp = 0
    val currentTimestampFire = mutable.ArrayBuffer[NetworkEntityPath]()

    val neuronSubscribed = mutable.ArrayBuffer[NetworkEntityPath]()


    load()

    override def receive = {
      case SynchronizedEvent(s, _, m, _) =>
        m match {
          case LabelChangeResponse(start, end, label) =>
            if(start > currentTimestamp) {

              currentTimestampFire.foreach { neuron =>
                if(!inputPattern.isDefinedAt(neuron))
                  inputPattern += neuron -> currentTimestamp
              }

              currentTimestampFire.clear()
              currentTimestamp = start
            }


            if(currentInputStart.isDefined && currentInputLabel.isDefined) {
              execute()
              save()
            }

            inputPattern.clear()
            currentInputStart = Some(start)
            currentInputLabel = Some(label)

          case NeuronFireResponse(timestamp, source) =>
            if(timestamp > currentTimestamp) {

              currentTimestampFire.foreach { neuron =>
                if(!inputPattern.isDefinedAt(neuron))
                  inputPattern += neuron -> currentTimestamp
              }

              currentTimestampFire.clear()
              currentTimestamp = timestamp
            }

            currentTimestampFire += source


          case _ => throw new RuntimeException("Unrecognized message : "+m)
        }
        ExternalSender.sendTo(s, Done)
      case Close =>
        currentTimestampFire.foreach { neuron =>
          if(!inputPattern.isDefinedAt(neuron))
            inputPattern += neuron -> currentTimestamp
        }
        if(currentInputStart.isDefined && currentInputLabel.isDefined)
          execute()
        save()


        ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, Unsubscribe(LabelChangeEvent, ExternalSender.getReference(self)))

        neuronSubscribed.foreach{ neuron =>
          ExternalSender.askTo(neuron, Unsubscribe(NeuronFireEvent, ExternalSender.getReference(self)))
        }

        sender ! Done
        context stop self
      case Done => sender ! Done
      case m => throw new RuntimeException("Unrecognized message : "+m)
    }

    def gaussian_kernel(x : Double, mean : Double, norm : Double) : Double = {
      val a = 1.0/*1.0/(std*math.sqrt(2.0*math.Pi))*/
      a*math.exp(-((x-mean)*(x-mean))/(2.0*norm*norm))
    }

    def post_spike_optimization_kernel(output_neuron : NetworkEntityPath, spikes : Seq[(Timestamp, Double, Double)]) : (Timestamp, Double) = {
      val spike_kernels = spikes.map{ case(t, _, _) =>
        (t, spikes.foldLeft(0.0){case(acc, (t_x, weight, score)) => acc+gaussian_kernel(t_x.toDouble, t.toDouble, neurons(output_neuron)._2.toDouble)*weight*score })
      }
      println(output_neuron+" "+spike_kernels.mkString(", "))
      val max_kernel = spike_kernels.maxBy(_._2)
      (max_kernel._1, max_kernel._2)
    }
/*
    def real_kernel(spikes : Seq[(Timestamp, Double, Double)]) : (Timestamp, Double) = {

    }
*/
    def silence_correction_kernel(count : Int) : Double = {
      count.toDouble/100.0
    }


    def rec_propagation(t : Timestamp, spikes : Map[NetworkEntityPath, Seq[(Timestamp, Double, Double)]]) : Unit = {

      val propagation_min_ratio = 0.8

      val scores = spikes.map { case (key, list) =>
        val (avg, score) = post_spike_optimization_kernel(key, list)
        (key, avg.toLong, score+silence_correction_kernel(neuronSilenceCorrection.getOrElse(key, 0)))
      }

      val max_score = scores.map(_._3).max

      scores.foreach { case (neuron, t, score) =>
        val count = neuronSilenceCorrection.getOrElseUpdate(neuron, 0)
        neuronSilenceCorrection(neuron) = if (score == max_score) 0 else count + 1

        if (score/max_score >= propagation_min_ratio) {
          println(neuron+" "+t+" "+score)
        }
      }
    }

    def execute() : Unit = {

      val output_timestamps = mutable.HashMap[NetworkEntityPath, mutable.ArrayBuffer[(Timestamp, Double, Double)]]()

      // forward connections
      inputPattern.map{case(neuron, t) => (neuron, t-currentInputStart.get, 1.0)}.foreach { case (input_neuron, t, score) =>
        forwardConnections(input_neuron).map(i => connections(i)).foreach { case (_, output_neuron, delay, weight) =>
          output_timestamps.getOrElseUpdate(output_neuron, mutable.ArrayBuffer[(Timestamp, Double, Double)]()) += ((t + delay, weight, score))
        }
      }

      rec_propagation(0L, output_timestamps.map{case(k,v) => (k, v)}.toMap)
   /*   println("#### Label = "+currentInputLabel.get+" Inputs=["+inputPattern.map(i => (i._1, i._2-currentInputStart.get)).mkString(",")+"]")

      val next_buffer = mutable.ArrayBuffer[(NetworkEntityPath, Timestamp, Double)]()
      next_buffer ++= inputPattern.map{case(neuron, t) => (neuron, t-currentInputStart.get, 1.0)}


      val process_buffer = mutable.ArrayBuffer[(NetworkEntityPath, Timestamp, Double)]()
      process_buffer ++= next_buffer
      next_buffer.clear()

      val output_timestamps = mutable.HashMap[NetworkEntityPath, mutable.ArrayBuffer[(Timestamp, Double, Double)]]()

      // forward connections
      process_buffer.foreach { case (input_neuron, t, score) =>
        forwardConnections(input_neuron).map(i => connections(i)).foreach { case (_, output_neuron, delay, weight) =>
          output_timestamps.getOrElseUpdate(output_neuron, mutable.ArrayBuffer[(Timestamp, Double, Double)]()) += ((t + delay, weight, score))
        }
      }

      if(output_timestamps.nonEmpty) {

        //
        //  OPTIMIZE POST SPIKE REPARTITION (WINNER TAKE ALL)
        //

        val scores = output_timestamps.map { case (key, list) =>
          val (avg, score) = post_spike_optimization_kernel(key, list)
          (key, avg.toLong, score+silence_correction_kernel(neuronSilenceCorrection.getOrElse(key, 0)))
        }

        val max_score = scores.map(_._3).max
/*
        println(scores.map{ case(neuron, t, score) =>
          (if(score == max_score) "X "  else "")+neuron+" : score="+score+", t="+t
        }.mkString("\n"))
*/
        // reinforce best
        scores.foreach{ case(neuron, t, score) =>
          val count = neuronSilenceCorrection.getOrElseUpdate(neuron, 0)
          neuronSilenceCorrection(neuron) = if(score == max_score) 0 else count+1

          if(score == max_score) {
            println("Winner : "+neuron+" with "+score+" at "+t+" : "+output_timestamps(neuron).map(e => (e._1, gaussian_kernel(e._1.toDouble, t.toDouble, neurons(neuron)._2.toDouble))).filter(_._2 > 0.1).mkString(", "))
            // delta_d proportional to score influence ?

            // Step 1 : optimize local divergence
            backwardConnections(neuron).map(i => (connections(i), i)).foreach { case ((input_neuron, output_neuron, delay, weight), index) =>

              if(inputPattern.isDefinedAt(input_neuron)) {
                val n_delay = delay + ((t - ((inputPattern(input_neuron) - currentInputStart.get) + delay)).toDouble * learningRate).toLong
                //println(input_neuron+" -> "+output_neuron+" : "+delay+" -> "+n_delay+" ("+(inputPattern(input_neuron)-currentInputStart.get)+"+"+delay+"="+((inputPattern(input_neuron)-currentInputStart.get)+delay)+" <> "+t+" : "+(t-((inputPattern(input_neuron)-currentInputStart.get)+delay))+")")
                connections.update(index, (input_neuron, output_neuron, n_delay, weight))
              }
            }

            // Step 2 : optimize global timestamp
            val min_delay = backwardConnections(neuron).map(i => connections(i)).map(_._3).min
            val global_correction = if(min_delay < 0) -min_delay else -(min_delay*learningRate).toLong

            backwardConnections(neuron).map(i => (connections(i), i)).foreach { case ((input_neuron, output_neuron, delay, weight), index) =>
              connections.update(index, (input_neuron, output_neuron, delay+global_correction, weight))
            }

            //
            //  OPTIMIZE MEMBRANE_TIME_CST AND THRESHOLD
            //

            val n_post_timestamps = backwardConnections(neuron).map(i => (connections(i), i)).filter(c => inputPattern.isDefinedAt(c._1._1)).map { case ((input_neuron, output_neuron, delay, weight), index) =>
              inputPattern(input_neuron)+delay
            }

            val max_t = n_post_timestamps.max

            val th_noise =
              n_post_timestamps.map(t_post => if(t_post == max_t)
                  1.0
                else
                  math.exp(-(max_t-t_post+2*noise_tolerance.timestamp).toDouble/neurons(neuron)._2.toDouble)
              ).sum
            val tau_mem_noise = (n_post_timestamps.map(t_post => n_post_timestamps.max-t_post+2*noise_tolerance.timestamp).max/minPreInfluence).toLong

            val neuron_parameter = neurons(neuron)
            neurons(neuron) = (neuron_parameter._1+(th_noise-neuron_parameter._1)*learningRate, neuron_parameter._2+((tau_mem_noise-neuron_parameter._2)*learningRate).toLong)

            println("th : "+neuron_parameter._1+" -> "+th_noise+" = "+neurons(neuron)._1)
            println("tau_mem : "+neuron_parameter._2+" -> "+tau_mem_noise+" = "+neurons(neuron)._2)

          }

        }

        // Update timestamps with new delays
        output_timestamps.clear()
        process_buffer.foreach { case (input_neuron, t, score) =>
          forwardConnections(input_neuron).map(i => connections(i)).foreach { case (_, output_neuron, delay, weight) =>
            output_timestamps.getOrElseUpdate(output_neuron, mutable.ArrayBuffer[(Timestamp, Double, Double)]()) += ((t + delay, weight, score))
          }
        }


        //println(connections.filter(_._2 == neuron).map(e => e.input_neuron+" -> "+e.output_neuron+" : "+Time(delay).asMilliSecond).mkString(", "))

        output_timestamps.clear()
      }*/
    }

    def load() : Unit = {

      ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))

      targetNeuron.foreach{ output_neuron =>

        neurons += output_neuron -> (1.0, initialMembraneTimeConstant.timestamp)

        (ExternalSender.askTo(output_neuron, GetAllConnectionProperty(SynapseWeightAndDelay)) match {
          case PropertyValue(values: Seq[(NetworkEntityPath, (Float, Time))]@unchecked) => values
          case _ => Seq[(NetworkEntityPath, (Float, Time))]()
        }).map { case (input_neuron, (weight, delay)) =>

          if(!neuronSubscribed.contains(input_neuron)) {
            ExternalSender.askTo(input_neuron, SubscribeSynchronized(NeuronFireEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))
            neuronSubscribed += input_neuron
          }

          connections += ((input_neuron, output_neuron, delay.timestamp, weight))
        }
      }

      // index connections
      forwardConnections ++= connections.zipWithIndex.groupBy(_._1._1).map{ case (input_neuron, list) =>
        (input_neuron, list.map(_._2))
      }

      backwardConnections ++= connections.zipWithIndex.groupBy(_._1._2).map{ case (output_neuron, list) =>
        (output_neuron, list.map(_._2))
      }

    }

    def save() : Unit = {
      //println("Save training")

      targetNeuron.foreach { output_neuron =>
        ExternalSender.askTo(output_neuron, SetProperty(MembraneThresholdFloat, neurons(output_neuron)._1.toFloat))
        ExternalSender.askTo(output_neuron, SetProperty(MembraneLeakTime, Time(neurons(output_neuron)._2)))

        val properties = ExternalSender.askTo(output_neuron, GetAllConnectionProperty(SynapseWeightAndDelay))
          .asInstanceOf[ConnectionPropertyValues[(Float, Time)]].values.map { case (connectionId, inputNeuron, (weight, delay)) =>
          val find_connection = connections.filter(c => c._1 == inputNeuron && c._2 == output_neuron)
          val connection = if(find_connection.size == 1)
            find_connection.head
          else
            (connectionId, output_neuron, delay.timestamp, weight.toDouble)

          (connectionId, (connection._4.toFloat, Time(connection._3)))
        }
        ExternalSender.askTo(output_neuron, SetAllConnectionProperty(SynapseWeightAndDelay, properties))
      }
    }

  }

  val actor = n2s3.system.actorOf(Props(new ReservoirCoincidenceDetectorActor(n2s3, targetNeuron, learningRate)), LocalActorDeploymentStrategy)
  Await.result(actor ? Done, timeout.duration)

  def destroy() : Unit = {
    Await.result(actor ? Close, timeout.duration)
  }

}
