package fr.univ_lille.cristal.emeraude.n2s3.features.builder

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{Destroy, SetSynchronizer}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.NetworkEntityActor
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.FixedParameter
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.ConnectionPolicy
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.FullConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.Shape
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{NetworkEntityDeploymentPolicy, PropsBuilder}

import scala.collection.mutable.ArrayBuffer

/**
  * Created by guille on 6/2/16.
  */
class NeuronGroupRef(n2s3: N2S3) extends N2S3ActorRef {


  var identifier: String = ""
  var defaultNeuronConstructor: () => Neuron = _
  var defaultNeuronModel: (NeuronModel, Seq[(Property[_], _)]) = _
  var connections = new ArrayBuffer[ConnectionRef]()
  var shape = Shape()
  var actorPolicy: Option[NetworkEntityDeploymentPolicy] = None
  var flatHierarchy = false

  /********************************************************************************************************************
    * Accessing
    ******************************************************************************************************************/

  def getActor : this.type = this
  def getContainer: NetworkEntityPath = this.actorPath.get
  def neurons : Seq[NeuronRef] = this.getNeuronRefs
  def neuronPaths : Seq[NetworkEntityPath] = {
    //println("Warning : neuronPaths")
    this.neurons.map(_.actorPath.get)
  }
  def getNeuronPathAt(index : Int*) : NetworkEntityPath = {
    this.neurons(shape.toIndex(index:_*)).actorPath.get
  }

  var neuronRefs: Option[Seq[NeuronRef]] = None
  def getActorPolicy : NetworkEntityDeploymentPolicy = actorPolicy.getOrElse(n2s3.getNetworkEntitiesPolicy)

  def getIdentifierOf(ref: NeuronRef): Any = this.identifier+":"+ref.getIndex.mkString(":")//this.identifier + ("%0" + (math.log10(shape.getNumberOfPoints()).toInt+1) + "d").format(ref.index)
  def hasIdentifier : Boolean = this.identifier.nonEmpty

  def getNeuronRefs : Seq[NeuronRef] = {
    if (neuronRefs.isEmpty){
      neuronRefs = Some(for (
        index <- shape.allIndex
      ) yield {
        val neuron = new NeuronRef(this, index)
        neuron
      })
    }
    neuronRefs.get
  }

  def isEmpty : Boolean = shape.isEmpty
  def isConnected(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = {
    this.connections.exists( c => c.connects(aNeuron, anotherNeuron))
  }

  /********************************************************************************************************************
    * Neuron Group Construction
    ******************************************************************************************************************/

  def setIdentifier(identifier: String) : this.type = {
    this.identifier = identifier
    this
  }

  def getIdentifier : String = this.identifier

  def setNumberOfNeurons(i: Int) : this.type = {
    this.shape = Shape(i)
    this
  }

  def getNumberOfNeurons : Int = {
    this.shape.dimensions.product
  }

  def setShape(index : Int*) : this.type = {
    this.shape = Shape(index:_*)
    this
  }

  def setNeuronModel(modelKind: NeuronModel, properties: Seq[(Property[_], _)] = Seq()) : this.type = {
    this.defaultNeuronModel = (modelKind, properties)
    this.defaultNeuronConstructor = () => {
      val neuron: Neuron = modelKind.createNeuron()
      for ((k, v) <- properties) {
        neuron.setProperty(k, v)
      }
      neuron
    }
    this
  }

  def setActorPolicy(policy: NetworkEntityDeploymentPolicy) : this.type = {
    this.actorPolicy = Some(policy)
    this
  }

  def connectTo(layer: NeuronGroupRef, connectionType: ConnectionPolicy = new FullConnection) : ConnectionRef = {
    val ref = new ConnectionRef(this, layer, connectionType)
    this.connections += ref
    ref
  }

  def setSynchronizer(synchronizer: NetworkEntityPath): Unit = {
    this.actorPath match {
      case None =>
      case Some(theActorPath) => this.ask(SetSynchronizer(synchronizer))
    }
  }

  /********************************************************************************************************************
    * Simulation Interaction/Actions
    ******************************************************************************************************************/

  def fixNeurons() : Unit = {
    neurons.foreach( neuron => neuron.ask(SetProperty(FixedParameter, true)))
  }

  def unfixNeurons() : Unit = {
    neurons.foreach( neuron => neuron.ask(SetProperty(FixedParameter, false)))
  }

  def removeNeuron(path : NetworkEntityPath) : Unit = {
    val newNeuronList = neuronRefs.get.filter(_.actorPath.get != path)

    if(newNeuronList.size != neuronRefs.get.size-1) {
      throw new NoSuchElementException
    }

    ExternalSender.askTo(path, Destroy)
    neuronRefs = Some(newNeuronList)

  }

  /********************************************************************************************************************
    * Deployment
    ******************************************************************************************************************/

  def ensureActorDeployed(n2s3: N2S3): Unit = {
    if (!this.isDeployed){
      this.deployActors(n2s3)
    }
  }

  /**
    * Hook to deploy actors in a given simulation.
    * This method will be called if the [[InputNeuronGroupRef]] is not yet deployed
    *
    * @param n2s3 the current simulation object
    */
  protected def deployActors(n2s3: N2S3): Unit = {
    println("Create "+getIdentifier+" ("+getNumberOfNeurons+" neurons)")
    this.getActorPolicy.deployNeuronGroupActor(this, n2s3)
    this.getNeuronRefs.foreach{ this.getActorPolicy.deployNeuronActor(_, n2s3) }
    this.setDeplyed()
  }

  def ensureConnectionsDeployed(n2s3: N2S3) : Unit = {
    this.connections.foreach( connection => connection.ensureDeployed(n2s3) )
  }

  def newActorProps(): PropsBuilder = NetworkEntityActor.newPropsBuilder().setEntity(new NetworkContainer)
}
