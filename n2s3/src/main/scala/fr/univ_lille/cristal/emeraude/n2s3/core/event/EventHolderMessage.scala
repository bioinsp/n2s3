package fr.univ_lille.cristal.emeraude.n2s3.core.event

import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message
/***********************************************************************************************
 * 														SUBSCRIBE AND UNSUBSCRIBE MESSAGES
 **********************************************************************************************/
abstract class EventHolderMessage extends Message