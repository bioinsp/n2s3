package fr.univ_lille.cristal.emeraude.n2s3;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;

import java.awt.*;

public class Graph extends ApplicationFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1352234752863184258L;

	// chart data
	private XYSeriesCollection _dataset;
	private XYSeries firstSerie, secondSerie;

	// abscissa of the next point
	private int time;
	private final String time_name = "time";

	/**
	 * Main constructor
	 *
	 * @param title
	 *            title of the frame
	 */
	public Graph(String title, String first_title, String second_title) {
		super(title);
		this.time = 1;
		this.firstSerie = new XYSeries(first_title);
		this.secondSerie = new XYSeries(second_title);
		this._dataset = new XYSeriesCollection();
		this._dataset.addSeries(firstSerie);
		this._dataset.addSeries(secondSerie);
		final JFreeChart chart = createChart(_dataset);
		final ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
		setContentPane(chartPanel);
		this.setVisible(true);
	}

	/**
	 * Creation of the chart
	 *
	 * @param dataset
	 *            data to display on the graph
	 * @return the chart initialized
	 */
	private JFreeChart createChart(final XYDataset dataset) {

		// create the chart...
		final JFreeChart chart = ChartFactory.createXYLineChart(
				this.getTitle(), // chart title
				this.time_name, // x axis label
				"Entity number", // y axis label
				dataset, // data
				PlotOrientation.VERTICAL, true, // include legend
				true, // tooltips
				false // urls
				);

		// NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
		chart.setBackgroundPaint(Color.white);

		// get a reference to the plot for further customisation...
		final XYPlot plot = chart.getXYPlot();
		plot.setBackgroundPaint(Color.lightGray);
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);

		final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
		renderer.setSeriesShapesVisible(0, false);
		renderer.setSeriesShapesVisible(1, false);
		plot.setRenderer(renderer);

		// change the auto tick unit selection to integer units only...
		final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		// OPTIONAL CUSTOMISATION COMPLETED.

		return chart;

	}

	@Override
	public void paint(Graphics grphcs) {
		super.paint(grphcs);
	}

	public void addall(double x, double y) {
		this.secondSerie.add(this.time, y);
		this.firstSerie.add(this.time, x);
		XYSeriesCollection xs = new XYSeriesCollection();
		xs.addSeries(firstSerie);
		xs.addSeries(secondSerie);
		this.time++;
		this._dataset = xs;
	}

}
