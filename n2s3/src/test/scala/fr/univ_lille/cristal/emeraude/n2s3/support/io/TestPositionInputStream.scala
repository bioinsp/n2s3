package fr.univ_lille.cristal.emeraude.n2s3.support.io

import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.MnistFileInputStream
import fr.univ_lille.cristal.emeraude.n2s3.support.N2S3ResourceManager

/**
  * Created by guille on 6/2/16.
  */
class TestPositionInputStream extends UnitSpec {
  val testImageFileName: String = N2S3ResourceManager.getByName("mnist-test-images").getAbsolutePath
  val testLabelFileName: String = N2S3ResourceManager.getByName("mnist-test-labels").getAbsolutePath

  "Position input stream" should "start in zero" in {
    val wrappee = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val stream = new PositionInputStream(wrappee)
    stream.position shouldBe 1
  }

  "Position input stream" should "have a position equals to the number of read images" in {
    val wrappee = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val stream = new PositionInputStream(wrappee)
    for (i <- 1 until 100) {
      stream.next()
      stream.position shouldBe i + 1
    }
  }

  "Position input stream after reset" should "be zero" in {
    val wrappee = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val stream = new PositionInputStream(wrappee)
    for (i <- 1 until 100) {
      stream.next()
    }
    stream.reset()
    stream.position shouldBe 1
  }
}
