package fr.univ_lille.cristal.emeraude.n2s3

import org.scalatest._

/*
 * Common style of test used in the N2S3 project.
 */
abstract class UnitSpec extends FlatSpec with Matchers