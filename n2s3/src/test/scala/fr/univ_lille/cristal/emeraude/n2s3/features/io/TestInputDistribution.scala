package fr.univ_lille.cristal.emeraude.n2s3.features.io

import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.{InputDistribution, Time}

import scala.util.Random

/**
  * Created by falezp on 24/03/16.
  */
class TestInputDistribution extends UnitSpec {

  "regular distribution with frequency f" should "return floor((end-start)*f) timestamps between [start; end] regular spaced" in {
    val start : Time = 0 Second
    val end : Time = 1 Second
    val f = 10.0f
    val list = InputDistribution.regular(start.timestamp, end.timestamp, f)

    val expectedNumber = math.floor(Time(end.timestamp-start.timestamp).asSecond*f)

    assert(list.size == expectedNumber.toInt)

    list.zipWithIndex.foreach { case (timestamp, index) =>
      assert(timestamp == start.timestamp+(index*(end.timestamp-start.timestamp).toDouble/expectedNumber).toLong)
    }
  }

  "uniform distribution of n average" should "return floor(n) timestamps between [start; end]" in {
    val start : Timestamp = 100
    val end : Timestamp = 200
    val n = 10.5f
    val list = InputDistribution.uniform(new Random(), start, end, n)

    assert(list.size == math.floor(n).toInt)

    list.foreach { timestamp =>
      assert(timestamp >= start && timestamp <= end)
    }
  }

  "gaussian distribution of n average" should "return floor(n) timestamps between [start; end]" in {
    val start : Timestamp = 100
    val end : Timestamp = 200
    val n = 10.5f
    val list = InputDistribution.gaussian(new Random(), start, end, n)

    assert(list.size == math.floor(n).toInt)

    list.foreach { timestamp =>
      assert(timestamp >= start && timestamp <= end)
    }
  }

  "poisson distribution of n average" should "return timestamps between [start; end]" in {
    val start : Timestamp = 100
    val end : Timestamp = 200
    val n = 10.5f
    val list = InputDistribution.poisson(new Random(), start, end, n)

    list.foreach { timestamp =>
      assert(timestamp >= start && timestamp <= end)
    }
  }

  "jitter distribution of n average" should "return timestamps between [start; end]" in {
    val start : Timestamp = 100
    val end : Timestamp = 200
    val n = 10.5f
    val list = InputDistribution.jitter(new Random(32), start, end, n, 0.1)

    list.foreach { timestamp =>
      assert(timestamp >= start && timestamp <= end)
    }
  }

}