package fr.univ_lille.cristal.emeraude.n2s3.apps

import fr.univ_lille.cristal.emeraude.n2s3.core.ConnectionIndex
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.MembranePotentialThreshold
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.FullConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph.{SynapticWeightSelectGraph, SynapticWeightSelectGraphRef}
import fr.univ_lille.cristal.emeraude.n2s3.models.neurons.LIF
import fr.univ_lille.cristal.emeraude.n2s3.models.synapses.{InhibitorySynapse, QBGParameters, SimplifiedSTDP}
import fr.univ_lille.cristal.emeraude.n2s3.support.N2S3ResourceManager
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import squants.electro.ElectricPotentialConversions.ElectricPotentialConversions

/**
  * Experience reproduced from:
  * Simulation of a memristor-based spiking neural network immune to device variations
  * D Querlioz, O Bichler, C Gamrat, Neural Networks (IJCNN), The 2011 International Joint Conference on
  */

object  ExampleMnist extends App {

  implicit val timeout = Config.longTimeout

  QBGParameters.alf_m = 0.005f
  QBGParameters.alf_p = 0.01f
  QBGParameters.beta_m = 2f
  QBGParameters.beta_p = 2f

  val n2s3 = new N2S3("N2S3")

  val inputStream = InputMnist.Entry >> SampleToSpikeTrainConverter[Float, InputSample2D[Float]](0, 22, 150 MilliSecond, 350 MilliSecond) >> N2S3Entry


//  val inputLayer = n2s3.createInput(Mnist.distributedInput("data/train2k-images.idx3-ubyte", "data/train2k-labels.idx1-ubyte"))
  val inputLayer = n2s3.createInput(inputStream)
  val dataFile = N2S3ResourceManager.getByName("mnist-train-images").getAbsolutePath
  val labelFile =  N2S3ResourceManager.getByName("mnist-train-labels").getAbsolutePath
  inputStream.append(InputMnist.DataFrom(dataFile, labelFile))

  val unsupervisedLayer = n2s3.createNeuronGroup()
    .setIdentifier("Layer1")
    .setNumberOfNeurons(30)
    .setNeuronModel(LIF, Seq(
      (MembranePotentialThreshold, 35 millivolts)))

  inputLayer.connectTo(unsupervisedLayer, new FullConnection(() => new SimplifiedSTDP))
  unsupervisedLayer.connectTo(unsupervisedLayer, new FullConnection(() => new InhibitorySynapse))

  n2s3.create()

  val inputToClassifierIndex = new ConnectionIndex(inputLayer, unsupervisedLayer)

  n2s3.addNetworkObserver(new SynapticWeightSelectGraphRef(
    for(outputIndex <- 0 until unsupervisedLayer.shape.getNumberOfPoints) yield {
      for(inputX <- 0 until InputMnist.shape.dimensions(0)) yield {
        for(inputY <- 0 until InputMnist.shape.dimensions(1)) yield {
          val input = inputLayer.getNeuronPathAt(inputX, inputY)
          val output = unsupervisedLayer.getNeuronPathAt(outputIndex)
          inputToClassifierIndex.getConnectionsBetween(input, output).head
        }
      }
    },
    SynapticWeightSelectGraph.heatMap,
    4,
    100)
  )

  println("Start Training ...")
  n2s3.runAndWait()

  val dataTestFile = N2S3ResourceManager.getByName("mnist-test-images").getAbsolutePath
  val labelTestFile =  N2S3ResourceManager.getByName("mnist-test-labels").getAbsolutePath

  inputStream.append(InputMnist.DataFrom(dataTestFile, labelTestFile))

  println("Start Testing ...")
  unsupervisedLayer.fixNeurons()
  val benchmarkMonitor = n2s3.createBenchmarkMonitor(unsupervisedLayer)

  n2s3.runAndWait()
  println(benchmarkMonitor.getResult.evaluationByMaxSpiking)
  benchmarkMonitor.exportToHtmlView("test.html")

  n2s3.destroy()
}
