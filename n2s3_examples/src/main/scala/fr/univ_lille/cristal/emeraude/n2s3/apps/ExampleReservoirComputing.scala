package fr.univ_lille.cristal.emeraude.n2s3.apps

import java.io.{File, PrintWriter}

import akka.actor.Props
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core.ExternalSender
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, Done, ElectricSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.event.{NeuronFireEvent, Subscribe}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.{RandomConnection, ReservoirConnection}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.features.learning.DelayAdaptation
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.NeuronsFireLogText
import fr.univ_lille.cristal.emeraude.n2s3.models.neurons.LIF
import fr.univ_lille.cristal.emeraude.n2s3.models.synapses.StaticSynapse
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.{InputDistribution, Time}
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy
import squants.electro.ElectricPotentialConversions._

import scala.collection.mutable.ListBuffer
import scala.concurrent.Await
import scala.util.Random


/**
  * Experiment reproduced from:
  * A supervised learning approach based on STDP and polychronization in spiking neuron networks,
  * H. Paugam-Moisy, R. Martinez, S. Bengio
  */

object InputMoveEntry extends InputFormat[InputTemporalPacket](Shape(10)) {

  def Data(duration : Time, patternFrequency : Float)  : MoveEntry = {
    val data = new MoveEntry
    data.generate(duration, patternFrequency)
    data
  }
}

class MoveEntry extends InputGenerator[InputTemporalPacket] {

  val entrySize = 10

  val velocity = 2 MilliSecond // per entry point

  var inputData = Seq[(Timestamp, Int)]()
  var labelTimestamps = Seq[(String, Timestamp)]()

  var labelCursor : Int = 0
  var dataCursor : Int = 0

  var currentSecond : Int = 0

  def generate(duration : Time, patternFrequency : Float) : Unit = {

    labelTimestamps = InputDistribution.poisson(Random, 0, duration.timestamp, patternFrequency, 4*velocity.timestamp*entrySize).map{ t =>
      (if(Random.nextBoolean()) "Up" else "Down", t)
    }

    inputData = labelTimestamps.flatMap { case(label, start) =>
      if(label == "Up") {
        (0 until entrySize).map(j => (start+j*velocity.timestamp, j))
      }
      else { //down
        (0 until entrySize).map(j => (start+j*velocity.timestamp, entrySize-1-j))
      }
    }sortBy(_._1)
  }

  override def shape: Shape = Shape(entrySize)

  override def next(): InputTemporalPacket = {
    if(dataCursor < inputData.size && (inputData(dataCursor)._1/1000000L).toInt != currentSecond) {
      currentSecond = (inputData(dataCursor)._1/1000000L).toInt
      println("[Spike Train] "+currentSecond+"s")
    }


    val endTimestamp = if(labelCursor+1 < labelTimestamps.size) labelTimestamps(labelCursor+1)._2-1L else Long.MaxValue
    val label_info = labelTimestamps(math.min(labelCursor, labelTimestamps.size-1))

    var list = ListBuffer[(Seq[Int], N2S3InputSpike)]()
    while(dataCursor < inputData.size && inputData(dataCursor)._1 <= endTimestamp) {
      list += Seq(inputData(dataCursor)._2) -> N2S3InputSpike(ElectricSpike(), inputData(dataCursor)._1)
      dataCursor += 1
    }
    labelCursor += 1
    new InputTemporalPacket(shape, list.groupBy(_._1).map(e => (e._1, e._2.map(_._2)))).setMetaData(N2S3InputLabel(label_info._1, label_info._2, endTimestamp))
  }

  override def atEnd(): Boolean = labelCursor >= labelTimestamps.size

  override def reset(): Unit = {
    labelCursor = 0
    dataCursor = 0
  }

  def saveLabel(filename : String) : Unit = {
    val writer = new PrintWriter(new File(filename))
    labelTimestamps.foreach { t =>
      writer.println(t)
    }
    writer.close()
  }
}

object ExampleReservoirComputing extends App {

  val n2s3 = new N2S3()



  val inputStream = InputMoveEntry.Entry >> JitterNoise(3 MilliSecond) >> N2S3Entry
  val inputLayer = n2s3.createInput(inputStream)

  inputStream.append(InputMoveEntry.Data(100 Second, 10))

  // input append

  val reservoir = n2s3.createNeuronGroup()
    .setIdentifier("Reservoir")
    .setShape(6, 6, 6)
    .setNeuronModel(LIF, Seq(
      (MembranePotentialThreshold, 5 millivolts),
      (MembraneLeakTime, 10 MilliSecond),
      (MembraneThresholdType, MembraneThresholdTypeEnum.Static),
      (MembraneRefractoryDuration, 5 MilliSecond)
    ))

  val classifier = n2s3.createNeuronGroup()
    .setIdentifier("Classifier")
    .setNumberOfNeurons(2)
    .setNeuronModel(LIF, Seq(
      (MembranePotentialThreshold, 2 millivolts),
      (MembraneLeakTime, 20 MilliSecond),
      (MembraneThresholdType, MembraneThresholdTypeEnum.Static),
      (MembraneRefractoryDuration, 10  MilliSecond)
    ))

  inputLayer.connectTo(reservoir, new RandomConnection(0.1f, () => new StaticSynapse(3.0f)))
  reservoir.connectTo(reservoir, new ReservoirConnection(0.3f, distance => new StaticSynapse(if(Random.nextFloat() <= 0.8) 1f else -1f, Random.nextFloat()*20f MilliSecond)))
  reservoir.connectTo(classifier, new RandomConnection(1.0f, () => new StaticSynapse(0.5f, Random.nextFloat()*20f MilliSecond)))

  n2s3.create()

//  ExportNetworkTopology.save("topology_graph", n2s3)

  println("Start Training ...")

  val learning = new DelayAdaptation(n2s3, Map(
    "Up" -> classifier.neuronPaths(0),
    "Down" -> classifier.neuronPaths(1)
  ), 1.0f)
  n2s3.runAndWait()
  learning.destroy()

  inputStream.clean()
  inputStream.append(InputMoveEntry.Data(10 Second, 10))

  println("Start Testing ...")
  //unsupervisedLayer.fixNeurons()

  val logger = n2s3.system.actorOf(Props(new NeuronsFireLogText("fire.out")), LocalActorDeploymentStrategy)
  inputLayer.neuronPaths.foreach{ n =>
    ExternalSender.askTo(n, Subscribe(NeuronFireEvent, ExternalSender.getReference(logger)))
  }
  reservoir.neuronPaths.foreach{ n =>
    ExternalSender.askTo(n, Subscribe(NeuronFireEvent, ExternalSender.getReference(logger)))
  }
  classifier.neuronPaths.foreach{ n =>
    ExternalSender.askTo(n, Subscribe(NeuronFireEvent, ExternalSender.getReference(logger)))
  }

  val benchmarkMonitor = n2s3.createBenchmarkMonitor(classifier)

  n2s3.runAndWait()

  implicit val timeout = Config.defaultTimeout
  Await.result(logger ? Done, Config.defaultTimeout.duration)

  println(benchmarkMonitor.getResult)
  benchmarkMonitor.exportToHtmlView("test.html")
}
