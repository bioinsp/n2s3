package fr.univ_lille.cristal.emeraude.n2s3.apps

import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.{MembranePotentialThreshold, SynapseLTP}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.FullConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.N2S3InputStreamCombinators._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{DigitalHexInputStream, _}
import fr.univ_lille.cristal.emeraude.n2s3.models.neurons.LIF
import fr.univ_lille.cristal.emeraude.n2s3.models.synapses.{InhibitorySynapse, QBGParameters, SimplifiedSTDPWithNegative}
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import squants.electro.ElectricPotentialConversions._

object ExampleActorPolicies extends App {

  QBGParameters.alf_m = 0.02f
  QBGParameters.alf_p = 0.06f
  QBGParameters.beta_m = 3f
  QBGParameters.beta_p = 2f

  val oversampling = 2

  val n2s3 = new N2S3()

  val inputStream =
    InputDigitalHex.Entry >> SampleToSpikeTrainConverter[Float, InputSample2D[Float]](0, 23, 150 MilliSecond, 350 MilliSecond) >> N2S3Entry
      //StreamOversampling[SampleInput](oversampling, oversampling) >>

      //InputNoiseGenerator(1 hertz)


  val inputLayer = n2s3.createInput(inputStream)
  //  val inputLayer = n2s3.createInput(DigitalHex.distributedInput(2, 1))

  val unsupervisedLayer = n2s3.createNeuronGroup()
    .setIdentifier("Layer1")
    .setNumberOfNeurons(18)
    .setNeuronModel(LIF, Seq(
      (MembranePotentialThreshold, 2*oversampling millivolts),
      (SynapseLTP, 50 MilliSecond)))
  inputLayer.connectTo(unsupervisedLayer, (new FullConnection).setDefaultConnectionConstructor(() => new SimplifiedSTDPWithNegative))
  unsupervisedLayer.connectTo(unsupervisedLayer, (new FullConnection).setDefaultConnectionConstructor(() => new InhibitorySynapse))
  n2s3.createSynapseWeightGraphOn(inputLayer, unsupervisedLayer)


  println("Start Training ...")
  inputStream.append(new DigitalHexInputStream().repeat(100).shuffle())
  n2s3.runAndWait()


  println("Start Testing ...")
  unsupervisedLayer.fixNeurons()
  inputStream.clean()
  inputStream.append(new DigitalHexInputStream().repeat(10).shuffle())
  //  n2s3.runAndWait(100)
  //  inputLayer.setInput(DigitalHex.distributedInput(2, 1))
  val benchmarkMonitor = n2s3.createBenchmarkMonitor(unsupervisedLayer)

  n2s3.runAndWait()

  println(benchmarkMonitor)
  benchmarkMonitor.exportToHtmlView("results/test.html")

  System.exit(0)
}
